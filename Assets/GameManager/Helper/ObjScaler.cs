﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjScaler : MonoBehaviour
{
    [SerializeField] private float        baseWidth  = 1280f;
    [SerializeField] private float        baseHeight = 720f;
    public float ratio;
    public Vector3 currentScale;

    private void Awake()
    {
        var w = baseWidth  / Screen.width;
        var h = baseHeight / Screen.height;
        ratio                     = h / w;
        transform.localScale = currentScale * ratio;
    }
}
