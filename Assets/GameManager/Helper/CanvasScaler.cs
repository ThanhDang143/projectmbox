﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HelperDev
{
    public class CanvasScaler : MonoBehaviour
    {
        [SerializeField] private float   baseWidth  = 1280f;
        [SerializeField] private float   baseHeight = 720f;
        [SerializeField] private float   ratio;
        [SerializeField] private float   ratioChild;
        [SerializeField] Vector3 currentScale = Vector3.one;
        [Range(0, 1)] public float match = 0;
        private float oldMatch = 0;
        private float step = 0;
        private void Awake()
        {
            var w = baseWidth  / Screen.width;
            var h = baseHeight / Screen.height;
            ratio                = h            / w;
            ratioChild = w / h;
            step = Mathf.Abs(ratio - ratioChild);
            transform.localScale = currentScale * ratio;
        }

        private void Update()
        {
            if (match > 0)
            {
                if (oldMatch != match)
                {
                    oldMatch = match;
                    foreach (Transform child in transform)
                    {
                        child.localScale = currentScale * Mathf.Clamp((ratio - step * match), 0, 1);
                    }
                }
            }
        }
    }
}

