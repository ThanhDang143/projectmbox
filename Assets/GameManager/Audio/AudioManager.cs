﻿using System;
using System.Collections;
using System.Collections.Generic;
using PathologicalGames;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager ins;

    public AudioClip[] audioClips;
    public AudioSource sourceBackGround;
    public AudioSource sourceClick;
    public List<AudioSource> audioSourceOncePlay;
    public Sound[] sounds;
    public Sound[] soundsBackGround;

    void Awake()
    {
        if (ins != null)
        {
            Destroy(gameObject);
        }
        else
        {
            ins = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    [ContextMenu("CreateSound")]
    public void CreateSound()
    {
        sounds = new Sound[audioClips.Length];
        for (int i = 0; i < sounds.Length; i++)
        {
            //sounds[i] = new Sound(audioClips[i].name,audioClips[i],false);
            sounds[i] = new Sound();
            sounds[i].name = audioClips[i].name;
            sounds[i].clip = audioClips[i];
            sounds[i].volume = 2;
            sounds[i].pitch = 1;
        }
    }

    public void PlayOther(string nameSound, float _volume = 1)
    {
        if (PlayerPrefs.GetInt(StaticString.saveSound) == 0) return;

        foreach (var t in sounds)
        {
            if (t.name.Equals(nameSound))
            {
                var index = 0;
                while (audioSourceOncePlay[index].isPlaying)
                {
                    index++;
                    if (index >= audioSourceOncePlay.Count)
                    {
                        var s = Instantiate(audioSourceOncePlay[0]);
                        audioSourceOncePlay.Add(s);
                        s.loop = t.loop;
                        s.clip = t.clip;
                        s.Play();
                        Destroy(s.gameObject, s.clip.length);
                        return;
                    }
                }

                audioSourceOncePlay[index].loop = t.loop;
                audioSourceOncePlay[index].clip = t.clip;
                audioSourceOncePlay[index].volume = _volume;
                audioSourceOncePlay[index].Play();
                for (int i = 0; i < audioSourceOncePlay.Count; i++)
                {
                    if (audioSourceOncePlay[i] == null) audioSourceOncePlay.Remove(audioSourceOncePlay[i]);
                }
                return;
            }
        }

        Debug.LogWarning("Not Found " + nameSound);
    }

    public void StopOther(string nameSound)
    {
        foreach (var t in sounds)
        {
            if (t.name.Equals(nameSound))
            {
                for (int i = 0; i < audioSourceOncePlay.Count; i++)
                {
                    try
                    {
                        if (audioSourceOncePlay[i].clip.name.Equals(t.clip.name))
                        {
                            audioSourceOncePlay[i].Stop();
                            return;
                        }
                    }
                    catch (System.Exception)
                    {

                    }
                }
            }
        }
    }

    public void StopAllSound()
    {
        for (int i = 0; i < audioSourceOncePlay.Count; i++)
        {
            audioSourceOncePlay[i].Stop();
        }
    }

    public void Click()
    {
        if (PlayerPrefs.GetInt(KeyPre.Sound, 1) == 0) return;
        sourceClick.Play();
    }

    public void PlayBackGround(string names)
    {
        if (PlayerPrefs.GetInt(StaticString.saveMusic) == 0) return;

        if (sourceBackGround.clip != null && names.Equals(sourceBackGround.clip.name)) return;
        var s = Array.Find(soundsBackGround, sound => sound.name == names);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + names + " not found");
            return;
        }

        sourceBackGround.loop = true;
        sourceBackGround.clip = s.clip;
        sourceBackGround.volume = 0.5f;
        sourceBackGround.Play();
        //s.source.Play();
    }

    public void StopBackGround()
    {
        sourceBackGround.Stop();
    }

    public void ContinueBg()
    {
        if (sourceBackGround.clip != null)
        {
            StopAllCoroutines();
            sourceBackGround.Play();
        }
    }

    public void PauseBg(bool win)
    {
        StartCoroutine(IePause(win));
    }

    IEnumerator IePause(bool win)
    {
        sourceBackGround.Pause();
        if (win)
        {
            yield return new WaitForSeconds(2.7f);
        }
        else
        {
            yield return new WaitForSeconds(4f);
        }

        sourceBackGround.Play();
    }

    public void ClickButton()
    {
        if (PlayerPrefs.GetInt(KeyPre.Sound, 1) == 0) return;
        sourceClick.Play();
    }
}