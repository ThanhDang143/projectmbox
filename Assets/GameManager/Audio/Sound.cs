﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Sound
{
    public string name;

    public AudioClip clip;

    [Range(0f, 2f)]
    public float volume;

    [Range(.1f, 1)]
    public float pitch;

    public bool loop;

    public bool playAwake;

    public Sound(string names, AudioClip c, bool l)
    {
        name = names;
        clip = c;
        loop = l;
    }

    public Sound()
    {
        
    }
}