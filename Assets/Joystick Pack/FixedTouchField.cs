using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class FixedTouchField : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [HideInInspector]
    public Vector2 TouchDist;
    [HideInInspector]
    public Vector2 PointerOld;
    [HideInInspector]
    protected int PointerId;
    [HideInInspector]
    public bool Pressed;
    private float pressedTime;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Pressed)
        {
            if (UIManager.ins.currentPopup == Popup.PTutorial1)
            {
                UIManager.ins.ShowPopup(Popup.None);
                StartCoroutine(IETutorial1());
            }

            if (UIManager.ins.currentPopup == Popup.PTutorial2 || UIManager.ins.currentPopup == Popup.PTutorial3)
            {
                UIManager.ins.ShowPopup(Popup.None);
            }

            pressedTime += Time.deltaTime;

            if (PointerId >= 0 && PointerId < Input.touches.Length)
            {
                TouchDist = Input.touches[PointerId].position - PointerOld;
                PointerOld = Input.touches[PointerId].position;
            }
            else
            {
                TouchDist = new Vector2(Input.mousePosition.x, Input.mousePosition.y) - PointerOld;
                PointerOld = Input.mousePosition;
            }
        }
        else
        {
            TouchDist = new Vector2();
        }
    }

    private IEnumerator IETutorial1()
    {
        PlayerController player = GameController.ins.player;
        yield return new WaitForSeconds(2f);
        UIManager.ins.ShowPopup(Popup.PTutorial2);
        Pressed = false;
        player.animator.SetTrigger("Catching");
        player.animator.SetTrigger("CatchCancel");

        GameController.ins.isPlaying = false;
        player.vMainCam.transform.DOLookAt(GameController.ins.listEnemyMonster[0].transform.position + Vector3.up * 0.5f, 0.5f);

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Pressed = true;
        pressedTime = 0;
        PointerId = eventData.pointerId;
        PointerOld = eventData.position;
        if (GameController.ins.chosenMonster < 0) // using empty monster ball
        {
            PlayerController pc = GameController.ins.player;
            if (!pc.animator.GetCurrentAnimatorStateInfo(0).IsName(pc.clipHoldMons.name)) return;

            int r = GameController.ins.ReadInt(GameController.ins.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14);
            if (r == 1 || r == 2)
            {
                if (GameController.ins.isInSpecialWorld)
                {
                    GameController.ins.ResetAnimTrigger();
                    pc.animator.SetTrigger("StartCatching");
                }
                else
                {
                    return;
                }
            }
            else
            {
                GameController.ins.ResetAnimTrigger();
                pc.animator.SetTrigger("StartCatching");
            }
        }
    }


    public void OnPointerUp(PointerEventData eventData)
    {
        Pressed = false;
        PlayerController pc = GameController.ins.player;

        if (pressedTime < 0.25f)
        {
            GameController.ins.player.animator.SetTrigger("Catching");
            GameController.ins.player.animator.SetTrigger("CatchCancel");
        }

        if (GameController.ins.chosenMonster > -1)
        {
            if (pc.animator.GetCurrentAnimatorStateInfo(0).IsName(pc.clipHoldMons.name))
            {
                pc.animator.SetTrigger("ThrowMons");
            }
        }
        else
        {
            if (!pc.animator.GetCurrentAnimatorStateInfo(0).IsName(pc.clipCatch2.name)) return;

            if (!GameController.ins.isCatching)
            {
                GameController.ins.player.animator.SetTrigger("Catching");
                GameController.ins.player.animator.SetTrigger("CatchCancel");
            }
        }
    }

}