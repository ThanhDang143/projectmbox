
/* Copyright © @ThanhDV */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonsterInfo : MonoBehaviour
{
    public Sprite[] avatar = new Sprite[2];
    public int ID;
    public int IDMonsterEvolve = -1;
    public string monsterName = "Monster";
    public float moveSpeed;
    public float scaleIndex;
    public Rigidbody rb;
    public Image imgFillHealthBar;
    public LayerMask groundLayerMask;
    public bool isGrounded;

    [Header("Attach Info")]
    public float scanPlayerZone;
    public float attackZone;
    public float dmg;

    [Header("Health Info")]
    public Slider healthBar;
    public float maxHealth;
    [HideInInspector] public float health;

    [Header("Animation Info")]
    public Animator animator;
    public AnimationClip animIdle;
    public AnimationClip animRun;
    public AnimationClip animAttack;

    [Header("VFX")]
    public Transform dyingFX;
    public Transform catchingFX1;
    public Transform catchingFX2;
    public Transform catchingFX3;
    public Transform attackFX;

    [Header("World")]
    public int[] world = new int[3];

    [Header("Debug")]
    public bool isCatching;

    public void PlayAnim(AnimationClip anim, float speed = 1)
    {
        animator.speed = speed;
        animator.Play(anim.name);
    }

    public void PlayAnim(string anim, float speed = 1)
    {
        animator.speed = speed;
        animator.Play(anim);
    }

    public void SetAnimTrigger(string triggerName, float speed = 1)
    {
        animator.SetTrigger(triggerName);
        if (triggerName == "move")
        {
            animator.speed = speed * Random.Range(1.35f, 1.6f);
        }
        else if (triggerName == "attack")
        {
            if (ID == 44 || ID == 207 || ID == 208 || ID == 209 || ID == 210)
            {
                animator.speed = speed / 1.65f;
            }
            else
            {
                animator.speed = speed;
            }
        }
        else
        {
            animator.speed = speed;
        }
    }

    public void SetHealthInfo()
    {
        Image imgSubHealth = imgFillHealthBar.transform.parent.GetChild(1).GetComponent<Image>();

        if (transform.CompareTag(StaticString.tagPlayerMonster) && PlayerPrefs.GetInt(StaticString.saveBuff) == 0)
        {
            health = maxHealth * 1.2f * 1.5f;
            healthBar.maxValue = maxHealth * 1.4f * 1.5f;
            healthBar.value = healthBar.maxValue;
        }
        else
        {
            if (transform.CompareTag(StaticString.tagPlayerMonster))
            {
                health = maxHealth * 1.2f;
                healthBar.maxValue = maxHealth * 1.2f;
                healthBar.value = healthBar.maxValue;
            }
            else
            {
                health = maxHealth;
                healthBar.maxValue = maxHealth;
                healthBar.value = healthBar.maxValue;
            }
        }

        imgSubHealth.type = Image.Type.Tiled;


        imgFillHealthBar.sprite = GameController.ins.whiteHealthBar;
        if (transform.CompareTag(StaticString.tagPlayerMonster))
        {
            if (healthBar.maxValue < 20)
            {
                imgFillHealthBar.color = new Color(0, 248, 255, 255);
                imgSubHealth.pixelsPerUnitMultiplier = health / 20;
            }
            else if (healthBar.maxValue >= 20 && healthBar.maxValue < 40)
            {
                imgFillHealthBar.color = new Color(0, 255, 140, 255);
                imgSubHealth.pixelsPerUnitMultiplier = health / 40f;
            }
            else if (healthBar.maxValue >= 40 && healthBar.maxValue < 60)
            {
                imgFillHealthBar.color = new Color(0, 255, 0, 255);
                imgSubHealth.pixelsPerUnitMultiplier = health / 80f;
            }
            else
            {
                imgFillHealthBar.color = new Color(200, 255, 0, 255);
                imgSubHealth.pixelsPerUnitMultiplier = health / 120f;
            }
        }
        else
        {
            if (healthBar.maxValue < 20)
            {
                imgFillHealthBar.color = new Color(255, 120, 0, 255);
                imgSubHealth.pixelsPerUnitMultiplier = health / 20f;
            }
            else if (healthBar.maxValue >= 20 && healthBar.maxValue < 40)
            {
                imgFillHealthBar.color = new Color(255, 0, 0, 255);
                imgSubHealth.pixelsPerUnitMultiplier = health / 40f;
            }
            else if (healthBar.maxValue >= 40 && healthBar.maxValue < 60)
            {
                imgFillHealthBar.color = new Color(255, 0, 175, 255);
                imgSubHealth.pixelsPerUnitMultiplier = health / 80f;
            }
            else
            {
                imgFillHealthBar.color = new Color(165, 0, 255, 255);
                imgSubHealth.pixelsPerUnitMultiplier = health / 120f;
            }
        }
    }

    private void Update()
    {
        RaycastHit raycastHit;
        Vector3 originRaycast = new Vector3(transform.position.x, transform.position.y + 0.1f, transform.position.z);

        Physics.Raycast(originRaycast, Vector3.down, out raycastHit, 0.2f, groundLayerMask);
        // Debug.DrawRay(originRaycast, Vector3.down, Color.red, 0.2f);
        if (raycastHit.collider == null)
        {
            isGrounded = false;
        }
        else
        {
            isGrounded = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (transform.CompareTag(StaticString.tagPlayerMonster))
        {
            if (other.CompareTag(StaticString.tagSea))
            {
                if (transform.GetComponent<PlayerMonster>().btnControllThisMonster == null)
                {
                    transform.GetComponent<PlayerMonster>().OnDespawn();
                    return;
                }

                transform.GetComponent<PlayerMonster>().btnControllThisMonster.ChooseMonster();
            }
        }

        if (transform.CompareTag(StaticString.tagEnemyMonster))
        {
            if (other.CompareTag(StaticString.tagSea))
            {
                transform.GetComponent<EnemyMonster>().OnDespawn();
            }
        }
    }
}
