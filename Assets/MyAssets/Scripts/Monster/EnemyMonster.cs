
/* Copyright © @ThanhDV */

using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Pathfinding;
using UnityEngine.UI;

public class EnemyMonster : MonoBehaviour
{
    private GameController gameController;
    [HideInInspector] public MonsterInfo info;
    private AIDestinationSetter aIDestination;
    private AIPath aIPath;
    private Vector3 direction;
    private EnemyMonsterState enemyState;
    private List<float> listTargetDistance;
    private float tempAttackZone;
    private float tempMoveSpeed;

    public void OnSpawn()
    {
        gameController = GameController.ins;

        gameController.listEnemyMonster.Add(this);

        transform.tag = StaticString.tagEnemyMonster;

        info = (MonsterInfo)GetComponent("MonsterInfo");

        aIDestination = (AIDestinationSetter)GetComponent("AIDestinationSetter");
        aIPath = (AIPath)GetComponent("AIPath");

        if (info.ID >= 162 && info.ID <= 200)
        {
            transform.localScale = Vector3.one * (info.scaleIndex - (info.scaleIndex * 0.25f)) * 1.5f;
        }
        else
        {
            transform.localScale = Vector3.one * info.scaleIndex * 1.5f;
        }

        info.SetHealthInfo();

        if (gameController.isEvolving)
        {
            info.healthBar.transform.parent.gameObject.SetActive(false);
            StartCoroutine(IEEvolved());
            return;
        }
        else
        {
            info.healthBar.gameObject.SetActive(true);
            info.healthBar.transform.parent.gameObject.SetActive(true);
        }

        info.isCatching = false;
        gameController.isCatching = false;

        tempAttackZone = info.attackZone;
        tempMoveSpeed = info.moveSpeed;

        if (gameController.listPlayerMonster.Count == 0 || !gameController.isPlaying)
        {
            aIDestination.target = gameController.player.transform;
        }
        else
        {
            aIDestination.target = NearestTarget();
        }

        StartCoroutine(IEFindTarget());

        SetState(EnemyMonsterState.Idle);
    }

    public void OnDespawn(bool playAudio = true)
    {
        if (!gameObject.activeSelf) return;
        Vector3 dyingFXPos = transform.GetChild(1).GetComponent<SkinnedMeshRenderer>().bounds.center;
        Transform fx = PoolVFX.pool.Spawn(info.dyingFX, dyingFXPos, Quaternion.Euler(-90, 0, 0));
        PoolVFX.pool.Despawn(fx, 3);
        if (playAudio) AudioManager.ins.PlayOther("Dead" + Random.Range(0, 5));
        transform.DOScale(Vector3.zero, 0.1f).OnComplete(() =>
        {
            info.attackZone = tempAttackZone;
            gameController.listEnemyMonster.Remove(this);

            if (!gameObject.activeSelf) return;

            if (!gameController.isEvolving)
            {
                PoolEnemyMonster.pool.Despawn(transform);
                gameController.CheckWin();
            }
        });
    }

    private IEnumerator IEEvolved()
    {
        info.rb.useGravity = false;
        float animAttackLength = info.animAttack.length * 2;
        transform.DOMoveY(1.2f, 3f);
        AudioManager.ins.PlayOther("EvolveSuccess");
        yield return new WaitForSeconds(3f);
        info.PlayAnim("Attack");
        yield return new WaitForSeconds(animAttackLength);
        info.PlayAnim("Idle");
        yield return new WaitForSeconds(animAttackLength / 1.5f);
        Vector3 dyingFXPos = transform.GetChild(1).GetComponent<SkinnedMeshRenderer>().bounds.center;
        Transform fx = PoolVFX.pool.Spawn(info.dyingFX, dyingFXPos, Quaternion.Euler(-90, 0, 0));
        float fxTime = fx.GetComponent<ParticleSystem>().main.duration;
        PoolVFX.pool.Despawn(fx, 3);
        transform.DOScale(0, 0.35f);
        GameController.ins.Vibrate();
        yield return new WaitForSeconds(0.4f);
        gameController.isEvolving = false;
        gameController.SaveMonsterCollection(info.ID);
        gameController.ResetMonsterBallBoss();
        gameController.LoadMonsterView();
        gameController.LoadLevel();
        gameController.player.handRenderer.gameObject.SetActive(true);
        gameController.listEnemyMonster.Remove(this);
        UIManager.ins.ShowScreen(ScreenX.SHome);
        Destroy(gameObject, 0.1f);
    }

    public void OnCatching()
    {
        gameController.player.animator.SetTrigger("Catching");
        SetState(EnemyMonsterState.Run);
        info.isCatching = true;
        gameController.isCatching = true;
        foreach (BtnChoseMonster btn in UIManager.ins.monsterView.allBallButton)
        {
            btn.thisBtn.interactable = false;
        }
        Transform fx1 = PoolVFX.pool.Spawn(info.catchingFX1, transform.position, Quaternion.Euler(-90, 0, 0));
        AudioManager.ins.PlayOther("CatchingSFX1");
        ParticleSystem fx1ps = fx1.GetComponent<ParticleSystem>();
        PoolVFX.pool.Despawn(fx1, fx1ps.main.duration);

        Transform fx2 = PoolVFX.pool.Spawn(info.catchingFX2, transform.position, Quaternion.Euler(-90, 0, 0));
        transform.DOScale(Vector3.one * 0.0001f, fx1ps.main.duration - 0.5f).OnComplete(() =>
        {
            if (!gameObject.activeSelf) return;

            fx2.DOMove(transform.position, 0.75f).OnComplete(() =>
            {
                PoolVFX.pool.Despawn(fx2);
                AudioManager.ins.PlayOther("Pop", 0.5f);
                Transform fx3 = PoolVFX.pool.Spawn(info.catchingFX3, transform.position, Quaternion.Euler(-90, 0, 0));
                ParticleSystem fx3ps = fx3.GetComponent<ParticleSystem>();
                PoolVFX.pool.Despawn(fx3, fx3ps.main.duration);

                if (gameController.isInSpecialWorld)
                {
                    if (gameController.RandomByPercent(20)) // catch fail
                    {
                        CatchFail(fx3ps.main.duration);
                    }
                    else // catch successful
                    {
                        CatchSuccessful();
                    }
                }
                else
                {
                    int readMap = gameController.ReadInt(gameController.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14);
                    if (readMap == 1 || readMap == 2)
                    {
                        CatchFail(fx3ps.main.duration);
                    }
                    else
                    {
                        if (info.IDMonsterEvolve == -1) // catch fail
                        {
                            if (gameController.RandomByPercent(20))
                            {
                                CatchFail(fx3ps.main.duration);
                            }
                            else
                            {
                                CatchSuccessful();
                            }
                        }
                        else // catch successful
                        {
                            CatchSuccessful();
                        }
                    }
                }
            });
        });
    }

    private void CatchSuccessful()
    {
        if (!gameController.isPlaying)
        {
            gameController.isPlaying = true;
        }

        info.moveSpeed = tempMoveSpeed;
        info.attackZone = tempAttackZone;
        gameController.listEnemyMonster.Remove(this);

        Transform playerTrans = gameController.player.transform;
        Transform fx;
        fx = Instantiate(info.attackFX.gameObject, transform.position, transform.rotation).transform;

        gameController.chosenMonster = info.ID;
        gameController.player.animator.SetTrigger("CatchSuccessful");
        AudioManager.ins.PlayOther("Caught");
        fx.DOJump(playerTrans.position - playerTrans.right - playerTrans.up, 1f, 1, 1.25f);
        UIManager.ins.monsterView.AddMonsterToBtn(info.monsterName, info.ID, info.avatar[0]);
        gameController.currentBtnChoseMonster.OnSpawn();
        gameController.currentBtnChoseMonster.catchEffect.Stop();
        gameController.currentBtnChoseMonster.catchEffect.Play();
        gameController.SaveMonsterCollection(info.ID);
        gameController.SaveMonsterView();
        Destroy(fx.gameObject);
        info.isCatching = false;
        gameController.isCatching = false;
        foreach (BtnChoseMonster btn in UIManager.ins.monsterView.allBallButton)
        {
            btn.thisBtn.interactable = false;
        }

        transform.DOScaleY(transform.localScale.y, 1f).OnComplete(() =>
        {
            if (gameObject.activeSelf)
            {
                PoolEnemyMonster.pool.Despawn(transform);
            }

            if (PlayerPrefs.GetInt(StaticString.checkFirstTimePlay) == 1)
            {
                UIManager.ins.ShowPopup(Popup.PTutorial3);
            }
            else
            {
                gameController.CheckWin();
            }
        });
    }

    private void CatchFail(float resizeTime = 0.25f)
    {
        info.isCatching = false;
        gameController.isCatching = false;
        foreach (BtnChoseMonster btn in UIManager.ins.monsterView.allBallButton)
        {
            btn.thisBtn.interactable = false;
        }
        transform.DOScale(Vector3.one * info.scaleIndex * 1.5f, resizeTime);
        info.moveSpeed = tempMoveSpeed;
        info.attackZone = tempAttackZone;
        gameController.player.animator.SetTrigger("CatchCancel");
    }

    private Transform NearestTarget()
    {
        listTargetDistance = new List<float>();

        for (int i = 0; i < gameController.listPlayerMonster.Count; i++)
        {
            float dis = Vector3.Distance(transform.position, gameController.listPlayerMonster[i].transform.position);
            listTargetDistance.Add(dis);
        }
        float minDis = Mathf.Min(listTargetDistance.ToArray());

        return gameController.listPlayerMonster[listTargetDistance.IndexOf(minDis)].transform;
    }

    private IEnumerator IEFindTarget()
    {
        while (true)
        {
            if (gameController.listPlayerMonster.Count == 0 || !gameController.isPlaying)
            {
                aIDestination.target = gameController.player.transform;
            }
            else
            {
                aIDestination.target = NearestTarget();
            }
            yield return new WaitForSeconds(0.25f);
        }
    }

    public void ManualUpdate()
    {
        if (gameController.isEvolving || info.isCatching || Vector3.Distance(gameController.player.transform.position, transform.position) >= info.scanPlayerZone || gameController.isChoosingBuff) return;


        if (gameController.isPlaying && !gameController.isMoving && !gameController.isExchangeMap)
        {
            if ((PlayerPrefs.GetInt(StaticString.checkFirstTimePlay) == 1 && GameController.ins.LoadMonsterCollection().Count <= 0) || UIManager.ins.currentPopup == Popup.PTutorial3 || UIManager.ins.currentPopup == Popup.PTutorial4)
            {
                MoveToTarget(0);
            }
            else
            {
                CheckDistanceToSetState();

                Action();
            }
        }
        else
        {
            MoveToTarget(0);
        }
    }

    private void CheckDistanceToSetState()
    {
        if (info.isCatching) return;

        float disToTarget;
        try
        {
            Vector3 thisPos = new Vector3(transform.position.x, 0, transform.position.z);
            Vector3 targetPos = new Vector3(aIDestination.target.position.x, 0, aIDestination.target.position.z);
            disToTarget = Vector3.Distance(thisPos, targetPos);
        }
        catch (System.Exception)
        {
            aIDestination.target = gameController.player.transform;
            Vector3 thisPos = new Vector3(transform.position.x, 0, transform.position.z);
            Vector3 targetPos = new Vector3(aIDestination.target.position.x, 0, aIDestination.target.position.z);
            disToTarget = Vector3.Distance(transform.position, aIDestination.target.position);
        }

        if (aIDestination.target.CompareTag(StaticString.tagPlayerMonster))
        {
            info.attackZone = tempAttackZone;
            if (disToTarget > info.attackZone + info.scaleIndex * 1.5f / 5f)
            {
                SetState(EnemyMonsterState.Run);
                return;
            }

            if (disToTarget <= info.attackZone + info.scaleIndex * 1.5f / 5f)
            {
                SetState(EnemyMonsterState.Attack);
                return;
            }
        }
        else if (aIDestination.target.CompareTag(StaticString.tagPlayer))
        {
            info.attackZone = 4f;
            if (disToTarget > info.attackZone)
            {
                SetState(EnemyMonsterState.Run);
                return;
            }

            if (disToTarget <= info.attackZone)
            {
                SetState(EnemyMonsterState.Attack);
                return;
            }
        }

    }

    private void Action()
    {
        if (enemyState == EnemyMonsterState.Idle)
        {
            MoveToTarget(0);
            return;
        }

        if (enemyState == EnemyMonsterState.Run)
        {
            MoveToTarget(info.moveSpeed);
        }

        if (enemyState == EnemyMonsterState.Attack)
        {
            MoveToTarget(info.moveSpeed / 1000);
            return;
        }
    }

    public void AttackEvents()
    {
        if (!gameController.isEvolving)
        {
            if ((PlayerPrefs.GetInt(StaticString.checkFirstTimePlay) == 1 && GameController.ins.LoadMonsterCollection().Count <= 0) || UIManager.ins.currentPopup == Popup.PTutorial3 || UIManager.ins.currentPopup == Popup.PTutorial4) return;

            if (aIDestination.target.CompareTag(StaticString.tagPlayer))
            {
                if (Vector3.Distance(transform.position, aIDestination.target.position) <= 6f)
                {
                    gameController.Lose();
                }
                else
                {
                    SetState(EnemyMonsterState.Run);
                }
            }

            if (aIDestination.target.CompareTag(StaticString.tagPlayerMonster))
            {
                MonsterInfo targetInfo = aIDestination.target.GetComponent<MonsterInfo>();
                Vector3 centerPos = transform.GetChild(1).GetComponent<Renderer>().bounds.center;

                //Take Dmg
                if (info.attackZone >= 5)
                {
                    AudioManager.ins.PlayOther("Attack");
                }
                else
                {
                    AudioManager.ins.PlayOther("MeleeAttack");
                }
                // if (info.attackZone <= 3) // Monster đánh gần 
                // {
                //     DelayTakeDmg(targetInfo);
                // }
                // else // Monster đánh xa
                // {
                // Spawn Attack Effect
                Instantiate(info.attackFX.gameObject, centerPos, transform.rotation).GetComponent<ProjectileMover>().OnSpawn(aIDestination.target, transform);
                DelayTakeDmg(targetInfo, 0.2f);
                // }
            }
        }
        else
        {
            if (info.attackZone >= 5)
            {
                AudioManager.ins.PlayOther("Attack");
            }
            else
            {
                AudioManager.ins.PlayOther("MeleeAttack");
            }
            gameController.player.ShakePlayer();
        }
    }

    private void DelayTakeDmg(MonsterInfo targetInfo, float delay = 0)
    {
        transform.DOScale(transform.localScale, delay).OnComplete(() =>
        {
            Vector3 thisPos = new Vector3(transform.position.x, 0, transform.position.z);
            Vector3 targetPos = new Vector3(aIDestination.target.position.x, 0, aIDestination.target.position.z);
            float dis = Vector3.Distance(thisPos, targetPos);
            if (dis > info.attackZone + info.scaleIndex * 1.5f / 5f)
            {
                targetInfo.health -= 0;
            }
            else
            {
                if (PlayerPrefs.GetInt(StaticString.saveLevel) == 3 || PlayerPrefs.GetInt(StaticString.saveLevel) == 6)
                {
                    targetInfo.health -= info.dmg * 0.45f;
                }
                else if (PlayerPrefs.GetInt(StaticString.saveLevel) == 9 || PlayerPrefs.GetInt(StaticString.saveLevel) == 12 || PlayerPrefs.GetInt(StaticString.saveLevel) == 16)
                {
                    targetInfo.health -= info.dmg * 0.55f;
                }
                else if (PlayerPrefs.GetInt(StaticString.saveLevel) == 15 || PlayerPrefs.GetInt(StaticString.saveLevel) == 19 || PlayerPrefs.GetInt(StaticString.saveLevel) == 32)
                {
                    targetInfo.health -= info.dmg * 0.65f;
                }
                else if (PlayerPrefs.GetInt(StaticString.saveLevel) == 22 || PlayerPrefs.GetInt(StaticString.saveLevel) == 25 || PlayerPrefs.GetInt(StaticString.saveLevel) == 28)
                {
                    targetInfo.health -= info.dmg * 0.75f;
                }
                else
                {
                    targetInfo.health -= info.dmg * 0.85f; ;
                }
            }
            targetInfo.healthBar.value = targetInfo.health;

            if (PlayerPrefs.GetInt(StaticString.checkFirstTimePlay) == 1 && targetInfo.health <= targetInfo.maxHealth / 1.5f)
            {
                UIManager.ins.ShowPopup(Popup.PTutorial4);
            }

            if (targetInfo.health <= 0)
            {
                PlayerMonster targetController = aIDestination.target.GetComponent<PlayerMonster>();
                if (targetController.onDespawn) return;

                if (targetController.btnControllThisMonster != null)
                {
                    targetController.btnControllThisMonster.thisText.text = "X";
                    // targetController.btnControllThisMonster.imgAvatar.sprite = targetInfo.avatar[1];
                    // targetController.btnControllThisMonster.imgAvatar.SetNativeSize();
                    targetController.btnControllThisMonster.thisBtn.image.color = Color.red;
                    targetController.btnControllThisMonster.monsterSpawned = null;
                    targetController.btnControllThisMonster.isSpawnedObj = true;
                }

                targetController.OnDespawn();

                PlayerController pc = GameController.ins.player;

                transform.DOScale(transform.localScale, 0.15f).OnComplete(() =>
                {
                    if (gameController.listPlayerMonster.Count > 0)
                    {
                        aIDestination.target = NearestTarget();
                    }
                    else
                    {
                        aIDestination.target = gameController.player.transform;
                    }
                });
            }
            return;
        });
    }

    private void SetState(EnemyMonsterState es)
    {
        if (enemyState == es) return;

        enemyState = es;

        if (es == EnemyMonsterState.Idle)
        {
            info.SetAnimTrigger("idle");
            return;
        }

        if (es == EnemyMonsterState.Run)
        {
            info.SetAnimTrigger("move");
            return;
        }

        if (es == EnemyMonsterState.Attack)
        {
            info.SetAnimTrigger("attack", 0.8f);
            return;
        }
    }

    public void MoveToTarget(float speed)
    {
        if (speed == 0)
        {
            aIPath.maxSpeed = speed;
            direction = gameController.player.transform.position - transform.position;
            float singleStep = 4 * Time.deltaTime;
            Vector3 newDirection = Vector3.RotateTowards(transform.forward, direction, singleStep, 0.0f);
            Vector3 dir = new Vector3(newDirection.x, 0, newDirection.z);
            transform.rotation = Quaternion.LookRotation(dir);
            return;
        }

        if (enemyState == EnemyMonsterState.Idle || enemyState == EnemyMonsterState.Attack)
        {
            // Determine which direction to rotate towards
            direction = aIDestination.target.position - transform.position;

            // The step size is equal to speed times frame time.
            float singleStep = 4 * Time.deltaTime;

            // Rotate the forward vector towards the target direction by one step
            Vector3 newDirection = Vector3.RotateTowards(transform.forward, direction, singleStep, 0.0f);
            Vector3 dir = new Vector3(newDirection.x, 0, newDirection.z);

            // Calculate a rotation a step closer to the target and applies rotation to this object
            transform.rotation = Quaternion.LookRotation(dir);
        }

        if (aIDestination.target.CompareTag(StaticString.tagPlayerMonster))
        {
            // transform.position += d * speed * Time.deltaTime;
            aIPath.maxSpeed = speed;
        }
        else
        {
            // transform.position += d * (speed / 1.5f) * Time.deltaTime;
            aIPath.maxSpeed = speed / 1.5f;
        }
    }
}

public enum EnemyMonsterState
{
    Idle,
    Run,
    Attack
}
