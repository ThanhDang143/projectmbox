
/* Copyright © @ThanhDV */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Pathfinding;
using UnityEngine.UI;

public class PlayerMonster : MonoBehaviour
{
    private GameController gameController;
    [HideInInspector] public MonsterInfo info;
    private AIDestinationSetter aIDestination;
    private AIPath aIPath;
    private Vector3 direction;
    private PlayerMonsterState playerMonsterState;
    private List<float> listTargetDistance;
    public BtnChoseMonster btnControllThisMonster;
    private IEnumerator findTarget;
    [HideInInspector] public bool onDespawn;

    public void OnSpawn()
    {
        onDespawn = false;

        gameController = GameController.ins;

        gameController.listPlayerMonster.Add(this);

        gameController.currentBtnChoseMonster.monsterSpawned = transform;

        btnControllThisMonster = gameController.currentBtnChoseMonster;

        transform.tag = StaticString.tagPlayerMonster;

        info = GetComponent<MonsterInfo>();

        aIDestination = (AIDestinationSetter)GetComponent("AIDestinationSetter");
        aIPath = (AIPath)GetComponent("AIPath");

        if (info.ID >= 162 && info.ID <= 200)
        {
            transform.localScale = Vector3.one * (info.scaleIndex - (info.scaleIndex * 0.25f)) * 1.5f;
        }
        else
        {
            transform.localScale = Vector3.one * info.scaleIndex * 1.5f;
        }

        info.SetHealthInfo();

        if (gameController.isEvolving)
        {
            info.healthBar.transform.parent.gameObject.SetActive(false);
            StartCoroutine(IEEvolve());
            return;
        }
        else
        {
            info.healthBar.gameObject.SetActive(true);
        }

        if (gameController.listEnemyMonster.Count == 0 || !gameController.isPlaying)
        {
            aIDestination.target = null;
        }
        else
        {
            aIDestination.target = NearestTarget();
        }

        findTarget = IEFindTarget();

        StartCoroutine(findTarget);

        // if (aIDestination.target == null)
        // {
        //     SetState(PlayerMonsterState.Idle);
        // }
        // else
        // {
        SetState(PlayerMonsterState.Run);
        info.animator.ResetTrigger("move");
        try
        {
            info.PlayAnim("Move");
        }
        catch (System.Exception)
        {
            info.PlayAnim("move");
        }
        // }

        btnControllThisMonster.imgAvatar.sprite = info.avatar[1];
        btnControllThisMonster.imgAvatar.SetNativeSize();
    }

    public void OnSpawnBuff()
    {
        onDespawn = false;

        gameController = GameController.ins;

        gameController.listPlayerMonster.Add(this);

        transform.tag = StaticString.tagPlayerMonster;

        info = GetComponent<MonsterInfo>();

        aIDestination = (AIDestinationSetter)GetComponent("AIDestinationSetter");
        aIPath = (AIPath)GetComponent("AIPath");

        if (info.ID >= 162 && info.ID <= 200)
        {
            transform.localScale = Vector3.one * (info.scaleIndex - (info.scaleIndex * 0.25f)) * 1.5f;
        }
        else
        {
            transform.localScale = Vector3.one * info.scaleIndex * 1.5f;
        }

        info.SetHealthInfo();

        if (gameController.isEvolving)
        {
            info.healthBar.transform.parent.gameObject.SetActive(false);
            StartCoroutine(IEEvolve());
            return;
        }
        else
        {
            info.healthBar.gameObject.SetActive(true);
        }

        if (gameController.listEnemyMonster.Count == 0 || !gameController.isPlaying)
        {
            aIDestination.target = null;
        }
        else
        {
            aIDestination.target = NearestTarget();
        }

        findTarget = IEFindTarget();

        StartCoroutine(findTarget);

        // if (aIDestination.target == null)
        // {
        //     SetState(PlayerMonsterState.Idle);
        // }
        // else
        // {
        SetState(PlayerMonsterState.Run);
        info.animator.ResetTrigger("move");
        try
        {
            info.PlayAnim("Move");
        }
        catch (System.Exception)
        {
            info.PlayAnim("move");
        }
        // }
    }

    public void AutoOnSpawn(BtnChoseMonster _btnControllThisMonster)
    {
        onDespawn = false;

        gameController = GameController.ins;

        gameController.listPlayerMonster.Add(this);

        _btnControllThisMonster.monsterSpawned = transform;
        _btnControllThisMonster.isSpawnedObj = true;
        btnControllThisMonster = _btnControllThisMonster;

        transform.tag = StaticString.tagPlayerMonster;

        info = GetComponent<MonsterInfo>();

        aIDestination = (AIDestinationSetter)GetComponent("AIDestinationSetter");
        aIPath = (AIPath)GetComponent("AIPath");

        if (info.ID >= 162 && info.ID <= 200)
        {
            transform.localScale = Vector3.one * (info.scaleIndex - (info.scaleIndex * 0.25f)) * 1.5f;
        }
        else
        {
            transform.localScale = Vector3.one * info.scaleIndex * 1.5f;
        }

        info.SetHealthInfo();

        if (gameController.isEvolving)
        {
            info.healthBar.transform.parent.gameObject.SetActive(false);
            StartCoroutine(IEEvolve());
            return;
        }
        else
        {
            info.healthBar.gameObject.SetActive(true);
        }

        if (gameController.listEnemyMonster.Count == 0 || !gameController.isPlaying)
        {
            aIDestination.target = null;
        }
        else
        {
            aIDestination.target = NearestTarget();
        }

        findTarget = IEFindTarget();

        StartCoroutine(findTarget);

        // if (aIDestination.target == null)
        // {
        //     SetState(PlayerMonsterState.Idle);
        // }
        // else
        // {
        SetState(PlayerMonsterState.Run);
        info.animator.ResetTrigger("move");
        try
        {
            info.PlayAnim("Move");
        }
        catch (System.Exception)
        {
            info.PlayAnim("move");
        }
        // }

        btnControllThisMonster.imgAvatar.sprite = info.avatar[1];
        btnControllThisMonster.imgAvatar.SetNativeSize();
    }

    public void OnDespawn(bool playAudio = true)
    {
        if (!gameObject.activeSelf) return;

        if (playAudio) AudioManager.ins.PlayOther("Dead" + Random.Range(0, 5));
        if (!gameController.isEvolving)
        {
            Vector3 dyingFXPos = transform.GetChild(1).GetComponent<SkinnedMeshRenderer>().bounds.center;
            Transform fx = PoolVFX.pool.Spawn(info.dyingFX, dyingFXPos, Quaternion.Euler(-90, 0, 0));
            PoolVFX.pool.Despawn(fx, 3);
            onDespawn = true;
            transform.DOScale(Vector3.zero, 0.1f).OnComplete(() =>
            {
                gameController.listPlayerMonster.Remove(this);

                if (!gameObject.activeSelf) return;
                StopCoroutine(findTarget);
                onDespawn = false;
                if (btnControllThisMonster != null)
                {
                    btnControllThisMonster.monsterSpawned = null;
                    GameController.ins.Vibrate();
                    PoolPlayerMonster.pool.Despawn(transform);
                }
                else
                {
                    SetArenaViewBGToRed();
                    GameController.ins.Vibrate();
                    Destroy(gameObject);
                }
            });
        }
        else
        {
            gameController.listPlayerMonster.Remove(this);
            if (btnControllThisMonster != null)
            {
                btnControllThisMonster.monsterSpawned = null;
            }
            Destroy(gameObject);
        }

    }

    public void SetArenaViewBGToRed()
    {
        if (gameController.isInArena)
        {
            for (int i = 0; i < UIManager.ins.arenaMonsterView.imgAvatarMonster.Length; i++)
            {
                int getAvatar = int.Parse(UIManager.ins.arenaMonsterView.imgAvatarMonster[i].name);
                Image BGOfAvatar = UIManager.ins.arenaMonsterView.imgAvatarMonster[i].transform.parent.GetComponent<Image>();
                if (BGOfAvatar.color != Color.red && getAvatar == info.ID)
                {
                    BGOfAvatar.color = Color.red;
                    return;
                }
            }
        }
    }

    private IEnumerator IEEvolve()
    {
        info.rb.useGravity = false;
        transform.DOMoveY(1.2f, 3f);
        yield return new WaitForSeconds(3);
        info.PlayAnim("Attack");
        yield return new WaitForSeconds(info.animAttack.length * 2f);
        info.PlayAnim("Idle");
        Transform fx1 = PoolVFX.pool.Spawn(info.catchingFX1, transform.position + Vector3.up * 0.25f, Quaternion.Euler(-90, 0, 0));
        AudioManager.ins.PlayOther("Evolve");
        ParticleSystem fx1ps = fx1.GetComponent<ParticleSystem>();
        PoolVFX.pool.Despawn(fx1, fx1ps.main.duration);
        transform.DOScale(Vector3.zero, fx1ps.main.duration * 0.35f);
        yield return new WaitForSeconds(0.5f);
        Transform fx2 = PoolVFX.pool.Spawn(info.catchingFX2, transform.position, Quaternion.Euler(-90, 0, 0));
        yield return new WaitForSeconds(fx1ps.main.duration * 0.25f);
        fx2.DOMoveY(gameController.currentMap.enemiesPos[0].position.y, 3f);
        yield return new WaitForSeconds(3.5f);
        AudioManager.ins.StopOther("Evolve");
        AudioManager.ins.PlayOther("Pop", 0.5f);
        Transform fx3 = PoolVFX.pool.Spawn(info.catchingFX3, fx2.position, Quaternion.Euler(-90, 0, 0));
        ParticleSystem fx3ps = fx3.GetComponent<ParticleSystem>();
        PoolVFX.pool.Despawn(fx3, fx3ps.main.duration);
        PoolVFX.pool.Despawn(fx2);
        gameController.InstantiateEnemyMonster(gameController.FindMonster(info.IDMonsterEvolve), fx3.position + Vector3.down * 0.5f, Quaternion.Euler(0, 180, 0));
        OnDespawn(false);
    }

    private Transform NearestTarget()
    {
        listTargetDistance = new List<float>();

        for (int i = 0; i < gameController.listEnemyMonster.Count; i++)
        {
            float dis = Vector3.Distance(transform.position, gameController.listEnemyMonster[i].transform.position);
            listTargetDistance.Add(dis);
        }
        float minDis = Mathf.Min(listTargetDistance.ToArray());

        if (!gameController.listEnemyMonster[listTargetDistance.IndexOf(minDis)].info.isCatching)
        {
            return gameController.listEnemyMonster[listTargetDistance.IndexOf(minDis)].transform;
        }
        else
        {
            if (gameController.listEnemyMonster.Count > 1)
            {
                try
                {
                    return gameController.listEnemyMonster[listTargetDistance.IndexOf(minDis) - 1].transform;
                }
                catch (System.Exception)
                {
                    return gameController.listEnemyMonster[listTargetDistance.IndexOf(minDis) + 1].transform;
                }
            }
            else
            {
                return null;
            }
        }
    }

    public void AttackEvents()
    {
        if (!gameController.isEvolving)
        {
            if ((PlayerPrefs.GetInt(StaticString.checkFirstTimePlay) == 1 && GameController.ins.LoadMonsterCollection().Count <= 0) || UIManager.ins.currentPopup == Popup.PTutorial3 || UIManager.ins.currentPopup == Popup.PTutorial4) return;
            if (!aIDestination.target) return;
            if (!aIDestination.target.gameObject.activeSelf)
            {
                CheckDistanceToSetState();
                return;
            }

            if (info.attackZone >= 5)
            {
                AudioManager.ins.PlayOther("Attack");
            }
            else
            {
                AudioManager.ins.PlayOther("MeleeAttack");
            }

            if (aIDestination.target.CompareTag(StaticString.tagEnemyMonster))
            {
                MonsterInfo targetInfo = aIDestination.target.GetComponent<MonsterInfo>();
                if (targetInfo.isCatching)
                {
                    if (gameController.listEnemyMonster.Count > 0)
                    {
                        aIDestination.target = NearestTarget();
                    }
                    else
                    {
                        aIDestination.target = null;
                    }
                }
                else
                {
                    // if (info.attackZone <= 3) // Monster đánh gần 
                    // {
                    //     if (PlayerPrefs.GetInt(StaticString.saveBuff) == 1)
                    //     {
                    //         targetInfo.health -= info.dmg * 1.25f * 1.5f;
                    //     }
                    //     else
                    //     {
                    //         targetInfo.health -= info.dmg * 1.25f;
                    //     }
                    //     targetInfo.healthBar.value = targetInfo.health;
                    //     if (targetInfo.health <= 0)
                    //     {
                    //         EnemyMonster targetController = aIDestination.target.GetComponent<EnemyMonster>();
                    //         targetController.OnDespawn();
                    //         if (gameController.listEnemyMonster.Count > 0)
                    //         {
                    //             aIDestination.target = NearestTarget();
                    //         }
                    //         else
                    //         {
                    //             aIDestination.target = null;
                    //         }
                    //     }
                    // }
                    // else // Monster đánh xa
                    // {
                    Vector3 centerPos = transform.GetChild(1).GetComponent<Renderer>().bounds.center;
                    Instantiate(info.attackFX.gameObject, centerPos, transform.rotation).GetComponent<ProjectileMover>().OnSpawn(aIDestination.target, transform);
                    transform.DOScale(transform.localScale, 0.2f).OnComplete(() =>
                    {
                        Vector3 thisPos = new Vector3(transform.position.x, 0, transform.position.z);
                        Vector3 targetPos = new Vector3(aIDestination.target.position.x, 0, aIDestination.target.position.z);
                        float dis = Vector3.Distance(thisPos, targetPos);
                        if (PlayerPrefs.GetInt(StaticString.saveBuff) == 1 || PlayerPrefs.GetInt(StaticString.checkFirstTimePlay) == 1)
                        {
                            if (GameController.ins.isInArena || PlayerPrefs.GetInt(StaticString.checkFirstTimePlay) == 1)
                            {
                                targetInfo.health -= info.dmg;
                            }
                            else
                            {
                                targetInfo.health -= info.dmg * 1.25f * 1.5f;
                            }
                        }
                        else
                        {
                            if (PlayerPrefs.GetInt(StaticString.saveBuff) == 3)
                            {
                                if (dis > (info.attackZone + (info.scaleIndex * 1.5f / 5f)) * 1.75f)
                                {
                                    targetInfo.health -= 0;
                                }
                                else
                                {
                                    if (GameController.ins.isInArena)
                                    {
                                        targetInfo.health -= info.dmg;
                                    }
                                    else
                                    {
                                        targetInfo.health -= info.dmg * 1.25f;
                                    }
                                }
                            }
                            else
                            {
                                if (dis > (info.attackZone + (info.scaleIndex * 1.5f / 5f)))
                                {
                                    targetInfo.health -= 0;
                                }
                                else
                                {
                                    if (GameController.ins.isInArena)
                                    {
                                        targetInfo.health -= info.dmg;
                                    }
                                    else
                                    {
                                        targetInfo.health -= info.dmg * 1.25f;
                                    }
                                }
                            }
                        }
                        targetInfo.healthBar.value = targetInfo.health;
                        if (targetInfo.health <= 0)
                        {
                            EnemyMonster targetController = aIDestination.target.GetComponent<EnemyMonster>();
                            targetController.OnDespawn();
                            if (gameController.listEnemyMonster.Count > 0)
                            {
                                aIDestination.target = NearestTarget();
                            }
                            else
                            {
                                aIDestination.target = null;
                            }
                        }
                    });
                }
            }
        }
        else
        {
            if (info.attackZone >= 5)
            {
                AudioManager.ins.PlayOther("Attack");
            }
            else
            {
                AudioManager.ins.PlayOther("MeleeAttack");
            }

            gameController.player.ShakePlayer();
        }
    }

    public void ManualUpdate()
    {
        if (gameController.isEvolving) return;

        if (gameController.isPlaying)
        {
            if ((PlayerPrefs.GetInt(StaticString.checkFirstTimePlay) == 1 && GameController.ins.LoadMonsterCollection().Count <= 0) || UIManager.ins.currentPopup == Popup.PTutorial3 || UIManager.ins.currentPopup == Popup.PTutorial4)
            {
                MoveToTarget(0);
            }
            else
            {
                CheckDistanceToSetState();

                Action();
            }
        }
        else
        {
            MoveToTarget(0);
        }
    }

    private IEnumerator IEFindTarget()
    {
        while (true)
        {
            if (gameController.listEnemyMonster.Count == 0 || !gameController.isPlaying)
            {
                aIDestination.target = null;
            }
            else
            {
                aIDestination.target = NearestTarget();
            }
            yield return new WaitForSeconds(0.25f);
        }
    }

    private void CheckDistanceToSetState()
    {
        if (aIDestination.target == null)
        {
            SetState(PlayerMonsterState.Idle);
        }
        else
        {
            Vector3 thisPos = new Vector3(transform.position.x, 0, transform.position.z);
            Vector3 targetPos = new Vector3(aIDestination.target.position.x, 0, aIDestination.target.position.z);
            float disToTarget = Vector3.Distance(thisPos, targetPos);

            if (PlayerPrefs.GetInt(StaticString.saveBuff) == 3)
            {
                if (disToTarget > (info.attackZone + (info.scaleIndex * 1.5f / 5f)) * 1.75f)
                {
                    SetState(PlayerMonsterState.Run);
                    return;
                }

                if (disToTarget <= (info.attackZone + (info.scaleIndex * 1.5f / 5f)) * 1.75f)
                {
                    SetState(PlayerMonsterState.Attack);
                    return;
                }
            }
            else
            {
                if (disToTarget > info.attackZone + (info.scaleIndex * 1.5f / 5f))
                {
                    SetState(PlayerMonsterState.Run);
                    return;
                }

                if (disToTarget <= info.attackZone + (info.scaleIndex * 1.5f / 5f))
                {
                    SetState(PlayerMonsterState.Attack);
                    return;
                }
            }
        }
    }

    private void Action()
    {
        if (playerMonsterState == PlayerMonsterState.Idle)
        {
            MoveToTarget(0);
            return;
        }

        if (playerMonsterState == PlayerMonsterState.Run)
        {
            MoveToTarget(info.moveSpeed);
            return;
        }

        if (playerMonsterState == PlayerMonsterState.Attack)
        {
            MoveToTarget(info.moveSpeed / 1000);
            return;
        }
    }

    public void MoveToTarget(float speed)
    {
        if (aIDestination.target == null) return;

        if (playerMonsterState == PlayerMonsterState.Idle || playerMonsterState == PlayerMonsterState.Attack)
        {
            // Determine which direction to rotate towards
            direction = aIDestination.target.position - transform.position;
            Vector3 v = new Vector3(direction.normalized.x, 0, direction.normalized.z);

            // The step size is equal to speed times frame time.
            float singleStep = 4 * Time.deltaTime;

            // Rotate the forward vector towards the target direction by one step
            Vector3 newDirection = Vector3.RotateTowards(transform.forward, direction, singleStep, 0.0f);
            Vector3 dir = new Vector3(newDirection.x, 0, newDirection.z);

            // Calculate a rotation a step closer to the target and applies rotation to this object
            transform.rotation = Quaternion.LookRotation(dir);
        }

        // transform.position += v * speed * Time.deltaTime;
        aIPath.maxSpeed = speed;
    }

    private void SetState(PlayerMonsterState ps)
    {
        if (playerMonsterState == ps) return;

        playerMonsterState = ps;

        if (playerMonsterState == PlayerMonsterState.Idle)
        {
            info.SetAnimTrigger("idle");
            return;
        }

        if (playerMonsterState == PlayerMonsterState.Run)
        {
            info.SetAnimTrigger("move");
            return;
        }

        if (playerMonsterState == PlayerMonsterState.Attack)
        {
            if (PlayerPrefs.GetInt(StaticString.saveBuff) == 2)
            {
                info.SetAnimTrigger("attack", 1.2f);
            }
            else
            {
                info.SetAnimTrigger("attack", 0.8f);
            }
            return;
        }
    }
}

public enum PlayerMonsterState
{
    Idle,
    Run,
    Attack
}