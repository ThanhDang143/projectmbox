using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class HandEvents : MonoBehaviour
{
    public HandType handType;
    public SkinnedMeshRenderer handRenderer;
    [HideInInspector] public PlayerController player;
    [HideInInspector] public GameObject monsterOnHand;
    [HideInInspector] public GameObject newBall;
    public GameObject monsterOnHandPrefab;
    public Transform attackPos;
    public Animator handAnimator;
    public AnimationClip clipIdle1;
    public AnimationClip clipHoldMons;
    public AnimationClip clipLose;
    public AnimationClip clipWin;
    public AnimationClip clipCatch2;
    public GameObject emptyBox;
    public GameObject normalBox;
    public GameObject vipBox;

    public void GetBall()
    {
        if (!GameController.ins.isPlaying)
        {
            if (monsterOnHand) Destroy(monsterOnHand);
            if (newBall) Destroy(newBall);

            return;
        };


        if (monsterOnHand) Destroy(monsterOnHand);
        if (newBall) Destroy(newBall);

        if (GameController.ins.chosenMonster > -1)
        {
            if (GameController.ins.FindMonster(GameController.ins.chosenMonster).IDMonsterEvolve > -1)
            {
                newBall = Instantiate(normalBox.transform, attackPos).gameObject;
                newBall.SetActive(true);
                newBall.transform.position = normalBox.transform.position;
                newBall.transform.rotation = normalBox.transform.rotation;
                newBall.transform.localScale = Vector3.one;
            }
            else
            {
                newBall = Instantiate(vipBox.transform, player.attackPos).gameObject;
                newBall.SetActive(true);
                newBall.transform.position = vipBox.transform.position;
                newBall.transform.rotation = vipBox.transform.rotation;
                newBall.transform.localScale = Vector3.one;
            }
        }
        else
        {
            newBall = Instantiate(emptyBox.transform, player.attackPos).gameObject;
            newBall.SetActive(true);
            newBall.transform.position = emptyBox.transform.position;
            newBall.transform.rotation = emptyBox.transform.rotation;
            newBall.transform.localScale = Vector3.one;
            return;
        }

        MonsterInfo monsterOnHandInfo = GameController.ins.FindMonster(GameController.ins.chosenMonster);

        monsterOnHand = Instantiate(monsterOnHandPrefab, newBall.transform);
        monsterOnHand.AddComponent<RotateMonsterInBox>();
        monsterOnHand.GetComponent<MeshFilter>().mesh = monsterOnHandInfo.transform.GetChild(1).GetComponent<SkinnedMeshRenderer>().sharedMesh;
        monsterOnHand.GetComponent<MeshRenderer>().material = monsterOnHandInfo.transform.GetChild(1).GetComponent<SkinnedMeshRenderer>().sharedMaterial;


        monsterOnHand.transform.position = vipBox.transform.position + (vipBox.transform.up * 0.05f);
        if (monsterOnHandInfo.ID == 141)
        {
            monsterOnHand.transform.position = vipBox.transform.position + (vipBox.transform.up * 0.11f);
        }
        monsterOnHand.transform.rotation = vipBox.transform.rotation;

        Vector3 baseSz = vipBox.GetComponent<Renderer>().bounds.size;
        Vector3 mSz = monsterOnHand.GetComponent<Renderer>().bounds.size;

        float[] findMax = new float[3] { baseSz.x / mSz.x, baseSz.y / mSz.y, baseSz.z / mSz.z };
        monsterOnHand.transform.localScale = Vector3.one * 0.5f * Mathf.Max(findMax);

        for (int i = 1; i < UIManager.ins.monsterView.allBallButton.Count; i++)
        {
            BtnChoseMonster btnController = UIManager.ins.monsterView.allBallButton[i];
            btnController.thisBtn.interactable = true;
        }
    }

    public void ThrowBall()
    {
        AudioManager.ins.PlayOther("Throw");
        player.ThrowMonsterBall();
    }

    public void AutoThrowBall()
    {
        GameController.ins.AutoThrowMonster();
    }

    public void EnableLaser()
    {
        player.EnableLaser();
    }
}

public enum HandType
{
    Default,
    Gift,
    Spin,
    IAP,
    Level
}