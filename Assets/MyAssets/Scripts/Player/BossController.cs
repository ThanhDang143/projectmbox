
/* Copyright © @ThanhDV */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    [HideInInspector] public int bossRound;
    [HideInInspector] public int maxRound;
    public Transform throwPos;
    public Animator animator;
    public AnimationClip clipAction;
    public AnimationClip clipIdle;
    public AnimationClip clipLose;
    public AnimationClip clipThrow;
    public GameObject[] hat;
    public GameObject[] hair;

    public void OnSpawn()
    {
        foreach (GameObject h in hat)
        {
            h.SetActive(false);
        }
        foreach (GameObject t in hair)
        {
            t.SetActive(false);
        }

        transform.localScale = Vector3.one * 4;
        bossRound = 0;
        maxRound = Random.Range(2, 4);

        if (hat.Length > 0)
        {
            hat[Random.Range(0, hat.Length)].SetActive(true);
        }
        if (hair.Length > 0)
        {
            hair[Random.Range(0, hair.Length)].SetActive(true);
        }

        animator.Play(clipIdle.name);
    }

    private void Update()
    {
        if (!GameController.ins.isPlaying && !GameController.ins.isMoving)
        {
            transform.LookAt(GameController.ins.player.transform, Vector3.up);
        }
    }

    public void ThrowMonster()
    {
        GameController gc = GameController.ins;
        PArenaLoading pArenaLoading = UIManager.ins.pArenaLoading;

        if (!gc.isInArena)
        {
            int checkMap = gc.ReadInt(gc.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14);
            int maxMonsterInRound = 0;
            if (checkMap == 1)
            {
                maxMonsterInRound = 3;
            }
            else
            {
                maxMonsterInRound = 5;
            }

            int readRateLv2 = gc.ReadInt(gc.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 13);
            int countMonsterLv2 = 0;

            List<int> kindOfMonster = new List<int>();
            for (int i = 1; i < 13; i++)
            {
                int readMonster = gc.ReadInt(gc.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), i);
                if (readMonster > 0)
                {
                    kindOfMonster.Add(readMonster);
                }
            }

            AudioManager.ins.PlayOther("Throw");
            for (int i = 0; i < maxMonsterInRound; i++)
            {
                int ranMonster = kindOfMonster[UnityEngine.Random.Range(0, kindOfMonster.Count)];

                if (countMonsterLv2 < readRateLv2) // chose monster lv1 or lv2
                {
                    int idEvolve = gc.FindMonster(ranMonster).IDMonsterEvolve;

                    if (idEvolve > 0)
                    {
                        Transform b = PoolMonsterBall.pool.Spawn(gc.monsterBall[1], throwPos.position, Quaternion.identity);
                        b.GetComponent<BossBall>().OnSpawn(gc.FindMonster(idEvolve), gc.currentMap.enemiesPos[i].position);
                        countMonsterLv2++;
                    }
                    else
                    {
                        Transform b = PoolMonsterBall.pool.Spawn(gc.monsterBall[0], throwPos.position, Quaternion.identity);
                        b.GetComponent<BossBall>().OnSpawn(gc.FindMonster(ranMonster), gc.currentMap.enemiesPos[i].position);
                    }
                }
                else
                {
                    Transform b = PoolMonsterBall.pool.Spawn(gc.monsterBall[0], throwPos.position, Quaternion.identity);
                    b.GetComponent<BossBall>().OnSpawn(gc.FindMonster(ranMonster), gc.currentMap.enemiesPos[i].position);
                }
            }
        }
        else
        {
            AudioManager.ins.PlayOther("Throw");

            for (int i = 0; i < pArenaLoading.arenaMonster.Count; i++)
            {
                if (pArenaLoading.arenaMonster[i] <= 0) continue;

                if (pArenaLoading.arenaMonster[i] < 100)
                {
                    Transform b = PoolMonsterBall.pool.Spawn(gc.monsterBall[0], throwPos.position, Quaternion.identity);
                    b.GetComponent<BossBall>().OnSpawn(gc.FindMonster(pArenaLoading.arenaMonster[i]), gc.currentMap.enemiesPos[i].position);
                }
                else
                {
                    Transform b = PoolMonsterBall.pool.Spawn(gc.monsterBall[1], throwPos.position, Quaternion.identity);
                    b.GetComponent<BossBall>().OnSpawn(gc.FindMonster(pArenaLoading.arenaMonster[i]), gc.currentMap.enemiesPos[i].position);
                }
            }
        }
    }
}
