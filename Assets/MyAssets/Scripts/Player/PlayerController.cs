using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using DG.Tweening;

public class PlayerController : MonoBehaviour
{
    public float mouseSensitivity;
    [SerializeField] private float throwForwardForce;
    [SerializeField] private float throwUpForce;
    [HideInInspector] public Transform attackPos;
    [HideInInspector] public SkinnedMeshRenderer handRenderer;
    [SerializeField] public Transform transMainCam;
    [SerializeField] private Camera fpsCam;
    [SerializeField] public CinemachineVirtualCamera vMainCam;
    [SerializeField] public CinemachineVirtualCamera vSubCam;
    [SerializeField] private bool invertLook;
    [SerializeField] private GameObject laserPrefab;
    private EGA_Laser laser;
    [HideInInspector] public float verticalRotation;
    private Vector2 mouseInput;
    private Vector3 direction;
    private GameController gameController;
    [HideInInspector] public List<GameObject> allCatchFX = new List<GameObject>();

    [Header("Anim")]
    public Transform handPos;
    [HideInInspector] public Animator animator;
    [HideInInspector] public AnimationClip clipIdle1;
    [HideInInspector] public AnimationClip clipHoldMons;
    [HideInInspector] public AnimationClip clipLose;
    [HideInInspector] public AnimationClip clipWin;
    [HideInInspector] public AnimationClip clipCatch2;


    public HandEvents handEvents;
    [HideInInspector] public GameObject emptyBox;
    [HideInInspector] public GameObject normalBox;
    [HideInInspector] public GameObject vipBox;
    private float timeCounter;

    [Header("Other")]
    public Transform freeMonsterFX;
    public PhysicMaterial ballPhysicMat;

    public void Setup()
    {
        gameController = GameController.ins;

        GameObject he = Instantiate(gameController.allGloves[PlayerPrefs.GetInt(StaticString.saveUsingGloves)].gameObject, transMainCam);
        he.transform.position = handPos.position;
        he.transform.rotation = handPos.rotation;
        handEvents = he.GetComponent<HandEvents>();

        vMainCam.enabled = true;
        vSubCam.enabled = false;

        handEvents.player = this;

        handRenderer = handEvents.handRenderer;
        attackPos = handEvents.attackPos;
        animator = handEvents.handAnimator;
        clipIdle1 = handEvents.clipIdle1;
        clipHoldMons = handEvents.clipHoldMons;
        clipLose = handEvents.clipLose;
        clipWin = handEvents.clipWin;
        clipCatch2 = handEvents.clipCatch2;

        emptyBox = handEvents.emptyBox;
        normalBox = handEvents.normalBox;
        vipBox = handEvents.vipBox;

        animator.Play(clipIdle1.name);
    }


    private void Update()
    {
        if (gameController.isInArena)
        {
            if (gameController.isMoving) return;

            LookAround();
            return;
        }

        if (gameController.isExchangeMap || gameController.isSpin) return;

        if (gameController.isEvolving)
        {
            gameController.UseMainCam();
            LookAt(gameController.currentMap.transform);
            return;
        }


        if (!gameController.isPlaying && !gameController.isLosing)
        {
            if (gameController.listEnemyMonster.Count > 0)
            {
                LookAt(gameController.listEnemyMonster[0].transform);
            }
            else
            {
                if (gameController.isMoving || gameController.isInSpecialWorld || gameController.currentBoss == null) return;

                int readMapType = gameController.ReadInt(gameController.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14);
                if (readMapType == 1 || readMapType == 2 || gameController.isExchangeMap) // if current map is boss map
                {
                    SimpleLookAt(gameController.currentBoss.transform);
                }
            }
            return;
        }

        if (gameController.isLosing && !gameController.isPlaying) return;

        LookAround();

    }

    public void EnableLaser()
    {
        if (allCatchFX.Count >= 1 || gameController.isCatching || gameController.isMoving || gameController.currentBtnChoseMonster.IDMonster > 0) return;

        AudioManager.ins.PlayOther("LazerFire");
        Vector3 laserPos = emptyBox.GetComponent<Renderer>().bounds.center;
        laser = Instantiate(laserPrefab, laserPos, attackPos.rotation, attackPos).GetComponent<EGA_Laser>();

        Ray ray = fpsCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        Vector3 targetPoint = ray.GetPoint(100);
        laser.transform.LookAt(targetPoint);

        allCatchFX.Add(laser.gameObject);
    }

    public void DisableLaser()
    {
        try
        {
            laser.DisablePrepare();
        }
        catch (System.Exception)
        {

        }
        allCatchFX.Clear();
        AudioManager.ins.StopOther("LazerFire");
        gameController.currentBtnChoseMonster.isSpawnedObj = false;

        if (laser) Destroy(laser.gameObject, 1);
    }

    public void ShakePlayer()
    {
        Vector3 tempPos = transform.position;
        transform.DOShakePosition(0.5f, 0.25f).OnComplete(() =>
        {
            transform.position = tempPos;
        });
    }


    public void ThrowMonsterBall()
    {
        if (gameController.currentBtnChoseMonster && gameController.currentBtnChoseMonster.isSpawnedObj && gameController.chosenMonster != -1) return;

        if (gameController.isMoving || gameController.isCatching) return;

        if (gameController.listBallSpawned.Count > 0) return;

        if (gameController.currentBtnChoseMonster)
        {
            gameController.currentBtnChoseMonster.isSpawnedObj = true;
        }

        if (gameController.chosenMonster < 0) // using empty monster ball
        {
            DisableLaser();
        }
        else // using normal monster ball
        {
            Ray ray = fpsCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)); // A ray through the middle of your current view
            RaycastHit hit;

            //Check if ray hits something
            Vector3 targetPoint;
            if (Physics.Raycast(ray, out hit))
            {
                targetPoint = hit.point;
            }
            else
            {
                targetPoint = ray.GetPoint(100); // Just a random point far away from the player
            }

            //Calculate direction from attackPoint to targetPoint
            Vector3 directionWithoutSpread = targetPoint - attackPos.position;

            Rigidbody rbb;
            // rbb = PoolMonsterBall.pool.Spawn(gameController.monsterBall[1], attackPos.position, Quaternion.identity).GetComponent<Rigidbody>();
            // rbb.GetComponent<MonsterBall>().OnSpawn();
            rbb = handEvents.newBall.AddComponent<Rigidbody>();
            handEvents.newBall.AddComponent<BoxCollider>().material = ballPhysicMat;
            handEvents.newBall.AddComponent<MonsterBall>().OnSpawn(rbb, freeMonsterFX);
            handEvents.newBall.transform.parent = null;
            rbb.AddForce(directionWithoutSpread.normalized * throwForwardForce, ForceMode.Impulse);
            rbb.AddForce(fpsCam.transform.up * throwUpForce, ForceMode.Impulse);
            gameController.ResetAnimTrigger();
        }
    }

    private void LookAround()
    {
        // Get Input
        // if (PlayerPrefs.GetInt(SaveKey.saveController) == 0)
        // {
        FixedTouchField touchField = UIManager.ins.sIngame.touchField;
        mouseInput = new Vector2(touchField.TouchDist.x, touchField.TouchDist.y) * (mouseSensitivity / 10);
        // }
        // else
        // {
        //     mouseInput = new Vector2(UIManager.ins.sIngame.variableJoystick.Horizontal, UIManager.ins.sIngame.variableJoystick.Vertical) * mouseSensitivity;
        // }
        // Horizontal rotation
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + mouseInput.x, transform.rotation.eulerAngles.z);

        // Vertical rotation
        verticalRotation += mouseInput.y;
        verticalRotation = Mathf.Clamp(verticalRotation, -60, 85);
        if (!invertLook)
        {
            transMainCam.rotation = Quaternion.Euler(-verticalRotation, transMainCam.rotation.eulerAngles.y, transMainCam.rotation.eulerAngles.z);
        }
        else
        {
            transMainCam.rotation = Quaternion.Euler(verticalRotation, transMainCam.rotation.eulerAngles.y, transMainCam.rotation.eulerAngles.z);
        }
    }

    public void LookAt(Transform target, float speed = 3.5f)
    {
        // Determine which direction to rotate towards
        direction = target.position - transform.position;

        // The step size is equal to speed times frame time.
        float singleStep = speed * Time.deltaTime;

        // Rotate the forward vector towards the target direction by one step
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, direction, singleStep, 0.0f);
        Vector3 dir = new Vector3(newDirection.x, 0, newDirection.z);

        // Calculate a rotation a step closer to the target and applies rotation to this object
        transform.rotation = Quaternion.LookRotation(dir);
    }

    public void SimpleLookAt(Transform target)
    {
        if (target == null) return;
        transform.LookAt(target, Vector3.up);
    }
}

