using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;
using SendEventAppMetrica;

public class IAPFunction : MonoBehaviour
{
    public Sprite spriteGray;
    public Sprite spriteGreen;

    public Text txtGlovesPrice;
    public Image imgGlovesPrice;
    public Button btnGlovesPrice;
    public Text txtNormalRemoveAdsPrice;
    public Image imgNormalRemoveAdsPrice;
    public Button btnNormalRemoveAdsPrice;
    public Text txtPremiumRemoveAdsPrice;
    public Image imgPremiumRemoveAdsPrice;
    public Button btnPremiumRemoveAdsPrice;
    public Text txtWorldTicketPrice;
    public Image imgWorldTicketPrice;
    public Button btnWorldTicketPrice;

    public Image[] imgMonster;
    public Button[] btnMonster;

    public void CheckOnLoadShop(bool viewed = false)
    {
        if (PlayerPrefs.GetInt(StaticString.saveIAPNormalRemoveAds) == 1)
        {
            txtNormalRemoveAdsPrice.text = "PURCHASED";
            txtNormalRemoveAdsPrice.fontSize = 35;
            imgNormalRemoveAdsPrice.sprite = spriteGray;
            btnNormalRemoveAdsPrice.interactable = false;
        }

        if (PlayerPrefs.GetInt(StaticString.saveIAPPremiumRemoveAds) == 1)
        {
            txtPremiumRemoveAdsPrice.text = "PURCHASED";
            txtPremiumRemoveAdsPrice.fontSize = 35;
            imgPremiumRemoveAdsPrice.sprite = spriteGray;
            btnPremiumRemoveAdsPrice.interactable = false;
        }

        if (PlayerPrefs.GetInt(StaticString.saveIAPGloves) == 1)
        {
            txtGlovesPrice.text = "PURCHASED";
            txtGlovesPrice.fontSize = 35;
            imgGlovesPrice.sprite = spriteGray;
            btnGlovesPrice.interactable = false;
        }

        if (PlayerPrefs.GetInt(StaticString.saveIAPWorldTicket) == 1)
        {
            txtWorldTicketPrice.text = "PURCHASED";
            txtWorldTicketPrice.fontSize = 35;
            imgWorldTicketPrice.sprite = spriteGray;
            btnWorldTicketPrice.interactable = false;
        }

        CheckNewIAPMonster(viewed);
    }

    public bool CheckNewIAPMonster(bool viewed = false)
    {
        int t = 0;
        if (!CheckMonsterAvailable(203) && PlayerPrefs.GetInt(StaticString.saveLevel) >= 5)
        {
            imgMonster[0].sprite = spriteGreen;
            btnMonster[0].interactable = true;
            t++;
        }
        else
        {
            imgMonster[0].sprite = spriteGray;
            btnMonster[0].interactable = false;
        }

        if (!CheckMonsterAvailable(205) && PlayerPrefs.GetInt(StaticString.saveLevel) >= 10)
        {
            imgMonster[1].sprite = spriteGreen;
            btnMonster[1].interactable = true;
            t++;
        }
        else
        {
            imgMonster[1].sprite = spriteGray;
            btnMonster[1].interactable = false;
        }

        if (!CheckMonsterAvailable(207) && PlayerPrefs.GetInt(StaticString.saveLevel) >= 20)
        {
            imgMonster[2].sprite = spriteGreen;
            btnMonster[2].interactable = true;
            t++;
        }
        else
        {
            imgMonster[2].sprite = spriteGray;
            btnMonster[2].interactable = false;
        }

        if (!CheckMonsterAvailable(208) && PlayerPrefs.GetInt(StaticString.saveLevel) >= 25)
        {
            imgMonster[3].sprite = spriteGreen;
            btnMonster[3].interactable = true;
            t++;
        }
        else
        {
            imgMonster[3].sprite = spriteGray;
            btnMonster[3].interactable = false;
        }

        if (t <= PlayerPrefs.GetInt(StaticString.saveNewIAPMonsterUnlock))
        {
            return false;
        }
        else
        {
            if (viewed)
            {
                PlayerPrefs.SetInt(StaticString.saveNewIAPMonsterUnlock, t);
            }
            return true;
        }
    }

    private bool CheckMonsterAvailable(int IDMonster)
    {
        foreach (var b in UIManager.ins.monsterView.allBallButton)
        {
            if (b.IDMonster == IDMonster) return true;
        }
        return false;
    }

    public void IAPNormalRemoveAds()
    {
        PlayerPrefs.SetInt(StaticString.saveIAPNormalRemoveAds, 1);
        ManagerAds.ins.RemoveBannerAds();
        ManagerAds.ins.RemoveInterstitialAds();
        AudioManager.ins.PlayOther("Buy");
        txtNormalRemoveAdsPrice.text = "PURCHASED";
        txtNormalRemoveAdsPrice.fontSize = 35;
        imgNormalRemoveAdsPrice.sprite = spriteGray;
        btnNormalRemoveAdsPrice.interactable = false;

        var product = CodelessIAPStoreListener.Instance.GetProduct("com.mtm.ads1");
        if (product != null)
        {
            SendEventManager.ins.SendPaymentSucceed(product, "IAPNormalRemoveAds");
        }
    }

    public void IAPPremiumRemoveAds()
    {
        ManagerAds.ins.RemoveBannerAds();
        ManagerAds.ins.RemoveInterstitialAds();
        ManagerAds.ins.RemoveRewardAds();
        AudioManager.ins.PlayOther("Buy");
        txtPremiumRemoveAdsPrice.text = "PURCHASED";
        txtPremiumRemoveAdsPrice.fontSize = 35;
        imgPremiumRemoveAdsPrice.sprite = spriteGray;
        btnPremiumRemoveAdsPrice.interactable = false;

        var product = CodelessIAPStoreListener.Instance.GetProduct("com.mtm.ads1");
        if (product != null)
        {
            SendEventManager.ins.SendPaymentSucceed(product, "IAPNormalRemoveAds");
        }
    }

    public void IAPGloves()
    {
        PlayerPrefs.SetInt(StaticString.saveIAPGloves, 1);
        GameController.ins.SaveGloves(3, 2.5f);
        GameController.ins.SaveGloves(4, 2.5f);
        GameController.ins.SaveGloves(9, 2.5f);
        txtGlovesPrice.text = "PURCHASED";
        txtGlovesPrice.fontSize = 35;
        imgGlovesPrice.sprite = spriteGray;
        btnGlovesPrice.interactable = false;

        var product = CodelessIAPStoreListener.Instance.GetProduct("com.mtm.gloves");
        if (product != null)
        {
            SendEventManager.ins.SendPaymentSucceed(product, "IAPGloves");
        }
    }

    public void IAPWorldTicket()
    {
        PlayerPrefs.SetInt(StaticString.saveIAPWorldTicket, 1);
        AudioManager.ins.PlayOther("Buy");
        txtWorldTicketPrice.text = "PURCHASED";
        txtWorldTicketPrice.fontSize = 35;
        imgWorldTicketPrice.sprite = spriteGray;
        btnWorldTicketPrice.interactable = false;

        var product = CodelessIAPStoreListener.Instance.GetProduct("com.mtm.world");
        if (product != null)
        {
            SendEventManager.ins.SendPaymentSucceed(product, "IAPWorldTicket");
        }
    }

    public void IAPMetamon(int IDMonster)
    {
        AudioManager.ins.PlayOther("Buy");
        MonsterInfo rm = GameController.ins.FindMonster(IDMonster);
        UIManager.ins.monsterView.RewardMonster(rm);
        CheckNewIAPMonster();

        string idProduct = "";
        switch (IDMonster)
        {
            case 3:
                idProduct = "com.mtm.pkm" + 1;
                break;
            case 5:
                idProduct = "com.mtm.pkm" + 2;
                break;
            case 7:
                idProduct = "com.mtm.pkm" + 3;
                break;
            case 8:
                idProduct = "com.mtm.pkm" + 4;
                break;
        }

        var product = CodelessIAPStoreListener.Instance.GetProduct(idProduct);
        if (product != null)
        {
            SendEventManager.ins.SendPaymentSucceed(product, "IAPMetamon" + IDMonster);
        }
    }
}
