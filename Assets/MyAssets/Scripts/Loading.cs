using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class Loading : MonoBehaviour
{
    // public static Loading ins;
    // public MonsterInfo[] tempMonsterInfo;

    // [Header("AssetBundle")]
    // public string bundleUrl = "https://drive.google.com/uc?export=download&id=1zx--n4l1HzF6lwJw_8KvtDOW9r4EGR8N";

    // [Header("UI")]
    // public Slider loadDataSlider;
    // public Text txtLoadData;
    // public Slider loadSetupData;
    // public Text txtSetupData;
    public Slider loadSceneSlider;
    public Text txtLoadScene;

    private void Awake()
    {
        // ins = this;
        // DontDestroyOnLoad(gameObject);

        // if (!File.Exists(StaticString.pathSaveAssetBundle))
        // {
        //     StartCoroutine(IEDownloadBundle());
        // }
        // else
        // {
        //     loadDataSlider.value = 1;
        //     txtLoadData.text = "100.00%";
        //     StartCoroutine(IESetupData(StaticString.pathSaveAssetBundle));
        // }
    }

    private void Start()
    {
        loadSceneSlider.value = 0;
        txtLoadScene.text = "0.00%";
        StartCoroutine(IELoadScene(1));
    }

    private IEnumerator IELoadScene(int sceneIndex)
    {
        yield return new WaitForSeconds(0.25f);

        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);

            loadSceneSlider.value = progress;
            txtLoadScene.text = (progress * 100f).ToString("#.00") + "%";

            yield return null;
        }
    }

    // public IEnumerator IEDownloadBundle()
    // {
    //     UnityWebRequest www = UnityWebRequest.Get(bundleUrl);
    //     DownloadHandler handler = www.downloadHandler;

    //     //Send Request and wait
    //     UnityWebRequestAsyncOperation dataOperation = www.SendWebRequest();
    //     loadDataSlider.value = 0;

    //     while (!dataOperation.isDone)
    //     {
    //         float progress = Mathf.Clamp01(dataOperation.progress / 0.9f);

    //         loadDataSlider.value = progress;
    //         txtLoadData.text = (progress * 100f).ToString("#.00") + "%";

    //         yield return null;
    //     }

    //     if (www.result == UnityWebRequest.Result.ConnectionError)
    //     {
    //         Debug.Log("Error while Downloading Data: " + www.error);
    //     }
    //     else
    //     {
    //         Debug.Log("Success");
    //         //handle.data

    //         //Construct path to save it
    //         string dataFileName = "allmonster";
    //         string tempPath = System.IO.Path.Combine(Application.persistentDataPath, "AssetData");
    //         tempPath = System.IO.Path.Combine(tempPath, dataFileName + ".unity3d");

    //         //Save
    //         SaveAssetBundle(handler.data, tempPath);

    //         //Load asset to array
    //         StartCoroutine(IESetupData(StaticString.pathSaveAssetBundle));
    //     }
    // }

    // public void SaveAssetBundle(byte[] data, string path)
    // {
    //     //Create the Directory if it does not exist
    //     if (!Directory.Exists(System.IO.Path.GetDirectoryName(path)))
    //     {
    //         Directory.CreateDirectory(System.IO.Path.GetDirectoryName(path));
    //     }

    //     try
    //     {
    //         File.WriteAllBytes(path, data);
    //         Debug.Log("Saved Data to: " + path.Replace("/", "\\"));
    //     }
    //     catch (Exception e)
    //     {
    //         Debug.LogWarning("Failed To Save Data to: " + path.Replace("/", "\\"));
    //         Debug.LogWarning("Error: " + e.Message);
    //     }
    // }

    // private IEnumerator IESetupData(string path)
    // {
    //     AssetBundleCreateRequest bundle = AssetBundle.LoadFromFileAsync(path);
    //     while (!bundle.isDone)
    //     {
    //         float progress = Mathf.Clamp01(bundle.progress / 0.9f);

    //         loadSetupData.value = progress;
    //         txtSetupData.text = (progress * 100f).ToString("#.00") + "%";

    //         yield return null;
    //     }

    //     AssetBundle myLoadedAssetBundle = bundle.assetBundle;
    //     if (myLoadedAssetBundle == null)
    //     {
    //         Debug.Log("Failed to load AssetBundle!");
    //         yield break;
    //     }

    //     AssetBundleRequest request = myLoadedAssetBundle.LoadAllAssetsAsync<GameObject>();
    //     while (!request.isDone)
    //     {
    //         float progress = Mathf.Clamp01(request.progress / 0.9f);

    //         loadSetupData.value = progress;
    //         txtSetupData.text = (progress * 100f).ToString("#.00") + "%";

    //         yield return null;
    //     }

    //     tempMonsterInfo = new MonsterInfo[request.allAssets.Length];

    //     for (int i = 0; i < request.allAssets.Length; i++)
    //     {
    //         GameObject g = request.allAssets[i] as GameObject;
    //         tempMonsterInfo[i] = g.GetComponent<MonsterInfo>();
    //         yield return null;
    //     }

    //     myLoadedAssetBundle.Unload(false);

    //     StartCoroutine(IELoadScene(1));
    // }
}
