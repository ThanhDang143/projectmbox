
/* Copyright © @ThanhDV */

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[System.Serializable]
public class ArenaData100
{
    public int rankArena;
    public int avatarArena;
    public string nameArena;
    public int monster1Arena;
    public int monster2Arena;
    public int monster3Arena;
    public int monster4Arena;
    public int monster5Arena;
    public int pointArena;
}

[System.Serializable]
public class SaveGame
{
    public int[] arrayIDMonster;
    public int[] arrayLvUnlock;
    public List<int> IDOfCaughtMonster;
    public float[] glovesStatus;
    public bool[] collectionRewardStatus;

    public ArenaData100[] arenaData100s;


    public SaveGame(int rank, int avatar, string name, int monster1, int monster2, int monster3, int monster4, int monster5, int point)
    {
        GameController gc = GameController.ins;
        if (File.Exists(StaticString.pathSaveArenaRank))
        {
            SaveGame data = SaveSystem.LoadData<SaveGame>(StaticString.pathSaveArenaRank);
            arenaData100s = data.arenaData100s;
        }
        else
        {
            string[] data = gc.txtArenaTop100.text.Split(new string[] { "\n" }, StringSplitOptions.None);
            int dataLength = data.Length;

            arenaData100s = new ArenaData100[dataLength - 1];

            for (int i = 1; i < dataLength; i++)
            {
                arenaData100s[i - 1] = new ArenaData100();
                arenaData100s[i - 1].rankArena = gc.ReadInt(gc.txtArenaTop100, i, 0);
                arenaData100s[i - 1].avatarArena = gc.ReadInt(gc.txtArenaTop100, i, 1);
                arenaData100s[i - 1].nameArena = gc.ReadString(gc.txtArenaTop100, i, 2);
                arenaData100s[i - 1].monster1Arena = gc.ReadInt(gc.txtArenaTop100, i, 3);
                arenaData100s[i - 1].monster2Arena = gc.ReadInt(gc.txtArenaTop100, i, 4);
                arenaData100s[i - 1].monster3Arena = gc.ReadInt(gc.txtArenaTop100, i, 5);
                arenaData100s[i - 1].monster4Arena = gc.ReadInt(gc.txtArenaTop100, i, 6);
                arenaData100s[i - 1].monster5Arena = gc.ReadInt(gc.txtArenaTop100, i, 7);
                arenaData100s[i - 1].pointArena = gc.ReadInt(gc.txtArenaTop100, i, 8);
            }
        }

        arenaData100s[rank - 1].rankArena = rank;
        arenaData100s[rank - 1].avatarArena = avatar;
        arenaData100s[rank - 1].nameArena = name;
        arenaData100s[rank - 1].monster1Arena = monster1;
        arenaData100s[rank - 1].monster2Arena = monster2;
        arenaData100s[rank - 1].monster3Arena = monster3;
        arenaData100s[rank - 1].monster4Arena = monster4;
        arenaData100s[rank - 1].monster5Arena = monster5;
        arenaData100s[rank - 1].pointArena = point;
    }

    public SaveGame()
    {
        int countAllBtn = UIManager.ins.monsterView.allBallButton.Count;

        arrayIDMonster = new int[countAllBtn];
        arrayLvUnlock = new int[countAllBtn];

        for (int i = 0; i < countAllBtn; i++)
        {
            if (i > 0)
            {
                arrayIDMonster[i] = UIManager.ins.monsterView.allBallButton[i].IDMonster;
                arrayLvUnlock[i] = UIManager.ins.monsterView.allBallButton[i].unlockAtLevel;
            }
        }
    }

    public SaveGame(int IDMonster)
    {
        if (File.Exists(StaticString.pathSaveMonsterCollection))
        {
            SaveGame data = SaveSystem.LoadData<SaveGame>(StaticString.pathSaveMonsterCollection);
            IDOfCaughtMonster = data.IDOfCaughtMonster;
        }
        else
        {
            IDOfCaughtMonster = new List<int>();
        }

        if (!IDOfCaughtMonster.Contains(IDMonster))
        {
            IDOfCaughtMonster.Add(IDMonster);
        }
    }

    public SaveGame(int IDGloves, float status)
    {
        if (File.Exists(StaticString.pathSaveGloves))
        {
            SaveGame data = SaveSystem.LoadData<SaveGame>(StaticString.pathSaveGloves);
            glovesStatus = data.glovesStatus;
        }
        else
        {
            glovesStatus = new float[GameController.ins.allGloves.Length];
            glovesStatus[0] = 2.1f;
            for (int i = 1; i < glovesStatus.Length; i++)
            {
                glovesStatus[i] = 0;
            }
        }

        glovesStatus[IDGloves] = status;
    }

    public SaveGame(int IDCollectionReward, bool status)
    {
        if (File.Exists(StaticString.pathSaveCollectionReward))
        {
            SaveGame data = SaveSystem.LoadData<SaveGame>(StaticString.pathSaveCollectionReward);
            collectionRewardStatus = data.collectionRewardStatus;
        }
        else
        {
            collectionRewardStatus = new bool[9];

            for (int i = 0; i < collectionRewardStatus.Length; i++)
            {
                collectionRewardStatus[i] = true;
            }
        }

        collectionRewardStatus[IDCollectionReward] = status;
    }
}
