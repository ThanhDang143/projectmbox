using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using Pathfinding;
using System;

// Create a menu item that causes a new controller and statemachine to be created.

public class CreateNewMonsterPrefab : Editor
{
    [MenuItem("Tools/Create New Monster Prefab")]
    static void CreateController()
    {
        GameController gameController = GameController.ins;
        int monsterInfoScriptLength = gameController.txtMonsterInfo.text.Split(new string[] { "\n" }, StringSplitOptions.None).Length;

        for (int i = 73; i < 73 + gameController.rootMonstersFBX.Length; i++)
        {
            // Get ID
            string getID = (i + 100).ToString("000");

            GameObject x = gameController.rootMonstersFBX[i - 73];

            // Creates the controller
            var animatorController = UnityEditor.Animations.AnimatorController.CreateAnimatorControllerAtPath("Assets/MyAssets/Animations/MonsterEvolveRemake/AC_ME" + getID + ".controller");

            // // Add parameters
            animatorController.AddParameter("idle", AnimatorControllerParameterType.Trigger);
            animatorController.AddParameter("move", AnimatorControllerParameterType.Trigger);
            animatorController.AddParameter("attack", AnimatorControllerParameterType.Trigger);

            // Add State
            var rootStateMachine = animatorController.layers[0].stateMachine;
            var stateIdle = rootStateMachine.AddState("Idle");
            var stateMove = rootStateMachine.AddState("Move");
            var stateAttack = rootStateMachine.AddState("Attack");

            // Add Motion
            var assetRepresentationsAtPath = AssetDatabase.LoadAllAssetRepresentationsAtPath(AssetDatabase.GetAssetPath(x));

            // Add Transitions
            var transitionIM = stateIdle.AddTransition(stateMove); transitionIM.hasExitTime = true; transitionIM.AddCondition(UnityEditor.Animations.AnimatorConditionMode.If, 0, "move");
            var transitionIA = stateIdle.AddTransition(stateAttack); transitionIA.hasExitTime = true; transitionIA.AddCondition(UnityEditor.Animations.AnimatorConditionMode.If, 0, "attack");
            var transitionMI = stateMove.AddTransition(stateIdle); transitionMI.hasExitTime = true; transitionMI.AddCondition(UnityEditor.Animations.AnimatorConditionMode.If, 0, "idle");
            var transitionMA = stateMove.AddTransition(stateAttack); transitionMA.hasExitTime = true; transitionMA.AddCondition(UnityEditor.Animations.AnimatorConditionMode.If, 0, "attack");
            var transitionAI = stateAttack.AddTransition(stateIdle); transitionAI.hasExitTime = true; transitionAI.AddCondition(UnityEditor.Animations.AnimatorConditionMode.If, 0, "idle");
            var transitionAM = stateAttack.AddTransition(stateMove); transitionAM.hasExitTime = true; transitionAM.AddCondition(UnityEditor.Animations.AnimatorConditionMode.If, 0, "move");

            // Create NewPrefabs
            string modelPath = AssetDatabase.GetAssetPath(x);
            var modelRoot = (GameObject)AssetDatabase.LoadMainAssetAtPath(modelPath);
            GameObject monsterPrefabs = Instantiate(modelRoot);

            monsterPrefabs.AddComponent<Animator>().runtimeAnimatorController = animatorController;

            monsterPrefabs.AddComponent<Rigidbody>();
            CapsuleCollider cmp = monsterPrefabs.AddComponent<CapsuleCollider>();
            cmp.radius = 0.2f;
            cmp.height = 0.5f;
            cmp.center = new Vector3(0, cmp.height / 2, 0);
            SphereCollider smp = monsterPrefabs.AddComponent<SphereCollider>();
            monsterPrefabs.AddComponent<MonsterInfo>();

            monsterPrefabs.AddComponent<AIDestinationSetter>();
            AIPath monsterAIPath = monsterPrefabs.AddComponent<AIPath>();

            monsterAIPath.radius = 0.35f;
            monsterAIPath.height = 1;
            monsterAIPath.enableRotation = true;

            // Modify Prefab contents.
            MonsterInfo monsterContentsInfo = monsterPrefabs.GetComponent<MonsterInfo>();

            monsterContentsInfo.ID = int.Parse(getID);
            for (int j = 0; j < monsterInfoScriptLength; j++)
            {
                if (gameController.ReadFloat(gameController.txtMonsterInfo, j, 0) == int.Parse(getID))
                {
                    monsterContentsInfo.IDMonsterEvolve = gameController.ReadInt(gameController.txtMonsterInfo, j, 1);
                    monsterContentsInfo.monsterName = gameController.ReadString(gameController.txtMonsterInfo, j, 2);
                    monsterContentsInfo.moveSpeed = gameController.ReadFloat(gameController.txtMonsterInfo, j, 7) + 0.5f;
                    monsterContentsInfo.scaleIndex = gameController.ReadFloat(gameController.txtMonsterInfo, j, 14);
                    monsterContentsInfo.rb = monsterPrefabs.GetComponent<Rigidbody>();
                    RectTransform hb = Instantiate(gameController.healthBars[1].gameObject, monsterPrefabs.transform).GetComponent<RectTransform>();
                    hb.anchoredPosition = new Vector2(0, 0.8f);
                    monsterContentsInfo.imgFillHealthBar = hb.GetComponent<UIHealthBar>().imgFillHealthBar;

                    monsterContentsInfo.attackZone = gameController.ReadFloat(gameController.txtMonsterInfo, j, 9);
                    monsterContentsInfo.dmg = gameController.ReadFloat(gameController.txtMonsterInfo, j, 6);

                    monsterContentsInfo.healthBar = hb.GetComponent<UIHealthBar>().healthBarSlider;
                    monsterContentsInfo.maxHealth = gameController.ReadFloat(gameController.txtMonsterInfo, j, 5);

                    monsterContentsInfo.animator = monsterPrefabs.GetComponent<Animator>();
                    foreach (var assetRepresentation in assetRepresentationsAtPath)
                    {
                        var animationClip = assetRepresentation as AnimationClip;

                        if (animationClip != null)
                        {
                            if (animationClip.name.Contains("dle"))
                            {
                                if (!animationClip.name.Contains("2"))
                                {
                                    stateIdle.motion = animationClip;
                                    monsterContentsInfo.animIdle = animationClip;
                                }
                            }
                            else if (animationClip.name.Contains("ove"))
                            {
                                stateMove.motion = animationClip;
                                monsterContentsInfo.animRun = animationClip;
                            }
                            else if (animationClip.name.Contains("ttack"))
                            {
                                stateAttack.motion = animationClip;
                                monsterContentsInfo.animAttack = animationClip;
                            }
                        }
                    }

                    monsterContentsInfo.world[0] = gameController.ReadInt(gameController.txtMonsterInfo, j, 11);
                    monsterContentsInfo.world[1] = gameController.ReadInt(gameController.txtMonsterInfo, j, 12);
                    monsterContentsInfo.world[2] = gameController.ReadInt(gameController.txtMonsterInfo, j, 13);

                    monsterContentsInfo.attackFX = gameController.tempFX[0];

                    // if (gameController.ReadInt(gameController.txtMonsterInfo, j, 1) != -1)
                    // {
                    //     monsterContentsInfo.avatar[0] = gameController.FindAvatar("IconMInbox_M" + getID);
                    //     monsterContentsInfo.avatar[1] = gameController.FindAvatar("IconMActive_M" + getID);
                    // }
                    // else
                    // {
                    //     monsterContentsInfo.avatar[0] = gameController.FindAvatar("IconMInbox_ME" + getID);
                    //     monsterContentsInfo.avatar[1] = gameController.FindAvatar("IconMActive_ME" + getID);
                    // }
                }
            }

            PrefabUtility.SaveAsPrefabAsset(monsterPrefabs, "Assets/MyAssets/Prefabs/MonsterEvolveRemake/ME" + getID + ".prefab");

        }
    }
}