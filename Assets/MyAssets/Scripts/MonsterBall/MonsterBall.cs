
/* Copyright © @ThanhDV */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class MonsterBall : MonoBehaviour
{
    public Rigidbody rb;
    public Transform freeMonsterFX;
    private bool wasCollision;

    public void OnSpawn(Rigidbody _rb, Transform _freeMonsterFX)
    {
        rb = _rb;
        freeMonsterFX = _freeMonsterFX;

        rb.isKinematic = false;
        wasCollision = false;

        GameController.ins.listBallSpawned.Add(transform);

        foreach (BtnChoseMonster btn in UIManager.ins.monsterView.allBallButton)
        {
            btn.thisBtn.interactable = false;
        }
    }

    public void OnDespawn(float delay = 0)
    {
        if (!gameObject.activeSelf) return;

        transform.DOScale(transform.localScale, delay).OnComplete(() =>
        {
            rb.isKinematic = false;
            wasCollision = false;
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            GameController.ins.listBallSpawned.Clear();
            Destroy(gameObject);

            foreach (BtnChoseMonster btn in UIManager.ins.monsterView.allBallButton)
            {
                btn.thisBtn.interactable = true;
                GameController.ins.SaveMonsterView();
            }
        });
    }

    private void OnCollisionEnter(Collision other)
    {
        if (wasCollision) return;

        wasCollision = true;

        GameObject otherObj = other.gameObject;

        if (otherObj.CompareTag(StaticString.tagEnemyMonster) || otherObj.CompareTag(StaticString.tagPlane) || otherObj.CompareTag(StaticString.tagPlayerMonster))
        {
            ParticleSystem ps = PoolVFX.pool.Spawn(freeMonsterFX, transform.position, Quaternion.Euler(-90, 0, 0)).GetComponent<ParticleSystem>();
            AudioManager.ins.PlayOther("Pop", 0.5f);
            PoolVFX.pool.Despawn(ps.transform, ps.main.duration);
            PlayerMonster pm = GameController.ins.SpawnPlayerMonster(GameController.ins.FindMonster(GameController.ins.chosenMonster), transform.position, Quaternion.identity).GetComponent<PlayerMonster>();
            OnDespawn();
            return;
        }
        else
        {
            AudioManager.ins.PlayOther("ThrowFail");
            transform.DOJump(GameController.ins.player.attackPos.position, 1f, 1, 0.75f).OnComplete(() =>
            {
                GameController.ins.currentBtnChoseMonster.isSpawnedObj = false;
                GameController.ins.player.animator.SetTrigger("GetBall");
                OnDespawn();
            });
        }
    }
}
