using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerBallAuto : MonoBehaviour
{
    [SerializeField] private Rigidbody rb;
    [SerializeField] private Transform freeMonsterFX;
    [SerializeField] private GameObject monsterInBoxPrefabs;

    public void OnSpawn(MonsterInfo monster, Vector3 endPoint, BtnChoseMonster _btnControllThisMonster = null)
    {
        rb.isKinematic = false;

        GameObject monsterOnHand = Instantiate(monsterInBoxPrefabs, transform);
        monsterOnHand.AddComponent<RotateMonsterInBox>();
        monsterOnHand.GetComponent<MeshFilter>().mesh = monster.transform.GetChild(1).GetComponent<SkinnedMeshRenderer>().sharedMesh;
        monsterOnHand.GetComponent<MeshRenderer>().material = monster.transform.GetChild(1).GetComponent<SkinnedMeshRenderer>().sharedMaterial;

        monsterOnHand.transform.position = transform.position + (transform.up * 0.065f);
        monsterOnHand.transform.rotation = transform.rotation;

        Vector3 baseSz = GetComponent<Renderer>().bounds.size;
        Vector3 mSz = monsterOnHand.GetComponent<Renderer>().bounds.size;

        float[] findMax = new float[3] { baseSz.x / mSz.x, baseSz.y / mSz.y, baseSz.z / mSz.z };
        monsterOnHand.transform.localScale = Vector3.one * 0.5f * Mathf.Max(findMax);

        GameController.ins.listBallSpawned.Add(transform);

        transform.localScale = Vector3.one * 0.45f;

        transform.DOJump(endPoint, 3f, 1, 1.5f).SetEase(Ease.Linear).OnComplete(() =>
        {
            ParticleSystem ps = PoolVFX.pool.Spawn(freeMonsterFX, transform.position, Quaternion.Euler(-90, 0, 0)).GetComponent<ParticleSystem>();
            AudioManager.ins.PlayOther("Pop", 0.5f);
            PoolVFX.pool.Despawn(ps.transform, ps.main.duration);
            if (GameController.ins.isInArena)
            {
                GameController.ins.SpawnMonsterBuff(monster, transform.position, Quaternion.identity);
            }
            else
            {
                GameController.ins.SpawnPlayerMonster(monster, transform.position, Quaternion.identity, _btnControllThisMonster);
            }
            OnDespawn();
        });
    }

    private void Update()
    {
        transform.Rotate(0, 1, 0);
    }

    public void OnDespawn(float delay = 0)
    {
        if (!gameObject.activeSelf) return;

        transform.DOScale(transform.localScale, delay).OnComplete(() =>
        {
            rb.isKinematic = false;
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            GameController.ins.listBallSpawned.Clear();
            PoolMonsterBall.pool.Despawn(transform);
        });
    }
}
