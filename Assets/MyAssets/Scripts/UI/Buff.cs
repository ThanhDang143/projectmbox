using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Buff : MonoBehaviour
{
    public Image thisImg;
    public Image subImg;
    public RectTransform thisRectTransform;
    public Sprite[] buffSprite;
    public Sprite[] buffNoAdsSprite;
    public int thisBuff;
    [HideInInspector] public MonsterInfo monsterBuff;
    private int btnID;

    public void OnSpawn(int _thisBuffID, int _btnID)
    {
        thisBuff = _thisBuffID;
        btnID = _btnID;
        if (PlayerPrefs.GetInt(ManagerAds.Save_Remove_RewardAds) == 1)
        {
            thisImg.sprite = buffNoAdsSprite[thisBuff];
        }
        else
        {
            thisImg.sprite = buffSprite[thisBuff];
        }

        if (thisBuff == 4)
        {
            List<int> monsterInLv = GameController.ins.GetKindOfMonster();

            int idMonsterBuff = monsterInLv[monsterInLv.Count - 1] + Random.Range(-2, 3) + 100;
            monsterBuff = GameController.ins.FindMonster(idMonsterBuff);

            if (monsterBuff == null)
            {
                idMonsterBuff = GameController.ins.allMonster.Length - 11;
                monsterBuff = GameController.ins.allMonster[idMonsterBuff];
            }

            subImg.enabled = true;
            subImg.sprite = monsterBuff.avatar[1];
            subImg.SetNativeSize();
        }
        else
        {
            subImg.enabled = false;
        }
    }

    public void ChooseThisBuff()
    {
        string buffName = "";
        switch (btnID)
        {
            case 0:
                buffName = "HP";
                break;
            case 1:
                buffName = "AtkDmg";
                break;
            case 2:
                buffName = "AtkSpeed";
                break;
            case 3:
                buffName = "AtkRange";
                break;
            case 4:
                buffName = "MonsterLv2";
                break;
        }
        ManagerAds.ins.ShowRewarded((s) =>
          {
              if (!s) return;

              AudioManager.ins.PlayOther("Buff");
              UIManager.ins.sIngame.btnSkip.gameObject.SetActive(true);
              UIManager.ins.sIngame.btnNo.SetActive(false);
              if (btnID == 1)
              {
                  UIManager.ins.sIngame.buff2.thisRectTransform.DOAnchorPosX(500, 0.25f).OnComplete(() =>
                  {
                      Destroy(UIManager.ins.sIngame.buff2.gameObject);
                  });
              }
              else
              {
                  UIManager.ins.sIngame.buff1.thisRectTransform.DOAnchorPosX(-500, 0.25f).OnComplete(() =>
                  {
                      Destroy(UIManager.ins.sIngame.buff1.gameObject);
                  });
              }
              thisRectTransform.DOScale(0.4f, 0.25f);
              transform.DOMove(UIManager.ins.sIngame.showBuffPos.transform.position, 0.25f).OnComplete(() =>
              {
                  PlayerPrefs.SetInt(StaticString.saveBuff, thisBuff);

                  int checkMap = GameController.ins.ReadInt(GameController.ins.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14);

                  if (checkMap == 1) //BossMap
                  {
                      foreach (BtnChoseMonster btn in UIManager.ins.monsterView.allBallButton)
                      {
                          btn.thisBtn.interactable = false;
                      }
                      UIManager.ins.sHome.monsterBuff = monsterBuff;
                      UIManager.ins.sHome.IntroSpecialMap();
                  }
                  else if (checkMap == 2) //ArenaMap
                  {
                      UIManager.ins.sHome.monsterBuff = monsterBuff;
                      UIManager.ins.sHome.IntroSpecialMap();
                  }
                  else //NormalMap
                  {
                      foreach (BtnChoseMonster btn in UIManager.ins.monsterView.allBallButton)
                      {
                          btn.thisBtn.interactable = true;
                      }
                      GameController.ins.isPlaying = true;

                      if (thisBuff == 4)
                      {
                          Vector3 pos = GameController.ins.currentMap.monsterBuffPos.position;
                          PoolVFX.pool.Spawn(GameController.ins.player.freeMonsterFX, pos, Quaternion.identity);
                          UIManager.ins.sIngame.buffMonster = GameController.ins.SpawnMonsterBuff(monsterBuff, pos, Quaternion.identity);
                      }
                  }

                  GameController.ins.isChoosingBuff = false;
                  UIManager.ins.sIngame.touchField.enabled = true;
                  GetComponent<Button>().enabled = false;
              });
          }, "Buff " + buffName);
    }
}
