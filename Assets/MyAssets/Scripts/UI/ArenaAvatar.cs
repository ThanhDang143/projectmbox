using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArenaAvatar : MonoBehaviour
{
    public Image imgBorder;
    public Image imgAvatar;
    private int IDMonster;

    public void OnSpawn(int _IDMonster)
    {
        IDMonster = _IDMonster;
        if (_IDMonster == PlayerPrefs.GetInt(StaticString.saveAvatar))
        {
            imgBorder.enabled = true;
        }
        else
        {
            imgBorder.enabled = false;
        }
        imgAvatar.sprite = GameController.ins.FindMonster(_IDMonster).avatar[1];
        imgAvatar.SetNativeSize();
    }

    public void BtnUse()
    {
        if (!imgBorder.enabled)
        {
            foreach (ArenaAvatar aa in UIManager.ins.pChangeInfo.arenaAvatars)
            {
                aa.imgBorder.enabled = false;
            }
            imgBorder.enabled = true;
        }
        PlayerPrefs.SetInt(StaticString.saveAvatar, IDMonster);
        UIManager.ins.pChangeInfo.mainAvatar.sprite = GameController.ins.FindMonster(PlayerPrefs.GetInt(StaticString.saveAvatar)).avatar[1];
        UIManager.ins.pChangeInfo.mainAvatar.SetNativeSize();
    }
}
