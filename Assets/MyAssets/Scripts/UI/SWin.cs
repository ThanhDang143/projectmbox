
/* Copyright © @ThanhDV */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using SendEventAppMetrica;

public class SWin : UIController
{
    public Image imgRewardGlovesBG;
    public Image imgRewardGloves;
    public Button btnClaimGloves;
    public Button btnNext;
    public Sprite[] btnClaimGlovesSprite;

    public override void Show(bool value)
    {
        base.Show(value);
        if (value)
        {
            float finishTime = Time.time - GameController.ins.timeStartLevel;
            LevelFinish.CustomParameters(new Dictionary<string, object>
            {
                {LevelFinishParameters.game_mode , GameController.ins.gameMode},
                {LevelFinishParameters.level_type , GameController.ins.levelType},
                {LevelFinishParameters.level_number , GameController.ins.levelNumber},
                {LevelFinishParameters.result , "Win"},
                {LevelFinishParameters.time , finishTime},
            });
            SendEventManager.ins.SendEventBuffer(LevelFinish.GetEventAppmetrica());

            AudioManager.ins.PlayOther("Win");
            if (UIManager.ins.sIngame.buff1 != null)
            {
                Destroy(UIManager.ins.sIngame.buff1.gameObject);
            }
            else if (UIManager.ins.sIngame.buff2 != null)
            {
                Destroy(UIManager.ins.sIngame.buff2.gameObject);
            }
            btnNext.interactable = false;
            btnClaimGloves.gameObject.SetActive(false);

            int rewardGlovesID = PlayerPrefs.GetInt(StaticString.saveRewardGloves);
            imgRewardGlovesBG.sprite = GameController.ins.allGlovesBg[rewardGlovesID];
            imgRewardGloves.sprite = GameController.ins.allGlovesAvatar[rewardGlovesID];
            imgRewardGloves.fillAmount = GameController.ins.LoadGloves()[rewardGlovesID];
            imgRewardGloves.SetNativeSize();
            imgRewardGlovesBG.SetNativeSize();
            RewardGloves(0.125f, imgRewardGloves);
        }
    }

    public void RewardGloves(float result, Image imgResult)
    {
        StartCoroutine(IERewardGloves(result, imgResult));
    }

    public IEnumerator IERewardGloves(float result, Image imgResult)
    {
        int rewardGlovesID = PlayerPrefs.GetInt(StaticString.saveRewardGloves);
        GameController.ins.SaveGloves(rewardGlovesID, GameController.ins.LoadGloves()[rewardGlovesID] + result);

        if (result > 0)
        {
            while (imgResult.fillAmount <= GameController.ins.LoadGloves()[rewardGlovesID])
            {
                imgResult.fillAmount += 0.01f;
                if (imgResult.fillAmount >= 1)
                {
                    if (PlayerPrefs.GetInt(ManagerAds.Save_Remove_RewardAds) == 1)
                    {
                        btnClaimGloves.image.sprite = btnClaimGlovesSprite[0];
                        if (PlayerPrefs.GetInt(ManagerAds.Save_Remove_RewardAds) == 1)
                        {
                            GameController.ins.SaveGloves(rewardGlovesID, GameController.ins.LoadGloves()[rewardGlovesID] + 1.5f);
                        }
                    }
                    else
                    {
                        btnClaimGloves.image.sprite = btnClaimGlovesSprite[1];
                    }
                    btnClaimGloves.gameObject.SetActive(true);
                    btnNext.interactable = true;
                    yield break;
                }
                yield return null;
            }
        }
        else if (result < 0)
        {
            while (imgResult.fillAmount >= GameController.ins.LoadGloves()[rewardGlovesID])
            {
                imgResult.fillAmount -= 0.001f;
                if (imgResult.fillAmount <= 0)
                {
                    btnNext.interactable = true;
                    yield break;
                }
                yield return null;
            }
        }

        btnNext.interactable = true;
    }

    public void BtnClaimGloves()
    {
        AudioManager.ins.PlayOther("Click");
        ManagerAds.ins.ShowRewarded((s) =>
         {
             if (!s) return;

             int rewardGlovesID = PlayerPrefs.GetInt(StaticString.saveRewardGloves);
             GameController.ins.SaveGloves(rewardGlovesID, GameController.ins.LoadGloves()[rewardGlovesID] + 1.5f);

             BtnNext();
         }, "ClaimGloves");

    }

    public void BtnNext()
    {
        GameController gc = GameController.ins;
        AudioManager.ins.PlayOther("Click");
        if (PlayerPrefs.GetInt(StaticString.checkFirstTimePlay) == 1 || gc.isInSpecialWorld || gc.isInArena)
        {
            PlayerPrefs.SetInt(StaticString.checkFirstTimePlay, 0);
        }
        else
        {
            PlayerPrefs.SetInt(StaticString.saveLevel, PlayerPrefs.GetInt(StaticString.saveLevel) + 1);
        }

        gc.player.animator.Play(gc.player.clipIdle1.name);
        Destroy(gc.player.handEvents.newBall);
        Destroy(gc.player.handEvents.monsterOnHand);

        gc.isLosing = false;
        gc.isInSpecialWorld = false;
        gc.ResetMonsterBallBoss();
        gc.LoadMonsterView();

        int rewardGlovesID = PlayerPrefs.GetInt(StaticString.saveRewardGloves);
        if (gc.LoadGloves()[rewardGlovesID] >= 1)
        {
            gc.ChooseRewardGloves();
        }

        if (!gc.isInArena)
        {
            PlayerPrefs.SetInt(StaticString.saveExchangePoint, PlayerPrefs.GetInt(StaticString.saveExchangePoint) + 1);
            if (PlayerPrefs.GetInt(StaticString.saveExchangePoint) >= 6 && CheckAvailableMonster())
            {
                gc.LoadExchangeMap();
                UIManager.ins.ShowScreen(ScreenX.SExchange);
            }
            else
            {
                gc.LoadLevel();
                UIManager.ins.ShowScreen(ScreenX.SHome);
            }
        }
        else
        {
            PlayerPrefs.SetInt(StaticString.saveArenaPoint, PlayerPrefs.GetInt(StaticString.saveArenaPoint) + Random.Range(5, 11));
            if (PlayerPrefs.GetInt(StaticString.saveArenaRank) > 100)
            {
                PlayerPrefs.SetInt(StaticString.saveUpRankPoint, PlayerPrefs.GetInt(StaticString.saveUpRankPoint) + Random.Range(2, 6));

                if (PlayerPrefs.GetInt(StaticString.saveArenaPoint) >= gc.LoadDataRank(100).pointArena)
                {
                    int currentRank = PlayerPrefs.GetInt(StaticString.saveArenaRank);
                    gc.UpdateArenaPlayerRank(currentRank, 100);
                    PlayerPrefs.SetInt(StaticString.saveArenaRank, 100);
                }
                else
                {
                    if (PlayerPrefs.GetInt(StaticString.saveUpRankPoint) >= 10)
                    {
                        PlayerPrefs.SetInt(StaticString.saveUpRankPoint, 0);
                        int currentRank = PlayerPrefs.GetInt(StaticString.saveArenaRank);
                        if (currentRank > 1500)
                        {
                            PlayerPrefs.SetInt(StaticString.saveArenaRank, currentRank - Random.Range(5, 11));
                        }
                        else if (currentRank > 1000)
                        {
                            PlayerPrefs.SetInt(StaticString.saveArenaRank, currentRank - Random.Range(3, 7));
                        }
                        else if (currentRank > 500)
                        {
                            PlayerPrefs.SetInt(StaticString.saveArenaRank, currentRank - Random.Range(2, 5));
                        }
                        else
                        {
                            PlayerPrefs.SetInt(StaticString.saveArenaRank, currentRank - Random.Range(1, 3));
                        }

                        if (PlayerPrefs.GetInt(StaticString.saveArenaRank) <= 100)
                        {
                            gc.UpdateArenaPlayerRank(currentRank, 100);
                            PlayerPrefs.SetInt(StaticString.saveArenaRank, 100);
                        }
                    }
                }
            }
            else
            {
                int oldRank = PlayerPrefs.GetInt(StaticString.saveArenaRank);
                while (PlayerPrefs.GetInt(StaticString.saveArenaPoint) > gc.LoadDataRank(PlayerPrefs.GetInt(StaticString.saveArenaRank) - 1).pointArena)
                {
                    PlayerPrefs.SetInt(StaticString.saveArenaRank, PlayerPrefs.GetInt(StaticString.saveArenaRank) - 1);
                }
                gc.UpdateArenaPlayerRank(oldRank, PlayerPrefs.GetInt(StaticString.saveArenaRank));
            }

            gc.isInArena = false;
            gc.LoadLevel();
            UIManager.ins.ShowScreen(ScreenX.SArena);
        }
    }

    public bool CheckAvailableMonster()
    {
        List<int> getMonsterToExchange = new List<int>();

        for (int i = 1; i < UIManager.ins.monsterView.allBallButton.Count; i++)
        {
            int IDMonster = UIManager.ins.monsterView.allBallButton[i].IDMonster;
            if (IDMonster > 0)
            {
                getMonsterToExchange.Add(IDMonster);
            }
        }

        if (getMonsterToExchange.Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
