
/* Copyright © @ThanhDV */

using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BtnChoseMonster : MonoBehaviour
{
    [Header("Var For Monster")]
    public int IDMonster;
    public Transform monsterSpawned;
    public bool isSpawnedObj;

    [Header("Var For VFX")]
    public Button thisBtn;
    public Text thisText;
    public Image imgAvatar;
    public ParticleSystem catchEffect;

    [Header("Var For Button")]
    public int unlockAtLevel;
    public Button btnFreeMonster;
    public Button btnEvolve;
    public Button btnUnlockAtLv;
    public Image imgBorder;
    public Sprite normalSprite;
    public Sprite lockSprite;

    [Header("Other")]
    public Sprite defaultAvatar;
    public Sprite imgEvolveAds;
    public Sprite imgEvolveNoAds;
    private bool canPlayFX = true;

    public void OnSpawn()
    {
        if (IDMonster > 0)
        {
            thisText.text = GameController.ins.FindMonster(IDMonster).monsterName;
            imgAvatar.sprite = GameController.ins.FindMonster(IDMonster).avatar[0];
            imgAvatar.SetNativeSize();
            thisBtn.image.color = Color.white;
        }
        else
        {
            if (unlockAtLevel < PlayerPrefs.GetInt(StaticString.saveLevel) + 1)
            {
                imgAvatar.sprite = defaultAvatar; ;
                imgAvatar.SetNativeSize();
            }
        }

        // Set up Children Button
        if (!GameController.ins.isPlaying)
        {
            if (unlockAtLevel < PlayerPrefs.GetInt(StaticString.saveLevel) + 1)
            {
                if (unlockAtLevel == PlayerPrefs.GetInt(StaticString.saveLevel) && IDMonster <= 0 && canPlayFX)
                {
                    StartCoroutine(IEPlayFX());
                }
                btnUnlockAtLv.gameObject.SetActive(false);
                thisBtn.image.sprite = normalSprite;
                thisBtn.image.SetNativeSize();
                thisBtn.enabled = true;
            }
            else
            {
                btnUnlockAtLv.gameObject.SetActive(true);
                thisBtn.image.sprite = lockSprite;
                thisBtn.image.SetNativeSize();
                btnUnlockAtLv.GetComponentInChildren<Text>().text = "Level " + unlockAtLevel;
                thisBtn.enabled = false;
            }

            if (IDMonster < 0)
            {
                btnFreeMonster.gameObject.SetActive(false);
                btnEvolve.gameObject.SetActive(false);
            }
            else
            {
                btnFreeMonster.gameObject.SetActive(true);
                if (GameController.ins.FindMonster(IDMonster).IDMonsterEvolve == -1)
                {
                    btnEvolve.gameObject.SetActive(false);
                }
                else
                {
                    btnEvolve.gameObject.SetActive(true);
                }
            }
        }
        else
        {
            btnEvolve.gameObject.SetActive(false);
            btnFreeMonster.gameObject.SetActive(false);

            if (unlockAtLevel < PlayerPrefs.GetInt(StaticString.saveLevel) + 1)
            {
                btnUnlockAtLv.gameObject.SetActive(false);
                thisBtn.image.sprite = normalSprite;
                thisBtn.image.SetNativeSize();
                thisBtn.enabled = true;
            }
            else
            {
                btnUnlockAtLv.gameObject.SetActive(true);
                thisBtn.image.sprite = lockSprite;
                thisBtn.image.SetNativeSize();
                btnUnlockAtLv.GetComponentInChildren<Text>().text = "Level " + unlockAtLevel;
                thisBtn.enabled = false;
            }
        }

        if (PlayerPrefs.GetInt(ManagerAds.Save_Remove_RewardAds) == 1)
        {
            btnEvolve.image.sprite = imgEvolveNoAds;
        }
        else
        {
            btnEvolve.image.sprite = imgEvolveAds;
        }

        isSpawnedObj = false;
    }

    public void BtnFreeMonster()
    {
        AudioManager.ins.PlayOther("FreeMonster");
        IDMonster = -1;
        monsterSpawned = null;
        isSpawnedObj = false;
        thisText.text = "Empty";
        imgAvatar.sprite = null;
        OnSpawn();
        GameController.ins.SaveMonsterView();
    }

    private IEnumerator IEPlayFX()
    {
        AudioManager.ins.PlayOther("Win");
        for (int i = 0; i < 5; i++)
        {
            catchEffect.Stop();
            catchEffect.Play();
            yield return new WaitForSeconds(0.5f);
        }
        canPlayFX = false;
    }

    public void BtnEvolve()
    {
        if (GameController.ins.isChoosingBuff) return;

        AudioManager.ins.PlayOther("Click");
        ManagerAds.ins.ShowRewarded((s) =>
         {
             if (!s) return;

             GameController gc = GameController.ins;

             gc.ResetMonsterBallBoss();

             gc.currentBtnChoseMonster = this;

             PoolMap.pool.Despawn(gc.currentMap.transform);
             gc.currentMap = PoolMap.pool.Spawn(gc.evolveMaps.transform).GetComponent<MapController>();

             gc.player.transform.DOMove(gc.currentMap.playerPos[0].position, 0.1f);
             gc.player.transform.DOLocalRotate(Vector3.zero, 0.1f);
             gc.player.transMainCam.DOLocalRotate(Vector3.zero, 0.1f);

             gc.isEvolving = true;

             gc.InstantiatePlayerMonster(gc.FindMonster(IDMonster), gc.currentMap.enemiesPos[0].position, Quaternion.Euler(0, 180, 0));

             IDMonster = gc.FindMonster(IDMonster).IDMonsterEvolve;
             gc.SaveMonsterView();

             UIManager.ins.ShowScreen(ScreenX.SEvolve);
         }, "EvolveMonster");
    }

    public int CountMonsterAvailable(int IDMonster)
    {
        int c = 0;
        for (int i = 0; i < UIManager.ins.monsterView.allBtnMonster.Count; i++)
        {
            if (UIManager.ins.monsterView.allBtnMonster[i].IDMonster == IDMonster)
            {
                c++;
            }
        }

        return c;
    }

    public int CountArenaMonsterChosen(int IDMonster)
    {
        int c = 0;
        for (int i = 0; i < UIManager.ins.pChangeInfo.allSlotMonster.Length; i++)
        {
            if (UIManager.ins.pChangeInfo.allSlotMonster[i].IDMonster == IDMonster)
            {
                c++;
            }
        }

        return c;
    }

    public void ChooseMonster(bool isPlayerClick = false)
    {
        if (UIManager.ins.currentPopup == Popup.PTutorial4)
        {
            UIManager.ins.ShowPopup(Popup.None);
            transform.DOScale(transform.localScale, 0.5f).OnComplete(() =>
            {
                UIManager.ins.ShowPopup(Popup.PTutorial3);
            });
        }

        if (UIManager.ins.currentPopup == Popup.PChangeInfo)
        {
            AudioManager.ins.PlayOther("Click");
            if (CountArenaMonsterChosen(IDMonster) < CountMonsterAvailable(IDMonster))
            {
                for (int i = 0; i < UIManager.ins.pChangeInfo.allSlotMonster.Length; i++)
                {
                    ArenaMonsterSlot ams = UIManager.ins.pChangeInfo.allSlotMonster[i];
                    if (ams.IDMonster == -1)
                    {
                        ams.IDMonster = IDMonster;
                        ams.btnUnchoose.gameObject.SetActive(true);
                        ams.imgAvatarMonster.sprite = GameController.ins.FindMonster(IDMonster).avatar[1];
                        ams.imgAvatarMonster.SetNativeSize();
                        ams.imgAvatarMonster.color = Color.white;
                        return;
                    }
                }
            }

            return;
        }

        PlayerController pc = GameController.ins.player;
        if (isSpawnedObj && !monsterSpawned && thisBtn.image.color == Color.red) return;
        if (GameController.ins.listBallSpawned.Count > 0) return;

        if (pc.allCatchFX.Count > 0)
        {
            GameController.ins.player.animator.SetTrigger("Catching");
            GameController.ins.player.animator.SetTrigger("CatchCancel");
            return;
        }

        if ((pc.animator.GetCurrentAnimatorStateInfo(0).IsName(pc.clipHoldMons.name) || pc.animator.GetCurrentAnimatorStateInfo(0).IsName("getMons")) && isPlayerClick)
        {
            if (GameController.ins.currentBtnChoseMonster == this) return;
        }

        if (isPlayerClick)
        {
            try
            {
                MonsterInfo mi = monsterSpawned.GetComponent<MonsterInfo>();

                if (mi.health <= mi.maxHealth * 0.2f) return;
            }
            catch (System.Exception) { }

            AudioManager.ins.PlayOther("ChooseMonster");
        }

        GameController.ins.chosenMonster = IDMonster;
        GameController.ins.currentBtnChoseMonster = this;

        for (int i = 1; i < UIManager.ins.monsterView.allBallButton.Count; i++)
        {
            BtnChoseMonster btnController = UIManager.ins.monsterView.allBallButton[i];
            btnController.imgBorder.enabled = false;
        }

        imgBorder.enabled = true;

        if (isSpawnedObj)
        {
            if (monsterSpawned != null)
            {
                MonsterInfo monsterSpawnedInfo = monsterSpawned.GetComponent<MonsterInfo>();

                imgAvatar.sprite = monsterSpawnedInfo.avatar[0];
                imgAvatar.SetNativeSize();
                monsterSpawned.GetComponent<PlayerMonster>().OnDespawn(false);
                monsterSpawned = null;
            }
            isSpawnedObj = false;
            thisBtn.image.color = Color.white;
        }

        if (!GameController.ins.isPlaying || GameController.ins.isMoving) return;

        if (IDMonster != -1)
        {
            for (int i = 1; i < UIManager.ins.monsterView.allBallButton.Count; i++)
            {
                BtnChoseMonster btnController = UIManager.ins.monsterView.allBallButton[i];
                btnController.thisBtn.interactable = true;
            }
        }

        pc.animator.SetTrigger("GetBall");
    }
}
