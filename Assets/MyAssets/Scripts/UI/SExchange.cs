using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class SExchange : UIController
{
    public RectTransform btnChange;
    public RectTransform btnExchange;
    public RectTransform btnNo;

    public Sprite[] imgBtnChangeSprite;
    public Sprite[] imgBtnExchangeSprite;

    [SerializeField] public MonsterInfo playerMonster;
    [SerializeField] public MonsterInfo monsterToExchange;
    [HideInInspector] public BtnChoseMonster btnControllMonsterExchange;

    public Transform freeMonsterFX;

    public override void Show(bool value)
    {
        base.Show(value);
        if (value)
        {
            PlayerPrefs.SetInt(StaticString.saveExchangePoint, 0);

            if (PlayerPrefs.GetInt(ManagerAds.Save_Remove_RewardAds) == 1)
            {
                btnChange.GetComponent<Button>().image.sprite = imgBtnChangeSprite[0];
                btnExchange.GetComponent<Button>().image.sprite = imgBtnExchangeSprite[0];
            }
            else
            {
                btnChange.GetComponent<Button>().image.sprite = imgBtnChangeSprite[1];
                btnExchange.GetComponent<Button>().image.sprite = imgBtnExchangeSprite[1];
            }

            btnChange.anchoredPosition = new Vector2(-800, 300);
            btnExchange.anchoredPosition = new Vector2(800, 300);
            btnNo.anchoredPosition = new Vector2(0, -400);

            btnChange.DOAnchorPos(new Vector2(-175, 300), 0.5f);
            btnExchange.DOAnchorPos(new Vector2(175, 300), 0.5f).OnComplete(() =>
            {
                btnNo.DOAnchorPos(new Vector2(0, -400), 1.5f).OnComplete(() =>
                {
                    btnNo.DOAnchorPos(new Vector2(0, 200), 0.5f);
                });
            });
        }
    }

    public void BtnChange()
    {
        AudioManager.ins.PlayOther("Click");
        ManagerAds.ins.ShowRewarded((s) =>
         {
             if (!s) return;

             playerMonster.GetComponent<EnemyMonster>().OnDespawn();
             monsterToExchange.GetComponent<EnemyMonster>().OnDespawn();

             GameController.ins.GetMonsterToExchange();
         }, "ChangeMonsterToExchange");
    }

    public void BtnExchange()
    {
        AudioManager.ins.PlayOther("TradeConfirmed");
        ManagerAds.ins.ShowRewarded((s) =>
         {
             if (!s) return;

             MonsterInfo info = monsterToExchange.GetComponent<MonsterInfo>();

             playerMonster.transform.DOScale(0, 0.15f);
             Vector3 dyingFXPos1 = playerMonster.transform.GetChild(1).GetComponent<SkinnedMeshRenderer>().bounds.center;
             Vector3 dyingFXPos2 = monsterToExchange.transform.GetChild(1).GetComponent<SkinnedMeshRenderer>().bounds.center;
             Transform fx1 = PoolVFX.pool.Spawn(info.dyingFX, dyingFXPos1, Quaternion.Euler(-90, 0, 0));
             PoolVFX.pool.Despawn(fx1, 3);

             monsterToExchange.transform.DOScale(0, 0.15f);
             Transform fx2 = PoolVFX.pool.Spawn(info.dyingFX, dyingFXPos2, Quaternion.Euler(-90, 0, 0));
             PoolVFX.pool.Despawn(fx2, 3);

             Transform playerTrans = GameController.ins.player.transform;
             Transform fx;
             fx = Instantiate(info.attackFX.gameObject, info.transform.position, info.transform.rotation).transform;

             fx.DOJump(playerTrans.position - playerTrans.right - playerTrans.up, 1f, 1, 1.5f).OnComplete(() =>
             {
                 btnControllMonsterExchange.IDMonster = info.ID;
                 GameController.ins.SaveMonsterCollection(info.ID);
                 GameController.ins.SaveMonsterView();
                 GameController.ins.listEnemyMonster.Remove(playerMonster.GetComponent<EnemyMonster>());
                 GameController.ins.listEnemyMonster.Remove(monsterToExchange.GetComponent<EnemyMonster>());
                 PoolEnemyMonster.pool.Despawn(playerMonster.transform);
                 PoolEnemyMonster.pool.Despawn(monsterToExchange.transform);
                 Destroy(fx.gameObject);

                 btnChange.DOAnchorPos(new Vector2(-800, 300), 0.5f);
                 btnExchange.DOAnchorPos(new Vector2(800, 300), 0.5f);
                 btnNo.DOAnchorPos(new Vector2(0, -400), 0.5f).OnComplete(() =>
                 {
                     GameController.ins.isExchangeMap = false;
                     GameController.ins.player.handRenderer.enabled = true;
                     GameController.ins.LoadLevel();
                     UIManager.ins.ShowScreen(ScreenX.SHome);
                 });
             });
         }, "ExchangeMonster");
    }

    public void BtnNo()
    {
        MonsterInfo info = monsterToExchange.GetComponent<MonsterInfo>();

        Vector3 dyingFXPos1 = playerMonster.transform.GetChild(1).GetComponent<SkinnedMeshRenderer>().bounds.center;
        Vector3 dyingFXPos2 = monsterToExchange.transform.GetChild(1).GetComponent<SkinnedMeshRenderer>().bounds.center;
        Transform fx1 = PoolVFX.pool.Spawn(info.dyingFX, dyingFXPos1, Quaternion.Euler(-90, 0, 0));
        PoolVFX.pool.Despawn(fx1, 3);
        playerMonster.transform.DOScale(0, 0.15f);

        Transform fx2 = PoolVFX.pool.Spawn(info.dyingFX, dyingFXPos2, Quaternion.Euler(-90, 0, 0));
        PoolVFX.pool.Despawn(fx2, 3);
        monsterToExchange.transform.DOScale(0, 0.15f).OnComplete(() =>
        {
            GameController.ins.listEnemyMonster.Remove(playerMonster.GetComponent<EnemyMonster>());
            GameController.ins.listEnemyMonster.Remove(monsterToExchange.GetComponent<EnemyMonster>());
            PoolEnemyMonster.pool.Despawn(playerMonster.transform);
            PoolEnemyMonster.pool.Despawn(monsterToExchange.transform);

            btnChange.DOAnchorPos(new Vector2(-800, 300), 0.5f);
            btnExchange.DOAnchorPos(new Vector2(800, 300), 0.5f);
            btnNo.DOAnchorPos(new Vector2(0, -400), 0.5f).OnComplete(() =>
            {
                GameController.ins.isExchangeMap = false;
                GameController.ins.player.handRenderer.enabled = true;
                GameController.ins.LoadLevel();
                UIManager.ins.ShowScreen(ScreenX.SHome);
            });
        });
    }
}
