
/* Copyright © @ThanhDV */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using SendEventAppMetrica;

public class SLose : UIController
{
    public RectTransform groupBtn;
    public Image imgRewardGlovesBG;
    public Image imgRewardGloves;
    public Button btnTryAgain;
    public Sprite[] spriteTryAgain;

    public override void Show(bool value)
    {
        base.Show(value);
        if (value)
        {
            float finishTime = Time.time - GameController.ins.timeStartLevel;
            LevelFinish.CustomParameters(new Dictionary<string, object>
            {
                {LevelFinishParameters.game_mode , GameController.ins.gameMode},
                {LevelFinishParameters.level_type , GameController.ins.levelType},
                {LevelFinishParameters.level_number , GameController.ins.levelNumber},
                {LevelFinishParameters.result , "Lose"},
                {LevelFinishParameters.time , finishTime},
            });
            SendEventManager.ins.SendEventBuffer(LevelFinish.GetEventAppmetrica());

            if (UIManager.ins.sIngame.buff1 != null)
            {
                Destroy(UIManager.ins.sIngame.buff1.gameObject);
            }
            else if (UIManager.ins.sIngame.buff2 != null)
            {
                Destroy(UIManager.ins.sIngame.buff2.gameObject);
            }

            if (PlayerPrefs.GetInt(ManagerAds.Save_Remove_RewardAds) == 1)
            {
                if (GameController.ins.isInArena)
                {
                    btnTryAgain.image.sprite = spriteTryAgain[2];
                }
                else
                {
                    btnTryAgain.image.sprite = spriteTryAgain[0];
                }
            }
            else
            {
                if (GameController.ins.isInArena)
                {
                    btnTryAgain.image.sprite = spriteTryAgain[3];
                }
                else
                {
                    btnTryAgain.image.sprite = spriteTryAgain[1];
                }
            }

            AudioManager.ins.PlayOther("Lose");

            int rewardGlovesID = PlayerPrefs.GetInt(StaticString.saveRewardGloves);
            imgRewardGlovesBG.sprite = GameController.ins.allGlovesBg[rewardGlovesID];
            imgRewardGloves.sprite = GameController.ins.allGlovesAvatar[rewardGlovesID];
            imgRewardGloves.fillAmount = GameController.ins.LoadGloves()[rewardGlovesID];
            imgRewardGloves.SetNativeSize();
            imgRewardGlovesBG.SetNativeSize();

            groupBtn.anchoredPosition = new Vector2(0, -420);

            groupBtn.DOAnchorPos(new Vector2(0, 300), 0.5f);
        }
    }

    public void BtnWatchVideoRetry()
    {
        AudioManager.ins.PlayOther("Click");
        groupBtn.DOAnchorPos(new Vector2(0, -420), 0.25f).OnComplete(() =>
        {
            ManagerAds.ins.ShowRewarded((s) =>
             {
                 if (!s) return;

                 GameController.ins.player.animator.Play(GameController.ins.player.clipIdle1.name);
                 Destroy(GameController.ins.player.handEvents.newBall);
                 Destroy(GameController.ins.player.handEvents.monsterOnHand);

                 GameController.ins.isLosing = false;
                 GameController.ins.ResetMonsterBallBoss();
                 if (!GameController.ins.isInArena)
                 {
                     GameController.ins.LoadMonsterView();

                     PlayerPrefs.SetInt(StaticString.saveExchangePoint, PlayerPrefs.GetInt(StaticString.saveExchangePoint) + 2);

                     GameController.ins.LoadLevel();
                     UIManager.ins.sHome.BtnPlay();
                 }
                 else
                 {
                     GameController.ins.isInArena = false;
                     GameController.ins.LoadLevel();
                     UIManager.ins.ShowScreen(ScreenX.SArena);
                 }
             }, "TryAgainAfterLose");
        });
    }

    public void RewardGloves(float result, Image imgResult)
    {
        StartCoroutine(IERewardGloves(result, imgResult));
    }

    public IEnumerator IERewardGloves(float result, Image imgResult)
    {
        int rewardGlovesID = PlayerPrefs.GetInt(StaticString.saveRewardGloves);
        float s = GameController.ins.LoadGloves()[rewardGlovesID] + result;
        if (s >= 0)
        {
            GameController.ins.SaveGloves(rewardGlovesID, s);
        }
        else
        {
            GameController.ins.SaveGloves(rewardGlovesID, 0);
        }

        if (result > 0)
        {
            while (imgResult.fillAmount <= GameController.ins.LoadGloves()[rewardGlovesID])
            {
                imgResult.fillAmount += 0.001f;
                if (imgResult.fillAmount >= 1)
                {
                    yield break;
                }
                yield return null;
            }
        }
        else if (result < 0)
        {
            while (imgResult.fillAmount >= GameController.ins.LoadGloves()[rewardGlovesID])
            {
                imgResult.fillAmount -= 0.01f;
                if (imgResult.fillAmount <= 0)
                {
                    yield break;
                }
                yield return null;
            }
        }
    }

    public void BtnNo()
    {
        AudioManager.ins.PlayOther("Click");
        RewardGloves(-0.1f, imgRewardGloves);
        groupBtn.DOAnchorPos(new Vector2(0, -420), 0.25f).OnComplete(() =>
        {
            int readCurrentMapType = GameController.ins.ReadInt(GameController.ins.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14);
            if (!GameController.ins.isInArena)
            {
                if (readCurrentMapType == 0) { } // Lose normal level -0
                else if (readCurrentMapType == 1) // Lose normal boss level -1
                {
                    PlayerPrefs.SetInt(StaticString.saveLevel, PlayerPrefs.GetInt(StaticString.saveLevel) - 1);
                }
                else // Lose vip boss level -2
                {
                    PlayerPrefs.SetInt(StaticString.saveLevel, PlayerPrefs.GetInt(StaticString.saveLevel) - 2);
                }
            }

            GameController.ins.player.animator.Play(GameController.ins.player.clipIdle1.name);
            Destroy(GameController.ins.player.handEvents.newBall);
            Destroy(GameController.ins.player.handEvents.monsterOnHand);

            GameController.ins.isLosing = false;
            GameController.ins.ResetMonsterBallBoss();
            GameController.ins.LoadMonsterView();

            if (!GameController.ins.isInArena)
            {
                PlayerPrefs.SetInt(StaticString.saveExchangePoint, PlayerPrefs.GetInt(StaticString.saveExchangePoint) + 2);
                if (PlayerPrefs.GetInt(StaticString.saveExchangePoint) >= 6 && UIManager.ins.sWin.CheckAvailableMonster())
                {
                    GameController.ins.LoadExchangeMap();
                    UIManager.ins.ShowScreen(ScreenX.SExchange);
                }
                else
                {
                    GameController.ins.LoadLevel();
                    UIManager.ins.ShowScreen(ScreenX.SHome);
                }
            }
            else
            {
                GameController gc = GameController.ins;
                PlayerPrefs.SetInt(StaticString.saveArenaPoint, PlayerPrefs.GetInt(StaticString.saveArenaPoint) - Random.Range(3, 7));
                if (PlayerPrefs.GetInt(StaticString.saveArenaRank) > 100)
                {
                    PlayerPrefs.SetInt(StaticString.saveUpRankPoint, PlayerPrefs.GetInt(StaticString.saveUpRankPoint) - Random.Range(2, 6));
                    if (PlayerPrefs.GetInt(StaticString.saveUpRankPoint) <= -10)
                    {
                        PlayerPrefs.SetInt(StaticString.saveUpRankPoint, 0);
                        int currentRank = PlayerPrefs.GetInt(StaticString.saveArenaRank);
                        if (currentRank > 1500)
                        {
                            PlayerPrefs.SetInt(StaticString.saveArenaRank, currentRank + Random.Range(1, 3));
                        }
                        else if (currentRank > 1000)
                        {
                            PlayerPrefs.SetInt(StaticString.saveArenaRank, currentRank + Random.Range(2, 5));
                        }
                        else if (currentRank > 500)
                        {
                            PlayerPrefs.SetInt(StaticString.saveArenaRank, currentRank + Random.Range(3, 7));
                        }
                        else
                        {
                            PlayerPrefs.SetInt(StaticString.saveArenaRank, currentRank + Random.Range(5, 11));
                        }
                    }
                }
                else
                {
                    if (PlayerPrefs.GetInt(StaticString.saveArenaPoint) < gc.LoadDataRank(100).pointArena)
                    {
                        int currentRank = PlayerPrefs.GetInt(StaticString.saveArenaRank);
                        gc.UpdateArenaPlayerRank(currentRank, 101);
                        PlayerPrefs.SetInt(StaticString.saveArenaRank, 101);
                        Debug.Log(101 + "" + currentRank);
                    }
                    else
                    {
                        int oldRank = PlayerPrefs.GetInt(StaticString.saveArenaRank);
                        while (PlayerPrefs.GetInt(StaticString.saveArenaPoint) < gc.LoadDataRank(PlayerPrefs.GetInt(StaticString.saveArenaRank) + 1).pointArena)
                        {
                            PlayerPrefs.SetInt(StaticString.saveArenaRank, PlayerPrefs.GetInt(StaticString.saveArenaRank) + 1);
                        }

                        gc.UpdateArenaPlayerRank(oldRank, PlayerPrefs.GetInt(StaticString.saveArenaRank));
                    }
                }

                GameController.ins.isInArena = false;
                GameController.ins.LoadLevel();
                UIManager.ins.ShowScreen(ScreenX.SArena);
            }

        });
    }
}
