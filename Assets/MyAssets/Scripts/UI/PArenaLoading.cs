using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PArenaLoading : UIController
{
    public Slider loadingSlider;
    public Text txtProgress;
    public RectTransform imgV;
    public RectTransform imgS;
    public RectTransform redTeam;
    public RectTransform blueTeam;
    public Text txtNameRed;
    public Image[] monsterRed;
    public Text txtNameBlue;
    public Image[] monsterBlue;
    public List<int> arenaMonster;


    public override void Show(bool value)
    {
        base.Show(value);
        if (value)
        {
            loadingSlider.value = 0;
            GameController.ins.isInArena = true;

            redTeam.anchoredPosition = new Vector2(0, 1500);
            blueTeam.anchoredPosition = new Vector2(0, -1500);
            imgV.anchoredPosition = new Vector2(-1000, 500);
            imgS.anchoredPosition = new Vector2(1000, -500);

            redTeam.DOAnchorPosY(350, 0.3f);
            blueTeam.DOAnchorPosY(-270, 0.3f);

            imgV.DOAnchorPos(new Vector2(-10, 50), 0.45f);
            imgS.DOAnchorPos(new Vector2(45, 0), 0.45f);

            LoadBlueTeam();
            LoadRedTeam();

            StartCoroutine(IERunLoadingBar());
        }
    }

    private IEnumerator IERunLoadingBar()
    {
        ManagerAds.ins.HideBanner();
        GameController.ins.LoadArena();
        while (loadingSlider.value < loadingSlider.maxValue)
        {
            loadingSlider.value += 0.002f;
            txtProgress.text = (loadingSlider.value * 100).ToString("#.00") + "%";
            yield return null;
        }

        // ManagerAds.Ins.ShowBanner();
        UIManager.ins.ShowScreen(ScreenX.SIngame);
        UIManager.ins.ShowPopup(Popup.None);
    }

    public void LoadBlueTeam()
    {
        txtNameBlue.text = PlayerPrefs.GetString(StaticString.saveArenaName);
        string[] varSaveMonsterKey = new string[5] { StaticString.saveArenaMonster0, StaticString.saveArenaMonster1, StaticString.saveArenaMonster2, StaticString.saveArenaMonster3, StaticString.saveArenaMonster4 };

        for (int i = 0; i < 5; i++)
        {
            int saveMonsterID = PlayerPrefs.GetInt(varSaveMonsterKey[i]);
            if (saveMonsterID > 0)
            {
                monsterBlue[i].sprite = GameController.ins.FindMonster(saveMonsterID).avatar[1];
                monsterBlue[i].SetNativeSize();
                monsterBlue[i].color = Color.white;
            }
            else
            {
                monsterBlue[i].color = new Color(0, 0, 0, 0);
            }
        }
    }

    public void LoadRedTeam()
    {
        LoadRedMonster();
        int saveRank = PlayerPrefs.GetInt(StaticString.saveArenaRank);
        if (saveRank <= 100)
        {
            txtNameRed.text = GameController.ins.LoadDataRank(saveRank).nameArena;
        }
        else
        {
            txtNameRed.text = StaticString.arenaName[Random.Range(0, StaticString.arenaName.Length)];
        }

        for (int i = 0; i < 5; i++)
        {
            int saveMonsterID = arenaMonster[i];
            if (saveMonsterID > 0)
            {
                monsterRed[i].sprite = GameController.ins.FindMonster(saveMonsterID).avatar[1];
                monsterRed[i].SetNativeSize();
                monsterRed[i].color = Color.white;
            }
            else
            {
                monsterRed[i].color = new Color(0, 0, 0, 0);
            }
        }
    }

    public void LoadRedMonster()
    {
        GameController gc = GameController.ins;
        int saveRank = PlayerPrefs.GetInt(StaticString.saveArenaRank);
        arenaMonster = new List<int>();

        if (saveRank > 1500)
        {
            for (int i = 0; i < 5; i++)
            {
                int r = Random.Range(1, 100);
                if (r < 20)
                {
                    arenaMonster.Add(0);
                }              // 20% no monster
                else if (r < 95 && r >= 20)  // 75% monster id 1-15
                {
                    int rIDMonster = Random.Range(0, 15);
                    int rEvolve = Random.Range(0, 100);

                    if (rEvolve < 90) //  90% monster Lv1
                    {
                        arenaMonster.Add(rIDMonster);
                    }
                    else // 10% monster Lv2
                    {
                        int idEvolve = gc.FindMonster(rIDMonster).IDMonsterEvolve;
                        arenaMonster.Add(idEvolve);
                    }
                }
                else                         // 5% monster id 15-40
                {
                    int rIDMonster = Random.Range(15, 40);
                    int rEvolve = Random.Range(0, 100);

                    if (rEvolve < 90) //  90% monster Lv1
                    {
                        arenaMonster.Add(rIDMonster);
                    }
                    else // 10% monster Lv2
                    {
                        int idEvolve = gc.FindMonster(rIDMonster).IDMonsterEvolve;
                        arenaMonster.Add(idEvolve);
                    }
                }
            }
            return;
        }

        if (saveRank > 1000)
        {
            for (int i = 0; i < 5; i++)
            {
                int r = Random.Range(0, 100);
                if (r < 10)
                {
                    arenaMonster.Add(0);
                }                 // 10% no monster
                else if (r < 80 && r >= 10)     // 70% monster id 1-15
                {
                    int rIDMonster = Random.Range(0, 15);
                    int rEvolve = Random.Range(0, 100);

                    if (rEvolve < 80) //  80% monster Lv1
                    {
                        arenaMonster.Add(rIDMonster);
                    }
                    else // 20% monster Lv2
                    {
                        int idEvolve = gc.FindMonster(rIDMonster).IDMonsterEvolve;
                        arenaMonster.Add(idEvolve);
                    }
                }
                else if (r < 95 && r >= 80)     // 15% monster id 15-40
                {
                    int rIDMonster = Random.Range(15, 40);
                    int rEvolve = Random.Range(0, 100);

                    if (rEvolve < 80) //  80% monster Lv1
                    {
                        arenaMonster.Add(rIDMonster);
                    }
                    else // 20% monster Lv2
                    {
                        int idEvolve = gc.FindMonster(rIDMonster).IDMonsterEvolve;
                        arenaMonster.Add(idEvolve);
                    }
                }
                else                            // 5% monster id 40-60
                {
                    int rIDMonster = Random.Range(40, 60);
                    int rEvolve = Random.Range(0, 100);

                    if (rEvolve < 80) //  80% monster Lv1
                    {
                        arenaMonster.Add(rIDMonster);
                    }
                    else // 20% monster Lv2
                    {
                        int idEvolve = gc.FindMonster(rIDMonster).IDMonsterEvolve;
                        arenaMonster.Add(idEvolve);
                    }
                }
            }
            return;
        }

        if (saveRank > 500)
        {
            for (int i = 0; i < 5; i++)
            {
                int r = Random.Range(0, 100);
                if (r < 5)
                {
                    arenaMonster.Add(0);
                }                  // 5% no monster
                else if (r < 45 && r >= 5)      // 40% monster id 1-15
                {
                    int rIDMonster = Random.Range(0, 15);
                    int rEvolve = Random.Range(0, 100);

                    if (rEvolve < 55) //  55% monster Lv1
                    {
                        arenaMonster.Add(rIDMonster);
                    }
                    else // 45% monster Lv2
                    {
                        int idEvolve = gc.FindMonster(rIDMonster).IDMonsterEvolve;
                        arenaMonster.Add(idEvolve);
                    }
                }
                else if (r < 85 && r >= 45)     // 40% monster id 15-40
                {
                    int rIDMonster = Random.Range(15, 40);
                    int rEvolve = Random.Range(0, 100);

                    if (rEvolve < 55) //  55% monster Lv1
                    {
                        arenaMonster.Add(rIDMonster);
                    }
                    else // 45% monster Lv2
                    {
                        int idEvolve = gc.FindMonster(rIDMonster).IDMonsterEvolve;
                        arenaMonster.Add(idEvolve);
                    }
                }
                else if (r < 95 && r >= 85)     // 10% monster id 40-60
                {
                    int rIDMonster = Random.Range(40, 60);
                    int rEvolve = Random.Range(0, 100);

                    if (rEvolve < 55) //  55% monster Lv1
                    {
                        arenaMonster.Add(rIDMonster);
                    }
                    else // 45% monster Lv2
                    {
                        int idEvolve = gc.FindMonster(rIDMonster).IDMonsterEvolve;
                        arenaMonster.Add(idEvolve);
                    }
                }
                else                            // 5% monster id 60-80
                {
                    int rIDMonster = Random.Range(60, 80);
                    int rEvolve = Random.Range(0, 100);

                    if (rEvolve < 55) //  55% monster Lv1
                    {
                        arenaMonster.Add(rIDMonster);
                    }
                    else // 45% monster Lv2
                    {
                        int idEvolve = gc.FindMonster(rIDMonster).IDMonsterEvolve;
                        arenaMonster.Add(idEvolve);
                    }
                }
            }
            return;
        }

        if (saveRank > 100)
        {
            for (int i = 0; i < 5; i++)
            {
                int r = Random.Range(0, 100);
                if (r < 5 && r >= 0)            // 5% monster id 1-15
                {
                    int rIDMonster = Random.Range(0, 15);
                    int rEvolve = Random.Range(0, 100);

                    if (rEvolve < 30) //  30% monster Lv1
                    {
                        arenaMonster.Add(rIDMonster);
                    }
                    else // 70% monster Lv2
                    {
                        int idEvolve = gc.FindMonster(rIDMonster).IDMonsterEvolve;
                        arenaMonster.Add(idEvolve);
                    }
                }
                else if (r < 20 && r >= 5)      // 15% monster id 15-40
                {
                    int rIDMonster = Random.Range(15, 40);
                    int rEvolve = Random.Range(0, 100);

                    if (rEvolve < 30) //  30% monster Lv1
                    {
                        arenaMonster.Add(rIDMonster);
                    }
                    else // 70% monster Lv2
                    {
                        int idEvolve = gc.FindMonster(rIDMonster).IDMonsterEvolve;
                        arenaMonster.Add(idEvolve);
                    }
                }
                if (r < 60 && r >= 20)          // 40% monster id 40-60
                {
                    int rIDMonster = Random.Range(40, 60);
                    int rEvolve = Random.Range(0, 100);

                    if (rEvolve < 30) //  30% monster Lv1
                    {
                        arenaMonster.Add(rIDMonster);
                    }
                    else // 70% monster Lv2
                    {
                        int idEvolve = gc.FindMonster(rIDMonster).IDMonsterEvolve;
                        arenaMonster.Add(idEvolve);
                    }
                }
                else if (r < 85 && r >= 60)     // 25% monster id 60-80
                {
                    int rIDMonster = Random.Range(60, 80);
                    int rEvolve = Random.Range(0, 100);

                    if (rEvolve < 30) //  30% monster Lv1
                    {
                        arenaMonster.Add(rIDMonster);
                    }
                    else // 70% monster Lv2
                    {
                        int idEvolve = gc.FindMonster(rIDMonster).IDMonsterEvolve;
                        arenaMonster.Add(idEvolve);
                    }
                }
                else                            // 15% monster id 80-100
                {
                    int rIDMonster = Random.Range(80, 101);
                    int rEvolve = Random.Range(0, 100);

                    if (rEvolve < 30) //  30% monster Lv1
                    {
                        arenaMonster.Add(rIDMonster);
                    }
                    else // 70% monster Lv2
                    {
                        int idEvolve = gc.FindMonster(rIDMonster).IDMonsterEvolve;
                        arenaMonster.Add(idEvolve);
                    }
                }
            }
            return;
        }

        if (saveRank <= 100)
        {
            int eRank = 0;
            if (saveRank == 100 || saveRank == 99)
            {
                eRank = 100 - Random.Range(1, 3);
            }
            else if (saveRank == 1 || saveRank == 2)
            {
                eRank = 1 + Random.Range(1, 3);
            }
            else
            {
                eRank = saveRank + Random.Range(-2, 3);
            }

            arenaMonster.Add(GameController.ins.LoadDataRank(eRank).monster1Arena);
            arenaMonster.Add(GameController.ins.LoadDataRank(eRank).monster2Arena);
            arenaMonster.Add(GameController.ins.LoadDataRank(eRank).monster3Arena);
            arenaMonster.Add(GameController.ins.LoadDataRank(eRank).monster4Arena);
            arenaMonster.Add(GameController.ins.LoadDataRank(eRank).monster5Arena);


        }
    }
}
