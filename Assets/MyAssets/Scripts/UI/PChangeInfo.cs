using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class PChangeInfo : UIController
{
    public RectTransform imgBG;
    public InputField inputName;
    public Text txtPlaceholder;
    public Image mainAvatar;
    public RectTransform allChooseMonster;
    public RectTransform allChooseAvatar;
    public Button btnAllChooseMonster;
    public Button btnAllChooseAvatar;
    public GameObject arenaAvatarPrefabs;
    public List<ArenaAvatar> arenaAvatars;
    public Sprite[] spritesChooseMonster;
    public Sprite[] spritesChooseAvatar;
    public RectTransform avatarContent;

    public ArenaMonsterSlot[] allSlotMonster;

    public override void Show(bool value)
    {
        base.Show(value);
        if (value)
        {
            imgBG.anchoredPosition = new Vector2(0, 1250);

            LoadAvatar();

            mainAvatar.sprite = GameController.ins.FindMonster(PlayerPrefs.GetInt(StaticString.saveAvatar)).avatar[1];
            mainAvatar.SetNativeSize();

            allChooseMonster.anchoredPosition = new Vector2(0, 0);
            allChooseAvatar.anchoredPosition = new Vector2(700, 0);

            btnAllChooseMonster.image.sprite = spritesChooseMonster[1];
            btnAllChooseAvatar.image.sprite = spritesChooseAvatar[0];

            string[] varSaveMonsterKey = new string[5] { StaticString.saveArenaMonster0, StaticString.saveArenaMonster1, StaticString.saveArenaMonster2, StaticString.saveArenaMonster3, StaticString.saveArenaMonster4 };

            for (int i = 0; i < 5; i++)
            {
                int saveMonsterID = PlayerPrefs.GetInt(varSaveMonsterKey[i]);
                if (saveMonsterID > 0 && UIManager.ins.sArena.CheckMonsterAvailable(saveMonsterID))
                {
                    allSlotMonster[i].IDMonster = saveMonsterID;
                    allSlotMonster[i].imgAvatarMonster.sprite = GameController.ins.FindMonster(saveMonsterID).avatar[1];
                    allSlotMonster[i].imgAvatarMonster.SetNativeSize();
                    allSlotMonster[i].imgAvatarMonster.color = Color.white;
                    allSlotMonster[i].btnUnchoose.gameObject.SetActive(true);
                }
                else
                {
                    allSlotMonster[i].IDMonster = -1;
                    allSlotMonster[i].imgAvatarMonster.color = new Color(0, 0, 0, 0);
                    allSlotMonster[i].btnUnchoose.gameObject.SetActive(false);
                }
            }

            txtPlaceholder.text = PlayerPrefs.GetString(StaticString.saveArenaName);

            imgBG.DOAnchorPos(new Vector2(0, 0), 0.3f).OnComplete(() =>
            {
                imgBG.DOAnchorPos(new Vector2(0, 100), 0.15f);
            });
        }
        else
        {
            if (arenaAvatars.Count > 0)
            {
                foreach (var item in arenaAvatars)
                {
                    Destroy(item.gameObject);
                }
                arenaAvatars.Clear();
            }
        }
    }

    public void LoadAvatar()
    {
        List<int> allAvatar = GameController.ins.LoadMonsterCollection();

        if (allAvatar.Count <= 0)
        {
            allAvatar.Add(1);
        }

        int countItemInRow = 0;
        int newColumn = 0;
        for (int i = 0; i < allAvatar.Count; i++)
        {
            ArenaAvatar aa = Instantiate(arenaAvatarPrefabs, avatarContent).GetComponent<ArenaAvatar>();
            arenaAvatars.Add(aa);
            aa.OnSpawn(allAvatar[i]);

            RectTransform aaTransform = aa.GetComponent<RectTransform>();
            aaTransform.anchoredPosition = new Vector2(-125 + (125 * countItemInRow), -75 - (125 * newColumn));

            countItemInRow++;
            if (countItemInRow > 2)
            {
                countItemInRow = 0;
                newColumn++;
            }
        }

        avatarContent.sizeDelta = new Vector2(avatarContent.sizeDelta.x, 150 + (125 * (newColumn)));
        avatarContent.anchoredPosition = new Vector2(avatarContent.anchoredPosition.x, 0);
    }

    public void BtnChooseMonster()
    {
        AudioManager.ins.PlayOther("Click");

        allChooseMonster.DOAnchorPos(new Vector2(0, 0), 0.25f);
        allChooseAvatar.DOAnchorPos(new Vector2(700, 0), 0.25f);

        btnAllChooseMonster.image.sprite = spritesChooseMonster[1];
        btnAllChooseAvatar.image.sprite = spritesChooseAvatar[0];
    }

    public void BtnChooseAvatar()
    {
        AudioManager.ins.PlayOther("Click");

        allChooseMonster.DOAnchorPos(new Vector2(-700, 0), 0.25f);
        allChooseAvatar.DOAnchorPos(new Vector2(0, 0), 0.25f);

        btnAllChooseMonster.image.sprite = spritesChooseMonster[0];
        btnAllChooseAvatar.image.sprite = spritesChooseAvatar[1];
    }

    public void BtnSaveChange()
    {
        AudioManager.ins.PlayOther("Click");
        if (inputName.text != "")
        {
            PlayerPrefs.SetString(StaticString.saveArenaName, inputName.text);
        }

        string[] varSaveMonsterKey = new string[5] { StaticString.saveArenaMonster0, StaticString.saveArenaMonster1, StaticString.saveArenaMonster2, StaticString.saveArenaMonster3, StaticString.saveArenaMonster4 };
        for (int i = 0; i < allSlotMonster.Length; i++)
        {
            PlayerPrefs.SetInt(varSaveMonsterKey[i], allSlotMonster[i].IDMonster);
        }

        BtnQuit();
    }

    public void BtnQuit()
    {
        AudioManager.ins.PlayOther("Click");
        UIManager.ins.sArena.GetMainPlayerInfor();
        imgBG.DOAnchorPos(new Vector2(0, 0), 0.15f).OnComplete(() =>
        {
            imgBG.DOAnchorPos(new Vector2(0, 1250), 0.3f).OnComplete(() =>
            {
                UIManager.ins.monsterView.Show(false);
                UIManager.ins.ShowPopup(Popup.None);
            });
        });
    }
}
