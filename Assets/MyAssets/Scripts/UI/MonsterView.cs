
/* Copyright © @ThanhDV */

using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class MonsterView : UIController
{
    public RectTransform contentListPlayerMonster;
    public Button btnChoseMonsterPrefabs;
    public List<BtnChoseMonster> allBallButton;
    public UI_ScrollRectOcclusion uI_ScrollRectOcclusion;

    [HideInInspector] public List<BtnChoseMonster> tempAllBallButton;
    [HideInInspector] public List<BtnChoseMonster> allBtnMonster;
    [HideInInspector] public List<BtnChoseMonster> allBtnEmpty;
    [HideInInspector] public List<BtnChoseMonster> allBtnWatchVideo;
    [HideInInspector] public List<BtnChoseMonster> allBtnLock;

    public Sprite[] btnUnlockSlotSprite;

    public override void Show(bool value)
    {
        base.Show(value);
        if (value)
        {
            SortBtn();

            if (PlayerPrefs.GetInt(ManagerAds.Save_Remove_RewardAds) == 1)
            {
                allBallButton[0].thisBtn.image.sprite = btnUnlockSlotSprite[0];
            }
            else
            {
                allBallButton[0].thisBtn.image.sprite = btnUnlockSlotSprite[1];
            }

            if (UIManager.ins.currentPopup != Popup.PChangeInfo)
            {
                allBallButton[1].GetComponent<BtnChoseMonster>().ChooseMonster();
            }
            UpdateBtnGroupSize();

            if (GameController.ins.isPlaying)
            {
                GameController.ins.player.animator.SetTrigger("GetBall");
            }

            if (PlayerPrefs.GetInt(StaticString.saveWatchVideoMonsterBox) >= 6)
            {
                allBallButton[0].thisBtn.interactable = false;
            }
            uI_ScrollRectOcclusion.Init();
        }
    }

    public void SortBtn()
    {
        allBtnMonster = new List<BtnChoseMonster>();
        allBtnEmpty = new List<BtnChoseMonster>();
        allBtnWatchVideo = new List<BtnChoseMonster>();
        allBtnLock = new List<BtnChoseMonster>();
        tempAllBallButton = new List<BtnChoseMonster>();

        allBtnWatchVideo.Add(allBallButton[0]);
        for (int i = 1; i < allBallButton.Count; i++)
        {
            allBallButton[i].OnSpawn();
            if (allBallButton[i].IDMonster > 0)
            {
                allBtnMonster.Add(allBallButton[i]);
            }
            else if (allBallButton[i].IDMonster <= 0)
            {
                if (allBallButton[i].unlockAtLevel <= PlayerPrefs.GetInt(StaticString.saveLevel))
                {
                    allBtnEmpty.Add(allBallButton[i]);
                }
                else
                {
                    allBtnLock.Add(allBallButton[i]);
                }
            }
        }

        for (int i = 0; i < allBtnMonster.Count; i++)
        {
            allBtnMonster[i].GetComponent<RectTransform>().anchoredPosition = new Vector2(75 + (135 * tempAllBallButton.Count), 0);
            tempAllBallButton.Add(allBtnMonster[i]);
        }

        for (int i = 0; i < allBtnEmpty.Count; i++)
        {
            allBtnEmpty[i].GetComponent<RectTransform>().anchoredPosition = new Vector2(75 + (135 * tempAllBallButton.Count), 0);
            tempAllBallButton.Add(allBtnEmpty[i]);
        }

        for (int i = 0; i < allBtnWatchVideo.Count; i++)
        {
            allBtnWatchVideo[i].GetComponent<RectTransform>().anchoredPosition = new Vector2(75 + (135 * tempAllBallButton.Count), 0);
            tempAllBallButton.Add(allBtnWatchVideo[i]);
        }

        for (int i = 0; i < allBtnLock.Count; i++)
        {
            allBtnLock[i].GetComponent<RectTransform>().anchoredPosition = new Vector2(75 + (135 * tempAllBallButton.Count), 0);
            tempAllBallButton.Add(allBtnLock[i]);
        }
    }

    public void BtnWatchAds()
    {
        if (GameController.ins.listBallSpawned.Count > 0) return;

        AudioManager.ins.PlayOther("Click");
        ManagerAds.ins.ShowRewarded((s) =>
         {
             if (!s) return;

             int maxWatchSlot = PlayerPrefs.GetInt(StaticString.saveWatchVideoMonsterBox);
             if (maxWatchSlot >= 6) return;

             AddEmptyButton();
             PlayerPrefs.SetInt(StaticString.saveWatchVideoMonsterBox, maxWatchSlot + 1);
             if (PlayerPrefs.GetInt(StaticString.saveWatchVideoMonsterBox) >= 6)
             {
                 allBallButton[0].thisBtn.interactable = false;
             }
             GameController.ins.SaveMonsterView();
         }, "BtnUnlockSlot");
    }

    public void UpdateBtnGroupSize()
    {
        contentListPlayerMonster.sizeDelta = new Vector2((allBallButton.Count * 120) + ((allBallButton.Count + 1) * 15), contentListPlayerMonster.sizeDelta.y);
    }

    public void AddMonsterToBtn(string nameOfMonster, int _ID, Sprite avatar, BtnChoseMonster btn = null)
    {
        if (btn == null) { btn = GameController.ins.currentBtnChoseMonster; }

        Text btnText = btn.thisText;
        Image btnImg = btn.imgAvatar;

        btn.IDMonster = _ID;
        btnText.fontSize = 30;
        btnText.text = nameOfMonster;
        btnImg.sprite = avatar;
        btnImg.SetNativeSize();
        btn.isSpawnedObj = false;
    }

    public void AddEmptyButton()
    {
        RectTransform btn = Instantiate(btnChoseMonsterPrefabs.gameObject, contentListPlayerMonster).GetComponent<RectTransform>();
        btn.localScale = Vector3.zero;
        BtnChoseMonster btnController = btn.GetComponent<BtnChoseMonster>();
        allBallButton.Add(btnController);
        btnController.OnSpawn();

        btn.GetComponentInChildren<Text>().text = "Empty";

        UpdateBtnGroupSize();

        if (allBallButton.Count >= 5)
        {
            foreach (BtnChoseMonster b in allBtnLock)
            {
                RectTransform brt = b.GetComponent<RectTransform>();
                brt.DOAnchorPosX(brt.anchoredPosition.x + 135, 0.25f);
            }
            RectTransform btnWatchVideo = allBallButton[0].GetComponent<RectTransform>();
            btn.anchoredPosition = btnWatchVideo.anchoredPosition;
            btnWatchVideo.DOAnchorPos(new Vector2(btn.anchoredPosition.x + 135, btn.anchoredPosition.y), 0.25f).OnComplete(() =>
            {
                btn.DOScale(Vector3.one * 1.15f, 0.25f).OnComplete(() =>
                {
                    btn.DOScale(Vector3.one, 0.15f);
                });
            });
        }
        else
        {
            foreach (BtnChoseMonster b in allBtnLock)
            {
                RectTransform brt = b.GetComponent<RectTransform>();
                brt.DOAnchorPosX(brt.anchoredPosition.x + 135, 0.25f);
            }
            RectTransform btnWatchVideo = allBallButton[0].GetComponent<RectTransform>();
            btn.anchoredPosition = btnWatchVideo.anchoredPosition;
            btnWatchVideo.DOAnchorPos(new Vector2(btn.anchoredPosition.x + 135, btn.anchoredPosition.y), 0.25f).OnComplete(() =>
            {
                btn.DOScale(Vector3.one * 1.15f, 0.25f).OnComplete(() =>
                {
                    btn.DOScale(Vector3.one, 0.15f);
                });
            });
        }

        btnController.ChooseMonster();
    }

    public void AddEmptyButtonNotScroll()
    {
        RectTransform btn = Instantiate(btnChoseMonsterPrefabs.gameObject, contentListPlayerMonster).GetComponent<RectTransform>();
        BtnChoseMonster btnController = btn.GetComponent<BtnChoseMonster>();
        allBallButton.Add(btnController);
        btnController.OnSpawn();

        btn.GetComponentInChildren<Text>().text = "Empty";

        UpdateBtnGroupSize();

        RectTransform btnWatchVideo = allBallButton[0].GetComponent<RectTransform>();
        btn.anchoredPosition = btnWatchVideo.anchoredPosition;
        btnWatchVideo.anchoredPosition = new Vector2(btn.anchoredPosition.x + 135, btn.anchoredPosition.y);
    }

    public void RewardMonster(MonsterInfo info)
    {
        RectTransform btn = Instantiate(btnChoseMonsterPrefabs.gameObject, contentListPlayerMonster).GetComponent<RectTransform>();
        BtnChoseMonster btnController = btn.GetComponent<BtnChoseMonster>();
        allBallButton.Add(btnController);

        UpdateBtnGroupSize();

        RectTransform btnWatchVideo = allBallButton[0].GetComponent<RectTransform>();
        btn.anchoredPosition = btnWatchVideo.anchoredPosition;
        btnWatchVideo.anchoredPosition = new Vector2(btn.anchoredPosition.x + 135, btn.anchoredPosition.y);

        UIManager.ins.monsterView.AddMonsterToBtn(info.monsterName, info.ID, info.avatar[0], btnController);
        GameController.ins.SaveMonsterCollection(info.ID);
        GameController.ins.SaveMonsterView();

        btnController.OnSpawn();
    }
}
