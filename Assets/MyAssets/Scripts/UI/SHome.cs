
/* Copyright © @ThanhDV */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using SendEventAppMetrica;
using System;

public class SHome : UIController
{
    public RectTransform groupLeftBtn;
    public RectTransform groupRightBtn;
    public RectTransform groupShowLevel;
    public RectTransform btnPlay;
    public Text txtCurrentLevel;

    [Header("LevelSprite")]
    public Sprite greenLvSprite;
    public Sprite grayLvSprite;

    [Header("ImgLevel")]
    public Image[] imgLevel;

    public GameObject lastImgLevel;
    public MonsterInfo monsterBuff;
    public Image[] imgWorldWatchVideo;

    public GameObject btnShopFX;
    public GameObject btnCollectionFX;
    public GameObject btnDailyFX;
    public GameObject btnDaily;

    public override void Show(bool value)
    {
        base.Show(value);
        if (value)
        {
            GameController.ins.ResetAnimTrigger();
            GameController.ins.player.animator.Play(GameController.ins.player.clipIdle1.name);
            if (PlayerPrefs.GetInt(StaticString.saveDailyClaimed) >= 5)
            {
                btnDaily.SetActive(false);
            }
            else
            {
                btnDaily.SetActive(true);
            }

            if (CanClaimDailyReward())
            {
                btnDailyFX.SetActive(true);
            }
            else
            {
                btnDailyFX.SetActive(false);
            }

            if (UIManager.ins.sShop.IAPFunction.CheckNewIAPMonster())
            {
                btnShopFX.SetActive(true);
            }
            else
            {
                btnShopFX.SetActive(false);
            }

            if (UIManager.ins.sCollection.CheckReward())
            {
                btnCollectionFX.SetActive(true);
            }
            else
            {
                btnCollectionFX.SetActive(false);
            }

            AnimUIOnShow();
        }
    }

    public bool CanClaimDailyReward()
    {
        DateTime currentDate = DateTime.Now;

        long temp = Convert.ToInt64(PlayerPrefs.GetString(StaticString.saveTimeLastDailyClaim));

        DateTime oldDate = DateTime.FromBinary(temp);

        if (currentDate.Day != oldDate.Day)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void MaxDebug()
    {
        // ManagerAds.Ins.ShowBanner();
        MaxSdk.ShowMediationDebugger();
    }

    private void AnimUIOnShow()
    {
        if (PlayerPrefs.GetInt(ManagerAds.Save_Remove_RewardAds) == 1 || PlayerPrefs.GetInt(StaticString.saveIAPWorldTicket) == 1)
        {
            foreach (Image i in imgWorldWatchVideo)
            {
                i.enabled = false;
            }
        }
        else
        {
            foreach (Image i in imgWorldWatchVideo)
            {
                i.enabled = true;
            }
        }

        groupLeftBtn.anchoredPosition = new Vector2(-500, -640);
        groupRightBtn.anchoredPosition = new Vector2(500, -640);
        if (ShowLevel())
        {
            groupShowLevel.anchoredPosition = new Vector2(30, 250);
        }
        else
        {
            groupShowLevel.anchoredPosition = new Vector2(75, 250);
        }
        btnPlay.anchoredPosition = new Vector2(0, -385);

        groupLeftBtn.DOAnchorPos(new Vector2(360, -640), 0.5f);
        groupRightBtn.DOAnchorPos(new Vector2(-360, -640), 0.5f);
        groupShowLevel.DOAnchorPosY(-150, 0.5f);
        btnPlay.DOAnchorPos(new Vector2(0, 435), 0.45f).OnComplete(() =>
        {
            btnPlay.DOAnchorPos(new Vector2(0, 385), 0.15f);
        });
    }

    public bool ShowLevel(bool useSubCam = true)
    {
        GameController.ins.ChangeTime();

        int[] r = new int[4];
        r[0] = GameController.ins.ReadInt(GameController.ins.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14);
        r[1] = GameController.ins.ReadInt(GameController.ins.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel) + 1, 14);
        r[2] = GameController.ins.ReadInt(GameController.ins.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel) + 2, 14);
        r[3] = GameController.ins.ReadInt(GameController.ins.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel) + 3, 14);

        string txtLv = PlayerPrefs.GetInt(StaticString.saveLevel).ToString();
        if (PlayerPrefs.GetInt(StaticString.checkFirstTimePlay) == 1 && GameController.ins.LoadMonsterCollection().Count <= 0)
        {
            txtLv = "0";
        }

        if (r[0] == 0)
        {
            txtCurrentLevel.text = txtLv;
        }
        else if (r[0] == 1)
        {
            txtCurrentLevel.text = txtLv;
            if (useSubCam)
            {
                GameController.ins.UseSubCam();
            }
        }
        else if (r[0] == 2)
        {
            txtCurrentLevel.text = txtLv;
            if (useSubCam)
            {
                GameController.ins.UseSubCam();
            }
        }

        if (r[0] == 0 && r[1] == 0 && r[2] == 1 && r[3] == 0)
        {
            imgLevel[0].sprite = greenLvSprite;
            imgLevel[1].sprite = grayLvSprite;
            imgLevel[2].sprite = grayLvSprite;
            imgLevel[3].sprite = grayLvSprite;
        }
        else if (r[0] == 0 && r[1] == 1 && r[2] == 0 && r[3] == 0)
        {
            imgLevel[0].sprite = greenLvSprite;
            imgLevel[1].sprite = greenLvSprite;
            imgLevel[2].sprite = grayLvSprite;
            imgLevel[3].sprite = grayLvSprite;
        }
        else if (r[0] == 1 && r[1] == 0 && r[2] == 0 && r[3] == 1)
        {
            imgLevel[0].sprite = greenLvSprite;
            imgLevel[1].sprite = greenLvSprite;
            imgLevel[2].sprite = greenLvSprite;
            imgLevel[3].sprite = grayLvSprite;
        }
        else if (r[0] == 1 && r[1] == 0 && r[2] == 0 && r[3] == 1)
        {
            imgLevel[0].sprite = greenLvSprite;
            imgLevel[1].sprite = greenLvSprite;
            imgLevel[2].sprite = grayLvSprite;
            imgLevel[3].sprite = grayLvSprite;
        }
        else if (r[0] == 0 && r[1] == 0 && r[2] == 1 && r[3] == 2)
        {
            imgLevel[0].sprite = greenLvSprite;
            imgLevel[1].sprite = grayLvSprite;
            imgLevel[2].sprite = grayLvSprite;
            imgLevel[3].sprite = grayLvSprite;
        }
        else if (r[0] == 0 && r[1] == 1 && r[2] == 2 && r[3] == 0)
        {
            imgLevel[0].sprite = greenLvSprite;
            imgLevel[1].sprite = greenLvSprite;
            imgLevel[2].sprite = grayLvSprite;
            imgLevel[3].sprite = grayLvSprite;
        }
        else if (r[0] == 1 && r[1] == 2 && r[2] == 0 && r[3] == 0)
        {
            imgLevel[0].sprite = greenLvSprite;
            imgLevel[1].sprite = greenLvSprite;
            imgLevel[2].sprite = greenLvSprite;
            imgLevel[3].sprite = grayLvSprite;
        }
        else if (r[0] == 2 && r[1] == 0 && r[2] == 0 && r[3] == 1)
        {
            imgLevel[0].sprite = greenLvSprite;
            imgLevel[1].sprite = greenLvSprite;
            imgLevel[2].sprite = greenLvSprite;
            imgLevel[3].sprite = greenLvSprite;
        }

        foreach (int x in r)
        {
            if (x == 2)
            {
                lastImgLevel.SetActive(true);
                return true;
            }
            else
            {
                lastImgLevel.SetActive(false);
            }
        }
        return false;
    }

    public void BtnGetAllMonster()
    {
        for (int i = 0; i < GameController.ins.allMonster.Length; i++)
        {
            UIManager.ins.monsterView.RewardMonster(GameController.ins.allMonster[i]);
        }
    }

    public void BtnArena()
    {
        AudioManager.ins.PlayOther("Click");
        UIManager.ins.ShowScreen(ScreenX.SArena);
    }

    public void BtnPlay()
    {
        UIManager.ins.ShowPopup(Popup.None);
        int mapType = GameController.ins.ReadInt(GameController.ins.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14);
        string levelType = "";
        switch (mapType)
        {
            case 0:
                levelType = "Normal";
                break;
            case 1:
                levelType = "Boss";
                break;
            case 2:
                levelType = "SuperBoss";
                break;
        }
        AudioManager.ins.PlayOther("Click");
        btnPlay.DOAnchorPos(new Vector2(0, 435), 0.15f).OnComplete(() =>
        {
            btnPlay.DOAnchorPos(new Vector2(0, -385), 0.45f);
        });
        groupLeftBtn.DOAnchorPos(new Vector2(-500, -640), 0.5f);
        groupRightBtn.DOAnchorPos(new Vector2(500, -640), 0.5f);
        groupShowLevel.DOAnchorPosY(250, 0.5f).OnComplete(() =>
        {
            if ((PlayerPrefs.GetInt(StaticString.checkFirstTimePlay) == 1 && GameController.ins.LoadMonsterCollection().Count <= 0))
            {
                UIManager.ins.ShowPopup(Popup.PTutorial1);
            }
            else
            {
                UIManager.ins.sIngame.LoadBuff();
            }

            if (GameController.ins.ReadInt(GameController.ins.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14) == 0 || (PlayerPrefs.GetInt(StaticString.checkFirstTimePlay) == 1 && GameController.ins.LoadMonsterCollection().Count <= 0))
            {
                GameController.ins.isPlaying = true;
            }
            UIManager.ins.ShowScreen(ScreenX.SIngame);
        });

        GameController.ins.timeStartLevel = Time.time;
        GameController.ins.gameMode = "Normal";
        GameController.ins.levelType = levelType;
        GameController.ins.levelNumber = PlayerPrefs.GetInt(StaticString.saveLevel);

        if (PlayerPrefs.GetInt(StaticString.savePreLevel) != PlayerPrefs.GetInt(StaticString.saveLevel))
        {
            PlayerPrefs.SetInt(StaticString.saveLevelLoop, 1);
            PlayerPrefs.SetInt(StaticString.savePreLevel, PlayerPrefs.GetInt(StaticString.saveLevel));
        }
        else
        {
            PlayerPrefs.SetInt(StaticString.saveLevelLoop, PlayerPrefs.GetInt(StaticString.saveLevelLoop) + 1);
        }

        LevelStart.CustomParameters(new Dictionary<string, object>
        {
            {LevelStartParameters.game_mode , GameController.ins.gameMode},
            {LevelStartParameters.level_type , GameController.ins.levelType},
            {LevelStartParameters.level_loop , PlayerPrefs.GetInt(StaticString.saveLevelLoop)},
            {LevelStartParameters.level_number , GameController.ins.levelNumber},
        });
        SendEventManager.ins.SendEventBuffer(LevelStart.GetEventAppmetrica());
    }

    public void IntroSpecialMap()
    {
        GameController gc = GameController.ins;
        PlayerController player = gc.player;
        UIManager.ins.sIngame.touchField.enabled = false;
        UIManager.ins.sIngame.btnBack.gameObject.SetActive(false);
        GameController.ins.isMoving = true;

        transform.DOScale(transform.localScale, 0.1f).OnComplete(() =>
        {
            for (int i = 1; i < UIManager.ins.monsterView.allBallButton.Count; i++)
            {
                UIManager.ins.monsterView.allBallButton[i].btnFreeMonster.gameObject.SetActive(false);
                UIManager.ins.monsterView.allBallButton[i].btnEvolve.gameObject.SetActive(false);
            }
            player.transform.DOMove(GameController.ins.currentMap.camPos[0].position, 2.5f).OnComplete(() =>
            {
                player.transform.DOMove(GameController.ins.currentMap.camPos[0].position, 1f).OnComplete(() =>
                {
                    int checkMap = GameController.ins.ReadInt(GameController.ins.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14);
                    if (checkMap == 2)
                    {
                        AudioManager.ins.PlayOther("Boss2In" + UnityEngine.Random.Range(0, 2));
                    }
                    else
                    {
                        AudioManager.ins.PlayOther("Boss1In" + UnityEngine.Random.Range(0, 2));
                    }
                    gc.currentBoss.animator.Play(gc.currentBoss.clipAction.name);
                    player.transform.DOMove(GameController.ins.currentMap.camPos[1].position, 1f).OnComplete(() =>
                    {
                        player.transform.DOMove(GameController.ins.currentMap.camPos[1].position, 1f).OnComplete(() =>
                        {
                            gc.currentBoss.animator.Play(gc.currentBoss.clipThrow.name);
                            player.transform.DOMove(gc.currentMap.playerPos[0].position, 3.5f).OnComplete(() =>
                            {
                                gc.UseMainCam();
                                gc.isPlaying = true;
                                gc.isMoving = false;
                                gc.currentBoss.bossRound++;
                                UIManager.ins.sIngame.btnSkip.gameObject.SetActive(true);
                                UIManager.ins.sIngame.touchField.enabled = true;
                                UIManager.ins.sIngame.btnBack.gameObject.SetActive(true);

                                AudioManager.ins.PlayOther("Bell");

                                foreach (BtnChoseMonster btn in UIManager.ins.monsterView.allBallButton)
                                {
                                    btn.thisBtn.interactable = true;
                                }

                                if (gc.ReadInt(gc.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14) == 2)
                                {
                                    gc.player.animator.SetTrigger("AutoThrow");
                                    return;
                                }

                                if (PlayerPrefs.GetInt(StaticString.saveBuff) == 4)
                                {
                                    Vector3 pos = GameController.ins.currentMap.monsterBuffPos.position;
                                    PoolVFX.pool.Spawn(GameController.ins.player.freeMonsterFX, pos, Quaternion.identity);
                                    UIManager.ins.sIngame.buffMonster = GameController.ins.SpawnMonsterBuff(monsterBuff, pos, Quaternion.identity);
                                }

                                UIManager.ins.monsterView.allBallButton[1].ChooseMonster();
                            });
                        });
                    });
                });
            });
        });
    }

    public void BtnDaily()
    {
        AudioManager.ins.PlayOther("Click");
        // ManagerAds.ins.ShowInterstitial(null, "BtnDaily");
        UIManager.ins.ShowPopup(Popup.PDaily);
    }

    public void BtnSettings()
    {
        AudioManager.ins.PlayOther("Click");
        UIManager.ins.ShowPopup(Popup.PSettings);
    }

    public void BtnShop()
    {
        AudioManager.ins.PlayOther("Click");
        // ManagerAds.ins.ShowInterstitial(null, "BtnShop");
        UIManager.ins.ShowScreen(ScreenX.SShop);
    }

    public void BtnCollection()
    {
        AudioManager.ins.PlayOther("Click");
        // ManagerAds.ins.ShowInterstitial(null, "BtnCollection");
        UIManager.ins.ShowScreen(ScreenX.SCollection);
    }

    public void BtnGrassMap()
    {
        AudioManager.ins.PlayOther("Click");
        if (PlayerPrefs.GetInt(StaticString.saveIAPWorldTicket) == 1)
        {
            btnPlay.DOAnchorPos(new Vector2(0, 435), 0.15f).OnComplete(() =>
            {
                btnPlay.DOAnchorPos(new Vector2(0, -385), 0.45f);
            });
            groupLeftBtn.DOAnchorPos(new Vector2(-500, -640), 0.5f);
            groupRightBtn.DOAnchorPos(new Vector2(500, -640), 0.5f);
            groupShowLevel.DOAnchorPosY(250, 0.5f).OnComplete(() =>
            {
                GameController.ins.LoadSpecialMap(World.Grass);
                foreach (BtnChoseMonster btn in UIManager.ins.monsterView.allBallButton)
                {
                    btn.thisBtn.interactable = true;
                }
                int currentLv = GameController.ins.ReadInt(GameController.ins.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14);

                GameController.ins.UseMainCam();
                GameController.ins.isPlaying = true;
                UIManager.ins.ShowScreen(ScreenX.SIngame);
            });
            return;
        }
        ManagerAds.ins.ShowRewarded((s) =>
         {
             if (!s) return;

             btnPlay.DOAnchorPos(new Vector2(0, 435), 0.15f).OnComplete(() =>
             {
                 btnPlay.DOAnchorPos(new Vector2(0, -385), 0.45f);
             });
             groupLeftBtn.DOAnchorPos(new Vector2(-500, -640), 0.5f);
             groupRightBtn.DOAnchorPos(new Vector2(500, -640), 0.5f);
             groupShowLevel.DOAnchorPosY(250, 0.5f).OnComplete(() =>
             {
                 GameController.ins.LoadSpecialMap(World.Grass);
                 foreach (BtnChoseMonster btn in UIManager.ins.monsterView.allBallButton)
                 {
                     btn.thisBtn.interactable = true;
                 }
                 int currentLv = GameController.ins.ReadInt(GameController.ins.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14);

                 GameController.ins.UseMainCam();
                 GameController.ins.isPlaying = true;
                 UIManager.ins.ShowScreen(ScreenX.SIngame);
             });
         }, "EnterGrassMap");
    }

    public void BtnFireMap()
    {
        AudioManager.ins.PlayOther("Click");
        if (PlayerPrefs.GetInt(StaticString.saveIAPWorldTicket) == 1)
        {
            btnPlay.DOAnchorPos(new Vector2(0, 435), 0.15f).OnComplete(() =>
            {
                btnPlay.DOAnchorPos(new Vector2(0, -385), 0.45f);
            });
            groupLeftBtn.DOAnchorPos(new Vector2(-500, -640), 0.5f);
            groupRightBtn.DOAnchorPos(new Vector2(500, -640), 0.5f);
            groupShowLevel.DOAnchorPosY(250, 0.5f).OnComplete(() =>
            {
                GameController.ins.LoadSpecialMap(World.Fire);
                foreach (BtnChoseMonster btn in UIManager.ins.monsterView.allBallButton)
                {
                    btn.thisBtn.interactable = true;
                }
                int currentLv = GameController.ins.ReadInt(GameController.ins.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14);

                GameController.ins.UseMainCam();

                GameController.ins.isPlaying = true;
                UIManager.ins.ShowScreen(ScreenX.SIngame);
            });
            return;
        }

        ManagerAds.ins.ShowRewarded((s) =>
         {
             if (!s) return;

             btnPlay.DOAnchorPos(new Vector2(0, 435), 0.15f).OnComplete(() =>
             {
                 btnPlay.DOAnchorPos(new Vector2(0, -385), 0.45f);
             });
             groupLeftBtn.DOAnchorPos(new Vector2(-500, -640), 0.5f);
             groupRightBtn.DOAnchorPos(new Vector2(500, -640), 0.5f);
             groupShowLevel.DOAnchorPosY(250, 0.5f).OnComplete(() =>
             {
                 GameController.ins.LoadSpecialMap(World.Fire);
                 foreach (BtnChoseMonster btn in UIManager.ins.monsterView.allBallButton)
                 {
                     btn.thisBtn.interactable = true;
                 }
                 int currentLv = GameController.ins.ReadInt(GameController.ins.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14);

                 GameController.ins.UseMainCam();

                 GameController.ins.isPlaying = true;
                 UIManager.ins.ShowScreen(ScreenX.SIngame);
             });
         }, "EnterFireMap");
    }

    public void BtnWaterMap()
    {
        AudioManager.ins.PlayOther("Click");
        if (PlayerPrefs.GetInt(StaticString.saveIAPWorldTicket) == 1)
        {
            btnPlay.DOAnchorPos(new Vector2(0, 435), 0.15f).OnComplete(() =>
            {
                btnPlay.DOAnchorPos(new Vector2(0, -385), 0.45f);
            });
            groupLeftBtn.DOAnchorPos(new Vector2(-500, -640), 0.5f);
            groupRightBtn.DOAnchorPos(new Vector2(500, -640), 0.5f);
            groupShowLevel.DOAnchorPosY(250, 0.5f).OnComplete(() =>
            {
                GameController.ins.LoadSpecialMap(World.Water);
                foreach (BtnChoseMonster btn in UIManager.ins.monsterView.allBallButton)
                {
                    btn.thisBtn.interactable = true;
                }
                int currentLv = GameController.ins.ReadInt(GameController.ins.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14);

                GameController.ins.UseMainCam();

                GameController.ins.isPlaying = true;
                UIManager.ins.ShowScreen(ScreenX.SIngame);
            });
            return;
        }
        ManagerAds.ins.ShowRewarded((s) =>
         {
             if (!s) return;

             btnPlay.DOAnchorPos(new Vector2(0, 435), 0.15f).OnComplete(() =>
             {
                 btnPlay.DOAnchorPos(new Vector2(0, -385), 0.45f);
             });
             groupLeftBtn.DOAnchorPos(new Vector2(-500, -640), 0.5f);
             groupRightBtn.DOAnchorPos(new Vector2(500, -640), 0.5f);
             groupShowLevel.DOAnchorPosY(250, 0.5f).OnComplete(() =>
             {
                 GameController.ins.LoadSpecialMap(World.Water);
                 foreach (BtnChoseMonster btn in UIManager.ins.monsterView.allBallButton)
                 {
                     btn.thisBtn.interactable = true;
                 }
                 int currentLv = GameController.ins.ReadInt(GameController.ins.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14);

                 GameController.ins.UseMainCam();

                 GameController.ins.isPlaying = true;
                 UIManager.ins.ShowScreen(ScreenX.SIngame);
             });
         }, "EnterWaterMap");
    }

    public void BtnSpin()
    {
        AudioManager.ins.PlayOther("Click");
        btnPlay.DOAnchorPos(new Vector2(0, 435), 0.15f).OnComplete(() =>
        {
            btnPlay.DOAnchorPos(new Vector2(0, -385), 0.45f);
        });
        groupLeftBtn.DOAnchorPos(new Vector2(-500, -640), 0.5f);
        groupRightBtn.DOAnchorPos(new Vector2(500, -640), 0.5f);
        groupShowLevel.DOAnchorPosY(250, 0.5f).OnComplete(() =>
        {
            GameController.ins.LoadSpin();
            UIManager.ins.ShowScreen(ScreenX.SSpin);
        });
    }

    public void BtnGloves()
    {
        AudioManager.ins.PlayOther("Click");
        // ManagerAds.ins.ShowInterstitial(null, "BtnGloves");
        UIManager.ins.ShowScreen(ScreenX.SGloves);
    }
}
