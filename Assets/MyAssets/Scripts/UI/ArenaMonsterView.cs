using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArenaMonsterView : UIController
{
    public Image[] imgAvatarMonster;

    public override void Show(bool value)
    {
        base.Show(value);

        string[] varSaveMonsterKey = new string[5] { StaticString.saveArenaMonster0, StaticString.saveArenaMonster1, StaticString.saveArenaMonster2, StaticString.saveArenaMonster3, StaticString.saveArenaMonster4 };

        for (int i = 0; i < 5; i++)
        {
            int saveMonsterID = PlayerPrefs.GetInt(varSaveMonsterKey[i]);
            imgAvatarMonster[i].transform.parent.GetComponent<Image>().color = Color.white;
            if (saveMonsterID > 0)
            {
                imgAvatarMonster[i].sprite = GameController.ins.FindMonster(saveMonsterID).avatar[1];
                imgAvatarMonster[i].SetNativeSize();
                imgAvatarMonster[i].color = Color.white;
                imgAvatarMonster[i].name = saveMonsterID.ToString();
            }
            else
            {
                imgAvatarMonster[i].color = new Color(0, 0, 0, 0);
                imgAvatarMonster[i].name = saveMonsterID.ToString();
            }
        }

    }
}
