using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System.Linq;

public class SCollection : UIController
{
    public RectTransform imgBGCollection;
    public RectTransform allMonsterView;
    public RectTransform contentOfListGrassMonster;
    public RectTransform contentOfListFireMonster;
    public RectTransform contentOfListWaterMonster;
    public Slider slideRewardGrass;
    public Slider slideRewardFire;
    public Slider slideRewardWater;
    public Text txtRewardGrass;
    public Text txtRewardFire;
    public Text txtRewardWater;
    public MonsterInCollection imgMonsterInCollectionPrefabs;
    public Button btnGrass;
    public Sprite[] spritesBtnGrass;
    public Button btnFire;
    public Sprite[] spritesBtnFire;
    public Button btnWater;
    public Sprite[] spritesBtnWater;
    public GameObject collectionRewardPrefabs;
    public List<CollectionReward> allCollectionReward;
    public GameObject txtMonsterPrefabs;

    public override void Show(bool value)
    {
        base.Show(value);
        if (value)
        {
            if (allCollectionReward == null)
            {
                allCollectionReward = new List<CollectionReward>();
            }

            contentOfListGrassMonster.DOAnchorPosX(0, 0);
            contentOfListFireMonster.DOAnchorPosX(0, 0);
            contentOfListWaterMonster.DOAnchorPosX(0, 0);

            imgBGCollection.anchoredPosition = new Vector2(0, -2000);

            MonsterInfo[] allMonster = GameController.ins.allMonster;
            List<int> caughtMonster = GameController.ins.LoadMonsterCollection();

            int countG1 = 0; int countF1 = 0; int countW1 = 0;
            int countG2 = 0; int countF2 = 0; int countW2 = 0;
            int countRG = 0; int countRF = 0; int countRW = 0;
            int countGSTT = 0; int countFSTT = 0; int countWSTT = 0;

            for (int i = 0; i < allMonster.Length; i++)
            {
                if (allMonster[i].world.Contains(0))
                {
                    if (allMonster[i].IDMonsterEvolve > 0)
                    {
                        MonsterInCollection gm = Instantiate(imgMonsterInCollectionPrefabs.gameObject, contentOfListGrassMonster).GetComponent<MonsterInCollection>();
                        gm.IDMonster = allMonster[i].ID;
                        AutoAnchorPosBottomLeft(gm.thisRectTransform, new Vector2(45 + (256 * countG1), 250));
                        gm.thisImg.sprite = allMonster[i].avatar[1];
                        if (!caughtMonster.Contains(allMonster[i].ID))
                        {
                            gm.thisImg.color = Color.black;
                        }
                        else
                        {
                            gm.thisImg.color = Color.white;
                            countRG++;
                        }
                        gm.thisImg.SetNativeSize();
                        countG1++;
                    }
                    else
                    {
                        MonsterInCollection gm = Instantiate(imgMonsterInCollectionPrefabs.gameObject, contentOfListGrassMonster).GetComponent<MonsterInCollection>();
                        gm.IDMonster = allMonster[i].ID;
                        AutoAnchorPosBottomLeft(gm.thisRectTransform, new Vector2(45 + (256 * countG2), 0));
                        gm.thisImg.sprite = allMonster[i].avatar[1];
                        if (!caughtMonster.Contains(allMonster[i].ID))
                        {
                            gm.thisImg.color = Color.black;
                        }
                        else
                        {
                            gm.thisImg.color = Color.white;
                            countRG++;
                        }
                        gm.thisImg.SetNativeSize();
                        countG2++;
                    }
                    countGSTT++;
                    Text t = Instantiate(txtMonsterPrefabs, contentOfListGrassMonster).GetComponent<Text>();
                    AutoAnchorPosBottomLeft(t.GetComponent<RectTransform>(), new Vector2(45 + (256 * (countGSTT - 1)), 450));
                    t.text = "#" + countGSTT.ToString("000");
                }

                if (allMonster[i].world.Contains(1))
                {
                    if (allMonster[i].IDMonsterEvolve > 0)
                    {
                        MonsterInCollection gm = Instantiate(imgMonsterInCollectionPrefabs.gameObject, contentOfListFireMonster).GetComponent<MonsterInCollection>();
                        gm.IDMonster = allMonster[i].ID;
                        AutoAnchorPosBottomLeft(gm.thisRectTransform, new Vector2(45 + (256 * countF1), 250));
                        gm.thisImg.sprite = allMonster[i].avatar[1];
                        if (!caughtMonster.Contains(allMonster[i].ID))
                        {
                            gm.thisImg.color = Color.black;
                        }
                        else
                        {
                            gm.thisImg.color = Color.white;
                            countRF++;
                        }
                        gm.thisImg.SetNativeSize();
                        countF1++;
                    }
                    else
                    {
                        MonsterInCollection gm = Instantiate(imgMonsterInCollectionPrefabs.gameObject, contentOfListFireMonster).GetComponent<MonsterInCollection>();
                        gm.IDMonster = allMonster[i].ID;
                        AutoAnchorPosBottomLeft(gm.thisRectTransform, new Vector2(45 + (256 * countF2), 0));
                        gm.thisImg.sprite = allMonster[i].avatar[1];
                        if (!caughtMonster.Contains(allMonster[i].ID))
                        {
                            gm.thisImg.color = Color.black;
                        }
                        else
                        {
                            gm.thisImg.color = Color.white;
                            countRF++;
                        }
                        gm.thisImg.SetNativeSize();
                        countF2++;
                    }
                    countFSTT++;
                    Text t = Instantiate(txtMonsterPrefabs, contentOfListFireMonster).GetComponent<Text>();
                    AutoAnchorPosBottomLeft(t.GetComponent<RectTransform>(), new Vector2(45 + (256 * (countFSTT - 1)), 450));
                    t.text = "#" + countFSTT.ToString("000");
                }

                if (allMonster[i].world.Contains(2))
                {
                    if (allMonster[i].IDMonsterEvolve > 0)
                    {
                        MonsterInCollection gm = Instantiate(imgMonsterInCollectionPrefabs.gameObject, contentOfListWaterMonster).GetComponent<MonsterInCollection>();
                        gm.IDMonster = allMonster[i].ID;
                        AutoAnchorPosBottomLeft(gm.thisRectTransform, new Vector2(45 + (256 * countW1), 250));
                        gm.thisImg.sprite = allMonster[i].avatar[1];
                        if (!caughtMonster.Contains(allMonster[i].ID))
                        {
                            gm.thisImg.color = Color.black;
                        }
                        else
                        {
                            gm.thisImg.color = Color.white;
                            countRW++;
                        }
                        gm.thisImg.SetNativeSize();
                        countW1++;
                    }
                    else
                    {
                        MonsterInCollection gm = Instantiate(imgMonsterInCollectionPrefabs.gameObject, contentOfListWaterMonster).GetComponent<MonsterInCollection>();
                        gm.IDMonster = allMonster[i].ID;
                        AutoAnchorPosBottomLeft(gm.thisRectTransform, new Vector2(45 + (256 * countW2), 0));
                        gm.thisImg.sprite = allMonster[i].avatar[1];
                        if (!caughtMonster.Contains(allMonster[i].ID))
                        {
                            gm.thisImg.color = Color.black;
                        }
                        else
                        {
                            gm.thisImg.color = Color.white;
                            countRW++;
                        }
                        gm.thisImg.SetNativeSize();
                        countW2++;
                    }
                    countWSTT++;
                    Text t = Instantiate(txtMonsterPrefabs, contentOfListWaterMonster).GetComponent<Text>();
                    AutoAnchorPosBottomLeft(t.GetComponent<RectTransform>(), new Vector2(45 + (256 * (countWSTT - 1)), 450));
                    t.text = "#" + countWSTT.ToString("000");
                }
            }

            BtnGrass();

            slideRewardGrass.maxValue = contentOfListGrassMonster.childCount - countGSTT;
            slideRewardFire.maxValue = contentOfListFireMonster.childCount - countFSTT;
            slideRewardWater.maxValue = contentOfListWaterMonster.childCount - countWSTT;

            txtRewardGrass.text = countRG.ToString();
            txtRewardFire.text = countRF.ToString();
            txtRewardWater.text = countRW.ToString();

            if (allCollectionReward.Count <= 0)
            {
                SpawnReward(countRG, countRF, countRW);
            }
            else
            {
                foreach (CollectionReward cw in allCollectionReward)
                {
                    Destroy(cw.gameObject);
                }
                allCollectionReward.Clear();

                SpawnReward(countRG, countRF, countRW);
            }

            contentOfListGrassMonster.sizeDelta = new Vector2((contentOfListGrassMonster.childCount - countGSTT) * 128 + 32, 485);
            contentOfListFireMonster.sizeDelta = new Vector2((contentOfListFireMonster.childCount - countFSTT) * 128 + 32, 485);
            contentOfListWaterMonster.sizeDelta = new Vector2((contentOfListWaterMonster.childCount - countWSTT) * 128 + 32, 485);

            imgBGCollection.DOAnchorPos(new Vector2(0, 100), 0.4f).OnComplete(() =>
            {
                imgBGCollection.DOAnchorPos(new Vector2(0, 0), 0.15f);
            });
        }
    }

    private void SpawnReward(int _countRG, int _countRF, int _countRW)
    {
        int ID = 0;
        bool[] rewardStatus = GameController.ins.LoadCollectionReward();
        // Grass 
        RectTransform parentOfRewardG = slideRewardGrass.transform.GetChild(2).GetComponent<RectTransform>();
        RectTransform handleG = txtRewardGrass.transform.parent.GetComponent<RectTransform>();

        slideRewardGrass.value = 15;
        CollectionReward cr15g = Instantiate(collectionRewardPrefabs, parentOfRewardG).GetComponent<CollectionReward>();
        cr15g.OnSpawn(ClaimMode.claimSlot, handleG.anchoredPosition, handleG.localPosition, 15, _countRG, rewardStatus[ID], ID++);
        allCollectionReward.Add(cr15g);

        slideRewardGrass.value = 30;
        CollectionReward cr30g = Instantiate(collectionRewardPrefabs, parentOfRewardG).GetComponent<CollectionReward>();
        cr30g.OnSpawn(ClaimMode.claimSlot, handleG.anchoredPosition, handleG.localPosition, 30, _countRG, rewardStatus[ID], ID++);
        allCollectionReward.Add(cr30g);

        slideRewardGrass.value = 45;
        CollectionReward cr45g = Instantiate(collectionRewardPrefabs, parentOfRewardG).GetComponent<CollectionReward>();
        cr45g.OnSpawn(ClaimMode.claimMonster, handleG.anchoredPosition, handleG.localPosition, 45, _countRG, rewardStatus[ID], ID++, 145);
        allCollectionReward.Add(cr45g);

        handleG.SetAsLastSibling();
        slideRewardGrass.value = _countRG;

        // Fire
        RectTransform parentOfRewardF = slideRewardFire.transform.GetChild(2).GetComponent<RectTransform>();
        RectTransform handleF = txtRewardFire.transform.parent.GetComponent<RectTransform>();

        slideRewardFire.value = 15;
        CollectionReward cr15f = Instantiate(collectionRewardPrefabs, parentOfRewardF).GetComponent<CollectionReward>();
        cr15f.OnSpawn(ClaimMode.claimSlot, handleF.anchoredPosition, handleF.localPosition, 15, _countRF, rewardStatus[ID], ID++);
        allCollectionReward.Add(cr15f);

        slideRewardFire.value = 30;
        CollectionReward cr30f = Instantiate(collectionRewardPrefabs, parentOfRewardF).GetComponent<CollectionReward>();
        cr30f.OnSpawn(ClaimMode.claimSlot, handleF.anchoredPosition, handleF.localPosition, 30, _countRF, rewardStatus[ID], ID++);
        allCollectionReward.Add(cr30f);

        slideRewardFire.value = 45;
        CollectionReward cr45f = Instantiate(collectionRewardPrefabs, parentOfRewardF).GetComponent<CollectionReward>();
        cr45f.OnSpawn(ClaimMode.claimMonster, handleF.anchoredPosition, handleF.localPosition, 45, _countRF, rewardStatus[ID], ID++, 150);
        allCollectionReward.Add(cr45f);

        handleF.SetAsLastSibling();
        slideRewardFire.value = _countRF;

        // Water
        RectTransform parentOfReward = slideRewardWater.transform.GetChild(2).GetComponent<RectTransform>();
        RectTransform handle = txtRewardWater.transform.parent.GetComponent<RectTransform>();

        slideRewardWater.value = 15;
        CollectionReward cr15w = Instantiate(collectionRewardPrefabs, parentOfReward).GetComponent<CollectionReward>();
        cr15w.OnSpawn(ClaimMode.claimSlot, handle.anchoredPosition, handle.localPosition, 15, _countRW, rewardStatus[ID], ID++);
        allCollectionReward.Add(cr15w);

        slideRewardWater.value = 30;
        CollectionReward cr30w = Instantiate(collectionRewardPrefabs, parentOfReward).GetComponent<CollectionReward>();
        cr30w.OnSpawn(ClaimMode.claimSlot, handle.anchoredPosition, handle.localPosition, 30, _countRW, rewardStatus[ID], ID++);
        allCollectionReward.Add(cr30w);

        slideRewardWater.value = 45;
        CollectionReward cr45w = Instantiate(collectionRewardPrefabs, parentOfReward).GetComponent<CollectionReward>();
        cr45w.OnSpawn(ClaimMode.claimMonster, handle.anchoredPosition, handle.localPosition, 45, _countRW, rewardStatus[ID], ID++, 148);
        allCollectionReward.Add(cr45w);

        handle.SetAsLastSibling();
        slideRewardWater.value = _countRW;
    }

    public bool CheckReward()
    {
        List<int> caughtMonster = GameController.ins.LoadMonsterCollection();
        int countGrass = 0; int countFire = 0; int countWater = 0;
        for (int i = 0; i < caughtMonster.Count; i++)
        {
            if (GameController.ins.FindMonster(caughtMonster[i]).world.Contains(0)) countGrass++;
        }

        for (int i = 0; i < caughtMonster.Count; i++)
        {
            if (GameController.ins.FindMonster(caughtMonster[i]).world.Contains(1)) countFire++;
        }

        for (int i = 0; i < caughtMonster.Count; i++)
        {
            if (GameController.ins.FindMonster(caughtMonster[i]).world.Contains(2)) countWater++;
        }

        SpawnReward(countGrass, countFire, countWater);

        foreach (var cr in allCollectionReward)
        {
            if (cr.thisBtn.interactable == true)
            {
                foreach (CollectionReward cw in allCollectionReward)
                {
                    Destroy(cw.gameObject);
                }
                allCollectionReward.Clear();
                return true;
            }
        }

        foreach (CollectionReward cw in allCollectionReward)
        {
            Destroy(cw.gameObject);
        }
        allCollectionReward.Clear();
        return false;
    }

    private void AutoAnchorPosBottomLeft(RectTransform rectTransform, Vector2 anchoredPosition)
    {
        rectTransform.anchorMin = new Vector2(0, 0);
        rectTransform.anchorMax = new Vector2(0, 0);
        rectTransform.pivot = new Vector2(0, 0);

        rectTransform.anchoredPosition = anchoredPosition;
    }

    public void BtnBack()
    {
        AudioManager.ins.PlayOther("Click");
        imgBGCollection.DOAnchorPos(new Vector2(0, 100), 0.15f).OnComplete(() =>
        {
            imgBGCollection.DOAnchorPos(new Vector2(0, -2000), 0.4f).OnComplete(() =>
            {
                foreach (RectTransform rgm in contentOfListGrassMonster)
                {
                    Destroy(rgm.gameObject);
                }

                foreach (RectTransform rfm in contentOfListFireMonster)
                {
                    Destroy(rfm.gameObject);
                }

                foreach (RectTransform rwm in contentOfListWaterMonster)
                {
                    Destroy(rwm.gameObject);
                }
                UIManager.ins.ShowScreen(ScreenX.SHome);
            });
        });
    }

    public void BtnGrass()
    {
        AudioManager.ins.PlayOther("Click");
        btnGrass.image.sprite = spritesBtnGrass[1];
        btnFire.image.sprite = spritesBtnFire[0];
        btnWater.image.sprite = spritesBtnWater[0];

        btnGrass.image.SetNativeSize();
        btnFire.image.SetNativeSize();
        btnWater.image.SetNativeSize();

        allMonsterView.DOAnchorPosX(720, 0.25f);
    }

    public void BtnFire()
    {
        AudioManager.ins.PlayOther("Click");
        btnGrass.image.sprite = spritesBtnGrass[0];
        btnFire.image.sprite = spritesBtnFire[1];
        btnWater.image.sprite = spritesBtnWater[0];

        btnGrass.image.SetNativeSize();
        btnFire.image.SetNativeSize();
        btnWater.image.SetNativeSize();

        allMonsterView.DOAnchorPosX(0, 0.25f);
    }

    public void BtnWater()
    {
        AudioManager.ins.PlayOther("Click");
        btnGrass.image.sprite = spritesBtnGrass[0];
        btnFire.image.sprite = spritesBtnFire[0];
        btnWater.image.sprite = spritesBtnWater[1];

        btnGrass.image.SetNativeSize();
        btnFire.image.SetNativeSize();
        btnWater.image.SetNativeSize();

        allMonsterView.DOAnchorPosX(-720, 0.25f);
    }


    public void ClaimMonsterBall()
    {
        AudioManager.ins.PlayOther("Click");
        UIManager.ins.monsterView.AddEmptyButtonNotScroll();
        PlayerPrefs.SetInt(StaticString.saveCollectionRewardClaimed, 2);
    }

    public void ClaimMonster(int IDMonsterReward)
    {
        AudioManager.ins.PlayOther("Click");
        MonsterInfo rm = GameController.ins.FindMonster(IDMonsterReward);
        UIManager.ins.monsterView.RewardMonster(rm);
        PlayerPrefs.SetInt(StaticString.saveCollectionRewardClaimed, 3);
    }
}
