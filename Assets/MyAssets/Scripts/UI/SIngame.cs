
/* Copyright © @ThanhDV */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SIngame : UIController
{
    [SerializeField] public FixedTouchField touchField;
    public Button btnBack;
    public Buff buffPrefabs;
    public Buff buff1;
    public Buff buff2;
    public Transform buffMonster;
    public Image crossHair;
    public GameObject btnNo;
    public RectTransform showBuffPos;
    public Button btnSkip;

    public override void Show(bool value)
    {
        base.Show(value);
        if (value)
        {
            btnSkip.gameObject.SetActive(false);
            if (!GameController.ins.isInArena)
            {
                if (UIManager.ins.sHome.ShowLevel(false))
                {
                    UIManager.ins.sHome.groupShowLevel.anchoredPosition = new Vector2(30, 250);
                }
                else
                {
                    UIManager.ins.sHome.groupShowLevel.anchoredPosition = new Vector2(75, 250);
                }
                UIManager.ins.sHome.groupShowLevel.DOAnchorPosY(-150, 0.5f);
            }
            else
            {
                UIManager.ins.sHome.groupShowLevel.gameObject.SetActive(false);
                StartCoroutine(IEArenaCountdown());
            }


            if (GameController.ins.isChoosingBuff)
            {
                btnNo.SetActive(true);
            }
            else
            {
                btnNo.SetActive(false);
            }

            RectTransform rectBtnBack = btnBack.GetComponent<RectTransform>();
            rectBtnBack.anchoredPosition = new Vector2(75, 150);

            rectBtnBack.DOAnchorPos(new Vector2(75, -125), 0.35f).OnComplete(() =>
            {
                rectBtnBack.DOAnchorPos(new Vector2(75, -75), 0.15f);
            });
        }
    }

    private IEnumerator IEArenaCountdown()
    {
        // GameController.ins.UseMainCam();

        // Vector3 dir = GameController.ins.currentBoss.transform.position - GameController.ins.player.transform.position;
        // float angle = Vector3.SignedAngle(GameController.ins.player.transform.forward, dir, -Vector3.up);

        // foreach (Transform i in GameController.ins.currentMap.playerMonsterPos)
        // {
        //     i.RotateAround(GameController.ins.player.transform.position, Vector3.up, angle);
        // }

        // PlayerPrefs.SetInt(StaticString.saveBuff, -1);

        // yield return new WaitForSeconds(0.5f);

        UIManager.ins.sArena.IntroArena();

        yield return new WaitForSeconds(10f);
        AudioManager.ins.PlayOther("Bell");

        GameController.ins.currentBoss.animator.Play(GameController.ins.currentBoss.clipThrow.name);
        GameController.ins.player.animator.SetTrigger("AutoThrow");

        // yield return new WaitForSeconds(1f);
        // foreach (Transform i in GameController.ins.currentMap.playerMonsterPos)
        // {
        //     i.RotateAround(GameController.ins.player.transform.position, Vector3.up, -angle);
        // }
    }

    public void LoadBuff()
    {
        GameController.ins.isChoosingBuff = true;
        UIManager.ins.sIngame.touchField.enabled = false;

        buff1 = Instantiate(buffPrefabs.gameObject, transform).GetComponent<Buff>();
        buff2 = Instantiate(buffPrefabs.gameObject, transform).GetComponent<Buff>();

        buff1.thisRectTransform.anchoredPosition = new Vector2(-500, 15);
        buff2.thisRectTransform.anchoredPosition = new Vector2(500, 15);

        buff1.thisRectTransform.DOAnchorPos(new Vector2(-150, 15), 0.35f);
        buff2.thisRectTransform.DOAnchorPos(new Vector2(150, 15), 0.35f);

        List<int> ranBuff = ChooseBuff();
        buff1.OnSpawn(ranBuff[0], 1);
        buff2.OnSpawn(ranBuff[1], 2);
    }

    public List<int> ChooseBuff()
    {
        int ranBuff1 = Random.Range(0, 5);
        int ranBuff2 = Random.Range(0, 5);

        if (ranBuff1 != ranBuff2)
        {
            List<int> l = new List<int>();
            l.Add(ranBuff1);
            l.Add(ranBuff2);

            return l;
        }
        else
        {
            return ChooseBuff();
        }
    }

    public void BtnNo()
    {
        AudioManager.ins.PlayOther("Click");
        btnNo.SetActive(false);
        UIManager.ins.sIngame.buff1.thisRectTransform.DOAnchorPosX(-500, 0.25f);
        UIManager.ins.sIngame.buff2.thisRectTransform.DOAnchorPosX(500, 0.25f).OnComplete(() =>
        {
            PlayerPrefs.SetInt(StaticString.saveBuff, -1);

            int checkMap = GameController.ins.ReadInt(GameController.ins.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14);

            if (checkMap == 1) //BossMap
            {
                btnSkip.gameObject.SetActive(false);
                foreach (BtnChoseMonster btn in UIManager.ins.monsterView.allBallButton)
                {
                    btn.thisBtn.interactable = false;
                }
                UIManager.ins.sHome.IntroSpecialMap();
            }
            else if (checkMap == 2) //ArenaMap
            {
                btnSkip.gameObject.SetActive(false);
                UIManager.ins.sHome.IntroSpecialMap();
            }
            else //NormalMap
            {
                btnSkip.gameObject.SetActive(true);
                foreach (BtnChoseMonster btn in UIManager.ins.monsterView.allBallButton)
                {
                    btn.thisBtn.interactable = true;
                }
                GameController.ins.isPlaying = true;
            }

            GameController.ins.isChoosingBuff = false;
            UIManager.ins.sIngame.touchField.enabled = true;
            Destroy(UIManager.ins.sIngame.buff1.gameObject);
            Destroy(UIManager.ins.sIngame.buff2.gameObject);
        });
    }

    public void BtnSkipLevel()
    {
        ManagerAds.ins.ShowRewarded((s) =>
        {
            btnSkip.gameObject.SetActive(false);
            foreach (EnemyMonster m in GameController.ins.listEnemyMonster)
            {
                m.OnDespawn();
            }
        });
    }

    public void BtnBack()
    {
        AudioManager.ins.PlayOther("Click");
        ManagerAds.ins.ShowInterstitial(null, "BtnHome");
        if (GameController.ins.isMoving || GameController.ins.listBallSpawned.Count > 0) return;

        RectTransform rectBtnBack = btnBack.GetComponent<RectTransform>();

        if (buff1 != null && buff2 != null)
        {
            buff1.thisRectTransform.DOAnchorPos(new Vector2(-500, 15), 0.35f);
            buff2.thisRectTransform.DOAnchorPos(new Vector2(500, 15), 0.35f);
        }

        rectBtnBack.DOAnchorPos(new Vector2(75, -125), 0.15f).OnComplete(() =>
        {
            rectBtnBack.DOAnchorPos(new Vector2(75, 75), 0.175f).OnComplete(() =>
            {
                if (GameController.ins.isInArena)
                {
                    UIManager.ins.sLose.BtnNo();
                    return;
                }

                GameController.ins.isLosing = false;
                GameController.ins.isPlaying = false;
                GameController.ins.isInSpecialWorld = false;
                GameController.ins.isChoosingBuff = false;

                GameController.ins.ResetMonsterBallBoss();
                GameController.ins.LoadMonsterView();
                if (PlayerPrefs.GetInt(StaticString.checkFirstTimePlay) == 1 && GameController.ins.LoadMonsterCollection().Count <= 0)
                {
                    GameController.ins.LoadTutorial();
                }
                else
                {
                    PlayerPrefs.SetInt(StaticString.checkFirstTimePlay, 0);
                    GameController.ins.LoadLevel();
                }

                GameController.ins.player.animator.Play(GameController.ins.player.clipIdle1.name);
                GameController.ins.player.DisableLaser();
                Destroy(GameController.ins.player.handEvents.newBall);
                Destroy(GameController.ins.player.handEvents.monsterOnHand);
                if (buff1) Destroy(buff1.gameObject);
                if (buff2) Destroy(buff2.gameObject);

                UIManager.ins.ShowScreen(ScreenX.SHome);
            });
        });

    }
}
