
/* Copyright © @ThanhDV */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PLogin : UIController
{
    public override void Show(bool value)
    {
        base.Show(value);
        if (value)
        {

        }
    }

    public void BtnQuit()
    {
        AudioManager.ins.PlayOther("Click");
        UIManager.ins.ShowPopup(Popup.None);
    }
}
