using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SGloves : UIController
{
    public RectTransform imgBGGloves;
    public GlovesItem glovesItemPrefabs;
    public RectTransform glovesContent;
    public List<GlovesItem> glovesItemsSpawned = new List<GlovesItem>();

    public override void Show(bool value)
    {
        base.Show(value);
        if (value)
        {
            imgBGGloves.anchoredPosition = new Vector2(0, -2000);

            int countItemInRow = 0;
            int newColumn = 0;
            for (int i = 0; i < GameController.ins.allGlovesAvatar.Length; i++)
            {
                RectTransform item = Instantiate(glovesItemPrefabs.gameObject, glovesContent).GetComponent<RectTransform>();
                GlovesItem gi = item.GetComponent<GlovesItem>();

                gi.OnSpawn(i);
                glovesItemsSpawned.Add(gi);

                item.anchoredPosition = new Vector2(124 + (225 * countItemInRow), -175 - (345 * newColumn));

                countItemInRow++;
                if (countItemInRow > 2)
                {
                    countItemInRow = 0;
                    newColumn++;
                }
            }
            glovesContent.sizeDelta = new Vector2(glovesContent.sizeDelta.x, 125 + (345f * (newColumn)));

            imgBGGloves.DOAnchorPos(new Vector2(0, 100), 0.4f).OnComplete(() =>
            {
                imgBGGloves.DOAnchorPos(new Vector2(0, 0), 0.15f);
            });
        }
    }

    public void BtnBack()
    {
        AudioManager.ins.PlayOther("Click");
        imgBGGloves.DOAnchorPos(new Vector2(0, 100), 0.15f).OnComplete(() =>
        {
            imgBGGloves.DOAnchorPos(new Vector2(0, -2000), 0.4f).OnComplete(() =>
            {
                UIManager.ins.ShowScreen(ScreenX.SHome);
                foreach (GlovesItem g in glovesItemsSpawned)
                {
                    Destroy(g.gameObject);
                }
                glovesItemsSpawned.Clear();
            });
        });
    }
}
