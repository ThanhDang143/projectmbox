
/* Copyright © @ThanhDV */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SShop : UIController
{
    public IAPFunction IAPFunction;
    public RectTransform imgBGShop;
    public RectTransform content;
    public InputField inputLevel;

    public override void Show(bool value)
    {
        base.Show(value);
        if (value)
        {
            content.anchoredPosition = Vector2.zero;
            imgBGShop.anchoredPosition = new Vector2(0, -2000);

            imgBGShop.DOAnchorPos(new Vector2(0, 100), 0.4f).OnComplete(() =>
            {
                imgBGShop.DOAnchorPos(new Vector2(0, 0), 0.15f);
            });

            IAPFunction.CheckOnLoadShop(true);
        }
    }

    public void BtnSetLevel()
    {
        AudioManager.ins.PlayOther("Click");
        int intLevel = int.Parse(inputLevel.text);
        if (intLevel > 465) return;

        int readCurrentMapType = GameController.ins.ReadInt(GameController.ins.txtLevelScript, intLevel, 14);
        if (readCurrentMapType == 0)
        {
            PlayerPrefs.SetInt(StaticString.saveLevel, intLevel);
        } // Lose normal level -0
        else if (readCurrentMapType == 1) // Lose normal boss level -1
        {
            PlayerPrefs.SetInt(StaticString.saveLevel, intLevel - 1);
        }
        else // Lose vip boss level -2
        {
            PlayerPrefs.SetInt(StaticString.saveLevel, intLevel - 2);
        }

        GameController.ins.player.animator.Play(GameController.ins.player.clipIdle1.name);
        Destroy(GameController.ins.player.handEvents.newBall);
        Destroy(GameController.ins.player.handEvents.monsterOnHand);

        GameController.ins.isLosing = false;
        GameController.ins.isInSpecialWorld = false;
        GameController.ins.ResetMonsterBallBoss();
        GameController.ins.LoadMonsterView();

        GameController.ins.LoadLevel();
        UIManager.ins.ShowScreen(ScreenX.SHome);
    }


    public void BtnBack()
    {
        AudioManager.ins.PlayOther("Click");
        imgBGShop.DOAnchorPos(new Vector2(0, 100), 0.15f).OnComplete(() =>
        {
            imgBGShop.DOAnchorPos(new Vector2(0, -2000), 0.4f).OnComplete(() =>
            {
                UIManager.ins.ShowScreen(ScreenX.SHome);
            });
        });
    }
}
