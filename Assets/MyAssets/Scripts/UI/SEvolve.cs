
/* Copyright © @ThanhDV */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SEvolve : UIController
{
    public override void Show(bool value)
    {
        base.Show(value);
        if (value)
        {
            UIManager.ins.ShowPopup(Popup.None);
            GameController.ins.ChangeTime(false);
            GameController.ins.player.handRenderer.gameObject.SetActive(false);
        }
    }
}
