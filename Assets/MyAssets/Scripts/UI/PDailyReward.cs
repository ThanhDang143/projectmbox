using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class PDailyReward : UIController
{
    public RectTransform imgBG;
    public Sprite spriteGreen;
    public Sprite spriteBlue;
    public Image[] allDay;
    public GameObject[] allCheck;
    public GameObject panelReward;
    public Text rewardText;
    public Image rewardImg;

    public override void Show(bool value)
    {
        base.Show(value);
        if (value)
        {
            panelReward.SetActive(false);
            LoadClaimed();
            imgBG.anchoredPosition = new Vector2(0, 1250);

            imgBG.DOAnchorPos(new Vector2(0, -100), 0.3f).OnComplete(() =>
            {
                imgBG.DOAnchorPos(new Vector2(0, 0), 0.15f);
            });
        }
    }

    public void LoadClaimed()
    {
        foreach (Image img in allDay)
        {
            img.sprite = spriteBlue;
        }

        foreach (GameObject g in allCheck)
        {
            g.SetActive(false);
        }

        if (UIManager.ins.sHome.CanClaimDailyReward())
        {
            for (int i = 0; i <= PlayerPrefs.GetInt(StaticString.saveDailyClaimed); i++)
            {
                allDay[i].sprite = spriteGreen;
            }
            for (int i = 0; i < PlayerPrefs.GetInt(StaticString.saveDailyClaimed); i++)
            {
                allCheck[i].SetActive(true);
            }
        }
        else
        {
            for (int i = 0; i < PlayerPrefs.GetInt(StaticString.saveDailyClaimed); i++)
            {
                allDay[i].sprite = spriteGreen;
            }
            for (int i = 0; i < PlayerPrefs.GetInt(StaticString.saveDailyClaimed); i++)
            {
                allCheck[i].SetActive(true);
            }
        }
    }

    public void BtnQuit()
    {
        AudioManager.ins.PlayOther("Click");
        imgBG.DOAnchorPos(new Vector2(0, -100), 0.15f).OnComplete(() =>
        {
            imgBG.DOAnchorPos(new Vector2(0, 1250), 0.3f).OnComplete(() =>
            {
                if (PlayerPrefs.GetInt(StaticString.saveDailyClaimed) >= 5)
                {
                    UIManager.ins.sHome.btnDaily.SetActive(false);
                }
                else
                {
                    UIManager.ins.sHome.btnDaily.SetActive(true);
                }

                if (UIManager.ins.sHome.CanClaimDailyReward())
                {
                    UIManager.ins.sHome.btnDailyFX.SetActive(true);
                }
                else
                {
                    UIManager.ins.sHome.btnDailyFX.SetActive(false);
                }
                UIManager.ins.ShowPopup(Popup.None);
            });
        });
    }

    public void ClosePanel()
    {
        UIManager.ins.monsterView.SortBtn();
        panelReward.SetActive(false);
        LoadClaimed();
    }

    public void BtnClaim()
    {
        AudioManager.ins.PlayOther("Click");
        if (!UIManager.ins.sHome.CanClaimDailyReward()) return;
        int dailyClaimed = PlayerPrefs.GetInt(StaticString.saveDailyClaimed);
        switch (dailyClaimed)
        {
            case 0:
                MonsterInfo rm = GameController.ins.FindMonster(102);
                UIManager.ins.monsterView.RewardMonster(rm);
                rewardText.text = "+1 Monster";
                rewardImg.sprite = rm.avatar[1];
                rewardImg.SetNativeSize();
                break;
            case 1:
                UIManager.ins.monsterView.AddEmptyButtonNotScroll();
                rewardText.text = "+1 Monster Slot";
                rewardImg.sprite = UIManager.ins.sSpin.rewardSprite[0];
                rewardImg.SetNativeSize();
                break;
            case 2:
                // PlayerPrefs.SetInt(StaticString.saveTicket, PlayerPrefs.GetInt(StaticString.saveTicket) + 5);
                // rewardText.text = "+5 Spin Ticket";
                // rewardImg.sprite = UIManager.ins.sSpin.rewardSprite[1];
                // rewardImg.SetNativeSize();

                bool receivedReward = false;
                for (int i = 1; i < UIManager.ins.monsterView.allBallButton.Count; i++)
                {
                    BtnChoseMonster btn = UIManager.ins.monsterView.allBallButton[i];

                    if (btn.IDMonster <= 0) continue;

                    MonsterInfo m = GameController.ins.FindMonster(btn.IDMonster);
                    if (m.IDMonsterEvolve > 0)
                    {
                        receivedReward = true;
                        btn.IDMonster = m.IDMonsterEvolve;
                        GameController.ins.SaveMonsterView();
                        rewardText.text = "Evolve Monster";
                        rewardImg.sprite = GameController.ins.FindMonster(m.IDMonsterEvolve).avatar[1];
                        rewardImg.SetNativeSize();
                    }
                }

                if (!receivedReward)
                {
                    MonsterInfo rmmm = GameController.ins.FindMonster(107);
                    UIManager.ins.monsterView.RewardMonster(rmmm);
                    rewardText.text = "+1 Monster";
                    rewardImg.sprite = rmmm.avatar[1];
                    rewardImg.SetNativeSize();
                }

                break;
            case 3:
                UIManager.ins.monsterView.AddEmptyButtonNotScroll();
                UIManager.ins.monsterView.AddEmptyButtonNotScroll();
                rewardText.text = "+2 Monster Slot";
                rewardImg.sprite = UIManager.ins.sSpin.rewardSprite[0];
                rewardImg.SetNativeSize();
                break;
            case 4:
                MonsterInfo rmm = GameController.ins.FindMonster(120);
                UIManager.ins.monsterView.RewardMonster(rmm);
                rewardText.text = "+1 Monster";
                rewardImg.sprite = rmm.avatar[1];
                rewardImg.SetNativeSize();
                break;
        }
        PlayerPrefs.SetInt(StaticString.saveDailyClaimed, dailyClaimed + 1);
        PlayerPrefs.SetString(StaticString.saveTimeLastDailyClaim, System.DateTime.Now.ToBinary().ToString());
        AudioManager.ins.PlayOther("Win");
        panelReward.SetActive(true);
    }
}
