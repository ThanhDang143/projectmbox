using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Exploder.Utils;
using UnityEngine;
using UnityEngine.UI;

public class SSpin : UIController
{
    [HideInInspector] public GameObject spinBase;
    [SerializeField] public List<GameObject> allSpinBall;
    public GameObject[] spinBallsPrefabs;
    public Sprite[] rewardSprite;
    public Text txtTicket;
    public GameObject rewardPanel;
    public Image rewardImg;
    public Text rewardText;
    public GameObject spinBasePrefabs;
    public RectTransform btnSpin;
    public RectTransform btnSpinAds;
    public RectTransform btnBack;
    private bool spinning;

    public override void Show(bool value)
    {
        base.Show(value);
        if (value)
        {
            rewardPanel.SetActive(false);
            btnSpin.anchoredPosition = new Vector2(-800, 250);
            btnSpinAds.anchoredPosition = new Vector2(800, 250);
            btnBack.anchoredPosition = new Vector2(75, 125);

            btnSpin.DOAnchorPos(new Vector2(-175, 250), 0.5f);
            btnSpinAds.DOAnchorPos(new Vector2(175, 250), 0.5f);
            btnBack.DOAnchorPos(new Vector2(75, -75), 0.5f);
            spinning = false;

            txtTicket.text = PlayerPrefs.GetInt(StaticString.saveTicket).ToString();

            StartCoroutine(IESpinIdle());
        }
    }

    private IEnumerator IESpinIdle()
    {
        while (!spinning)
        {
            foreach (GameObject b in allSpinBall)
            {
                Vector3 dirForce = new Vector3(Random.Range(-1f, 1f), Random.Range(0f, 1f), Random.Range(-1f, 1f));
                b.GetComponent<Rigidbody>().AddForce(dirForce * 1250);
            }
            yield return new WaitForSeconds(0.5f);
        }
    }

    public void BtnSpin()
    {
        AudioManager.ins.PlayOther("Click");
        if (spinning || PlayerPrefs.GetInt(StaticString.saveTicket) <= 0) return;
        PlayerPrefs.SetInt(StaticString.saveTicket, PlayerPrefs.GetInt(StaticString.saveTicket) - 1);
        txtTicket.text = PlayerPrefs.GetInt(StaticString.saveTicket).ToString();
        StartCoroutine(IEAddForce());
    }

    public void BtnSpinAds()
    {
        AudioManager.ins.PlayOther("Click");
        if (spinning) return;
        ManagerAds.ins.ShowRewarded((s) =>
        {
            if (!s) return;
            StartCoroutine(IEAddForce());
        }, "Spin");
    }

    public void BtnBack()
    {
        if (spinning) return;

        AudioManager.ins.PlayOther("Click");
        GameController.ins.isSpin = false;
        GameController.ins.sea.SetActive(true);
        GameController.ins.player.handRenderer.enabled = true;
        GameController.ins.ResetMonsterBallBoss();
        Destroy(spinBase);
        foreach (GameObject b in allSpinBall)
        {
            Destroy(b);
        }
        GameController.ins.LoadLevel();
        UIManager.ins.ShowScreen(ScreenX.SHome);
    }

    public IEnumerator IEAddForce()
    {
        spinning = true;
        for (int i = 0; i < 10; i++)
        {
            foreach (GameObject b in allSpinBall)
            {
                Vector3 dirForce = new Vector3(Random.Range(-1f, 1f), Random.Range(0f, 1f), Random.Range(-1f, 1f));
                b.GetComponent<Rigidbody>().AddForce(dirForce * 10000);
            }
            yield return new WaitForSeconds(0.5f);
        }
        Transform resultBall = Instantiate(spinBallsPrefabs[Random.Range(0, spinBallsPrefabs.Length)], spinBase.transform.GetChild(2).position, Quaternion.Euler(UnityEngine.Random.Range(-180, 180), UnityEngine.Random.Range(-180, 180), UnityEngine.Random.Range(-180, 180))).transform;
        resultBall.localScale = Vector3.zero;
        resultBall.DOScale(1, 0.5f);
        yield return new WaitForSeconds(1f);

        for (int i = 0; i < 20; i++)
        {
            resultBall.DOShakePosition(0.25f, 0.1f);
            yield return new WaitForSeconds(0.25f);
        }

        ExploderSingleton.Instance.ExplodeObject(resultBall.gameObject);
        Destroy(resultBall.gameObject, 5.1f);

        yield return new WaitForSeconds(0.25f);
        int reward = Random.Range(0, 100);

        if (reward >= 0 && reward < 15) // 15% +1 monster lv1
        {
            int allMonsterLength = GameController.ins.allMonster.Length - 10;
            MonsterInfo rm = GameController.ins.allMonster[Random.Range(0, allMonsterLength / 2)];
            UIManager.ins.monsterView.RewardMonster(rm);
            rewardText.text = "+1 Monster Lv1";
            rewardImg.sprite = rm.avatar[1];
            rewardImg.SetNativeSize();
        }
        else if (reward >= 15 && reward < 35) // 20% +1 slot
        {
            UIManager.ins.monsterView.AddEmptyButtonNotScroll();
            rewardText.text = "+1 Monster Slot";
            rewardImg.sprite = rewardSprite[0];
            rewardImg.SetNativeSize();
        }
        else if (reward >= 35 && reward < 75) // 40% +1 spin
        {
            PlayerPrefs.SetInt(StaticString.saveTicket, PlayerPrefs.GetInt(StaticString.saveTicket) + 1);
            rewardText.text = "+1 Spin Ticket";
            rewardImg.sprite = rewardSprite[1];
            rewardImg.SetNativeSize();
        }
        else if (reward >= 75 && reward < 95) // 20% evolve random monster else +1 Spin
        {
            if (!RewardEvolveMonster())
            {
                PlayerPrefs.SetInt(StaticString.saveTicket, PlayerPrefs.GetInt(StaticString.saveTicket) + 1);
                rewardText.text = "+1 Spin Ticket";
                rewardImg.sprite = rewardSprite[1];
                rewardImg.SetNativeSize();
            }
        }
        else // 5% +1 vip Monster
        {
            int allMonsterLength = GameController.ins.allMonster.Length;
            MonsterInfo rm = GameController.ins.allMonster[Random.Range(allMonsterLength - 10, allMonsterLength)];
            UIManager.ins.monsterView.RewardMonster(rm);
            rewardText.text = "+1 Monster VIP";
            rewardImg.sprite = rm.avatar[1];
            rewardImg.SetNativeSize();
        }

        AudioManager.ins.PlayOther("Win");
        rewardPanel.SetActive(true);
    }

    public void ClosePanel()
    {
        rewardPanel.SetActive(false);
        txtTicket.text = PlayerPrefs.GetInt(StaticString.saveTicket).ToString();

        UIManager.ins.monsterView.SortBtn();
        spinning = false;

        StartCoroutine(IESpinIdle());
    }

    public bool RewardEvolveMonster()
    {
        for (int i = 1; i < UIManager.ins.monsterView.allBallButton.Count; i++)
        {
            BtnChoseMonster btn = UIManager.ins.monsterView.allBallButton[i];

            if (btn.IDMonster <= 0) continue;

            MonsterInfo m = GameController.ins.FindMonster(btn.IDMonster);
            if (m.IDMonsterEvolve > 0)
            {
                btn.IDMonster = m.IDMonsterEvolve;
                GameController.ins.SaveMonsterView();
                rewardText.text = "Evolve Monster";
                rewardImg.sprite = GameController.ins.FindMonster(m.IDMonsterEvolve).avatar[1];
                rewardImg.SetNativeSize();
                return true;
            }
        }
        return false;
    }
}
