
/* Copyright © @ThanhDV */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHealthBar : MonoBehaviour
{
    public Image imgFillHealthBar;
    public Slider healthBarSlider;
    private float initScale;

    private void Start()
    {
        initScale = GetComponent<RectTransform>().localScale.x;
    }

    private void LateUpdate()
    {
        Transform cam = GameController.ins.mainCam;
        transform.LookAt(transform.position + cam.forward);
        float dis = Vector3.Distance(transform.parent.position, GameController.ins.player.transform.position);

        transform.localScale = Vector3.one * 0.05f * dis * initScale;
    }
}
