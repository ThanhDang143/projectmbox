
/* Copyright © @ThanhDV */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager ins;

    [Header("Screen")]
    public SHome sHome;
    public SIngame sIngame;
    public SLose sLose;
    public SWin sWin;
    public MonsterView monsterView;
    public ArenaMonsterView arenaMonsterView;
    public SEvolve sEvolve;
    public SShop sShop;
    public SCollection sCollection;
    public SExchange sExchange;
    public SSpin sSpin;
    public SGloves sGloves;
    public SArena sArena;

    [Header("Popup")]
    public PSettings pSettings;
    public PChangeInfo pChangeInfo;
    public PArenaLoading pArenaLoading;
    public PDailyReward pDaily;
    public PTutorial1 pTutorial1;
    public PTutorial2 pTutorial2;
    public PTutorial3 pTutorial3;
    public PTutorial4 pTutorial4;
    public PTutorial5 pTutorial5;

    public Popup currentPopup = Popup.None;


    private void Awake()
    {
        ins = this;
    }

    private void Start()
    {
        ShowScreen(ScreenX.SHome);
        if (PlayerPrefs.GetInt(StaticString.checkFirstTimePlay) == 1 && GameController.ins.LoadMonsterCollection().Count <= 0)
        {
            ShowPopup(Popup.PTutorial5);
        }
        else
        {
            ShowPopup(Popup.None);
        }
    }

    public void ShowScreen(ScreenX s)
    {
        if (s == ScreenX.SHome)
        {
            sHome.groupShowLevel.parent = sHome.transform;
        }
        else if (s == ScreenX.SIngame)
        {
            sHome.groupShowLevel.parent = sIngame.transform;
        }

        if (s == ScreenX.SIngame)
        {
            if (GameController.ins.isInArena)
            {
                ShowMonsterView(EnumMonsterView.ArenaMonsterView);
            }
            else
            {
                ShowMonsterView(EnumMonsterView.NormalMonsterView);
            }
            for (int i = 1; i < UIManager.ins.monsterView.allBallButton.Count; i++)
            {
                BtnChoseMonster btnController = UIManager.ins.monsterView.allBallButton[i];
                btnController.thisBtn.interactable = true;
            }
        }
        else if (s == ScreenX.SHome)
        {
            ShowMonsterView(EnumMonsterView.NormalMonsterView);
        }
        else
        {
            ShowMonsterView(EnumMonsterView.None);
        }

        sHome.Show(s == ScreenX.SHome);
        sIngame.Show(s == ScreenX.SIngame);
        sLose.Show(s == ScreenX.SLose);
        sWin.Show(s == ScreenX.SWin);
        sEvolve.Show(s == ScreenX.SEvolve);
        sShop.Show(s == ScreenX.SShop);
        sCollection.Show(s == ScreenX.SCollection);
        sExchange.Show(s == ScreenX.SExchange);
        sSpin.Show(s == ScreenX.SSpin);
        sGloves.Show(s == ScreenX.SGloves);
        sArena.Show(s == ScreenX.SArena);
    }

    public void ShowPopup(Popup p)
    {
        pSettings.Show(p == Popup.PSettings);
        pChangeInfo.Show(p == Popup.PChangeInfo);
        pArenaLoading.Show(p == Popup.PArenaLoading);
        pDaily.Show(p == Popup.PDaily);
        pTutorial1.Show(p == Popup.PTutorial1);
        pTutorial2.Show(p == Popup.PTutorial2);
        pTutorial3.Show(p == Popup.PTutorial3);
        pTutorial4.Show(p == Popup.PTutorial4);
        pTutorial5.Show(p == Popup.PTutorial5);
        currentPopup = p;

        if (p == Popup.PChangeInfo)
        {
            ShowMonsterView(EnumMonsterView.NormalMonsterView);
        }
    }

    public void ShowMonsterView(EnumMonsterView mv)
    {
        monsterView.Show(mv == EnumMonsterView.NormalMonsterView);
        arenaMonsterView.Show(mv == EnumMonsterView.ArenaMonsterView);
    }
}

public enum ScreenX
{
    None,
    SHome,
    SIngame,
    SLose,
    SWin,
    SEvolve,
    SShop,
    SCollection,
    SExchange,
    SSpin,
    SGloves,
    SArena
}

public enum EnumMonsterView
{
    None,
    NormalMonsterView,
    ArenaMonsterView
}

public enum Popup
{
    None,
    PSettings,
    PChangeInfo,
    PArenaLoading,
    PDaily,
    PTutorial1,
    PTutorial2,
    PTutorial3,
    PTutorial4,
    PTutorial5,
}

