using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.UI.Extensions;
using SendEventAppMetrica;
using System;

public class SArena : UIController
{
    public RectTransform imgBG;
    public RectTransform content;
    public UI_ScrollRectOcclusion uI_ScrollRectOcclusion;
    public Text txtMainRank;
    public Text txtMainName;
    public Text txtMainPoint;
    public Sprite defaultAvatar;
    public Image imgMainTopRank;
    public Image playerMainAvatar;
    public Image[] monsterMainAvatar;
    public GameObject imgArenaPlayerInforPrefabs;
    public Sprite[] spriteTopRank;
    public Sprite[] bGArenaPlayerInfo;
    public ArenaPlayerInfor[] allArenaPlayerInfor;

    public override void Show(bool value)
    {
        base.Show(value);
        if (value)
        {
            GetMainPlayerInfor();
            LoadTop100ArenaPlayerData();
            content.anchoredPosition = Vector2.zero;
            uI_ScrollRectOcclusion.Init();
            imgBG.anchoredPosition = new Vector2(0, -2000);

            imgBG.DOAnchorPos(new Vector2(0, 100), 0.4f).OnComplete(() =>
            {
                imgBG.DOAnchorPos(new Vector2(0, 0), 0.15f);
            });
        }
    }

    public void CheckAndRefreshArenaData()
    {
        DateTime currentDate = DateTime.Now;

        long temp = Convert.ToInt64(PlayerPrefs.GetString(StaticString.saveLastRefreshRank));

        DateTime oldDate = DateTime.FromBinary(temp);

        TimeSpan currentDateSubtractOldDate = currentDate.Subtract(oldDate);

        if (currentDateSubtractOldDate.Hours >= 1)
        {
            GameController.ins.ArenaRefreshRank();
            PlayerPrefs.SetString(StaticString.saveLastRefreshRank, DateTime.Now.ToBinary().ToString());
        }
    }

    private void LoadTop100ArenaPlayerData()
    {
        CheckAndRefreshArenaData();
        GameController gc = GameController.ins;
        ArenaData100[] arenaData100s = gc.LoadAllDataRank();
        if (content.childCount <= 0)
        {
            allArenaPlayerInfor = new ArenaPlayerInfor[100];
            for (int i = 0; i < arenaData100s.Length; i++)
            {
                if (i + 1 != PlayerPrefs.GetInt(StaticString.saveArenaRank))
                {
                    ArenaPlayerInfor ap = Instantiate(imgArenaPlayerInforPrefabs, content).GetComponent<ArenaPlayerInfor>();
                    if (i < 3)
                    {
                        ap.topRank.color = Color.white;
                        ap.topRank.sprite = spriteTopRank[i];
                        ap.topRank.SetNativeSize();
                        ap.txtRank.text = "";
                        ap.GetComponent<Image>().sprite = bGArenaPlayerInfo[i];
                    }
                    else
                    {
                        ap.topRank.color = new Color(0, 0, 0, 0);
                        ap.txtRank.text = (i + 1).ToString();
                        ap.GetComponent<Image>().sprite = bGArenaPlayerInfo[3];
                    }

                    ap.txtName.text = arenaData100s[i].nameArena;
                    ap.txtPoint.text = arenaData100s[i].pointArena.ToString();
                    ap.playerAvatar.sprite = gc.FindMonster(arenaData100s[i].avatarArena).avatar[1];
                    ap.playerAvatar.SetNativeSize();

                    int[] monsters = new int[5] { arenaData100s[i].monster1Arena, arenaData100s[i].monster2Arena, arenaData100s[i].monster3Arena, arenaData100s[i].monster4Arena, arenaData100s[i].monster5Arena };
                    for (int j = 0; j < ap.monsterAvatar.Length; j++)
                    {
                        if (monsters[j] > 0)
                        {
                            ap.monsterAvatar[j].color = Color.white;
                            ap.monsterAvatar[j].sprite = gc.FindMonster(monsters[j]).avatar[1];
                            ap.monsterAvatar[j].SetNativeSize();
                        }
                        else
                        {
                            ap.monsterAvatar[j].color = new Color(0, 0, 0, 0);
                        }
                    }

                    RectTransform apTransform = ap.GetComponent<RectTransform>();
                    apTransform.anchoredPosition = new Vector2(0, -60 + (-110 * i));

                    allArenaPlayerInfor[i] = ap;
                }
                else
                {
                    ArenaPlayerInfor ap = Instantiate(imgArenaPlayerInforPrefabs, content).GetComponent<ArenaPlayerInfor>();
                    if (i < 3)
                    {
                        ap.topRank.color = Color.white;
                        ap.topRank.sprite = spriteTopRank[i];
                        ap.topRank.SetNativeSize();
                        ap.txtRank.text = "";
                        ap.GetComponent<Image>().sprite = bGArenaPlayerInfo[i];
                    }
                    else
                    {
                        ap.topRank.color = new Color(0, 0, 0, 0);
                        ap.txtRank.text = (i + 1).ToString();
                        ap.GetComponent<Image>().sprite = bGArenaPlayerInfo[3];
                    }

                    ap.txtName.text = PlayerPrefs.GetString(StaticString.saveArenaName);
                    ap.txtPoint.text = PlayerPrefs.GetInt(StaticString.saveArenaPoint).ToString();
                    ap.playerAvatar.sprite = gc.FindMonster(PlayerPrefs.GetInt(StaticString.saveAvatar)).avatar[1];
                    ap.playerAvatar.SetNativeSize();

                    int[] monsters = new int[5] { PlayerPrefs.GetInt(StaticString.saveArenaMonster0), PlayerPrefs.GetInt(StaticString.saveArenaMonster1), PlayerPrefs.GetInt(StaticString.saveArenaMonster2), PlayerPrefs.GetInt(StaticString.saveArenaMonster3), PlayerPrefs.GetInt(StaticString.saveArenaMonster4) };
                    for (int j = 0; j < ap.monsterAvatar.Length; j++)
                    {
                        if (monsters[j] > 0)
                        {
                            ap.monsterAvatar[j].color = Color.white;
                            ap.monsterAvatar[j].sprite = gc.FindMonster(monsters[j]).avatar[1];
                            ap.monsterAvatar[j].SetNativeSize();
                        }
                        else
                        {
                            ap.monsterAvatar[j].color = new Color(0, 0, 0, 0);
                        }
                    }

                    RectTransform apTransform = ap.GetComponent<RectTransform>();
                    apTransform.anchoredPosition = new Vector2(0, -60 + (-110 * i));

                    allArenaPlayerInfor[i] = ap;
                }
            }
        }
        else
        {
            for (int i = 0; i < allArenaPlayerInfor.Length; i++)
            {
                if (i + 1 != PlayerPrefs.GetInt(StaticString.saveArenaRank))
                {
                    ArenaPlayerInfor ap = allArenaPlayerInfor[i];
                    if (i < 3)
                    {
                        ap.topRank.color = Color.white;
                        ap.topRank.sprite = spriteTopRank[i];
                        ap.topRank.SetNativeSize();
                        ap.txtRank.text = "";
                        ap.GetComponent<Image>().sprite = bGArenaPlayerInfo[i];
                    }
                    else
                    {
                        ap.topRank.color = new Color(0, 0, 0, 0);
                        ap.txtRank.text = (i + 1).ToString();
                        ap.GetComponent<Image>().sprite = bGArenaPlayerInfo[3];
                    }

                    ap.txtName.text = arenaData100s[i].nameArena;
                    ap.txtPoint.text = arenaData100s[i].pointArena.ToString();
                    ap.playerAvatar.sprite = gc.FindMonster(arenaData100s[i].avatarArena).avatar[1];
                    ap.playerAvatar.SetNativeSize();

                    int[] monsters = new int[5] { arenaData100s[i].monster1Arena, arenaData100s[i].monster2Arena, arenaData100s[i].monster3Arena, arenaData100s[i].monster4Arena, arenaData100s[i].monster5Arena };
                    for (int j = 0; j < ap.monsterAvatar.Length; j++)
                    {
                        if (monsters[j] > 0)
                        {
                            ap.monsterAvatar[j].color = Color.white;
                            ap.monsterAvatar[j].sprite = gc.FindMonster(monsters[j]).avatar[1];
                            ap.monsterAvatar[j].SetNativeSize();
                        }
                        else
                        {
                            ap.monsterAvatar[j].color = new Color(0, 0, 0, 0);
                        }
                    }

                    RectTransform apTransform = ap.GetComponent<RectTransform>();
                    apTransform.anchoredPosition = new Vector2(0, -60 + (-110 * i));
                }
                else
                {
                    ArenaPlayerInfor ap = allArenaPlayerInfor[i];
                    if (i < 3)
                    {
                        ap.topRank.color = Color.white;
                        ap.topRank.sprite = spriteTopRank[i];
                        ap.topRank.SetNativeSize();
                        ap.txtRank.text = "";
                        ap.GetComponent<Image>().sprite = bGArenaPlayerInfo[i];
                    }
                    else
                    {
                        ap.topRank.color = new Color(0, 0, 0, 0);
                        ap.txtRank.text = (i + 1).ToString();
                        ap.GetComponent<Image>().sprite = bGArenaPlayerInfo[3];
                    }

                    ap.txtName.text = PlayerPrefs.GetString(StaticString.saveArenaName);
                    ap.txtPoint.text = PlayerPrefs.GetInt(StaticString.saveArenaPoint).ToString();
                    ap.playerAvatar.sprite = gc.FindMonster(PlayerPrefs.GetInt(StaticString.saveAvatar)).avatar[1];
                    ap.playerAvatar.SetNativeSize();

                    int[] monsters = new int[5] { PlayerPrefs.GetInt(StaticString.saveArenaMonster0), PlayerPrefs.GetInt(StaticString.saveArenaMonster1), PlayerPrefs.GetInt(StaticString.saveArenaMonster2), PlayerPrefs.GetInt(StaticString.saveArenaMonster3), PlayerPrefs.GetInt(StaticString.saveArenaMonster4) };
                    for (int j = 0; j < ap.monsterAvatar.Length; j++)
                    {
                        if (monsters[j] > 0)
                        {
                            ap.monsterAvatar[j].color = Color.white;
                            ap.monsterAvatar[j].sprite = gc.FindMonster(monsters[j]).avatar[1];
                            ap.monsterAvatar[j].SetNativeSize();
                        }
                        else
                        {
                            ap.monsterAvatar[j].color = new Color(0, 0, 0, 0);
                        }
                    }

                    RectTransform apTransform = ap.GetComponent<RectTransform>();
                    apTransform.anchoredPosition = new Vector2(0, -60 + (-110 * i));
                }
            }
        }
    }

    public void GetMainPlayerInfor()
    {
        playerMainAvatar.sprite = GameController.ins.FindMonster(PlayerPrefs.GetInt(StaticString.saveAvatar)).avatar[1];
        playerMainAvatar.SetNativeSize();

        if (PlayerPrefs.GetInt(StaticString.saveArenaRank) <= 3)
        {
            imgMainTopRank.color = Color.white;
            imgMainTopRank.sprite = spriteTopRank[PlayerPrefs.GetInt(StaticString.saveArenaRank) - 1];
            imgMainTopRank.SetNativeSize();
            txtMainRank.text = "";
        }
        else
        {
            imgMainTopRank.color = new Color(0, 0, 0, 0);
            txtMainRank.text = PlayerPrefs.GetInt(StaticString.saveArenaRank).ToString();
        }
        txtMainPoint.text = PlayerPrefs.GetInt(StaticString.saveArenaPoint).ToString();
        txtMainName.text = PlayerPrefs.GetString(StaticString.saveArenaName);

        string[] varSaveMonsterKey = new string[5] { StaticString.saveArenaMonster0, StaticString.saveArenaMonster1, StaticString.saveArenaMonster2, StaticString.saveArenaMonster3, StaticString.saveArenaMonster4 };

        for (int i = 0; i < 5; i++)
        {
            int saveMonsterID = PlayerPrefs.GetInt(varSaveMonsterKey[i]);
            if (saveMonsterID > 0 && CheckMonsterAvailable(saveMonsterID))
            {
                monsterMainAvatar[i].sprite = GameController.ins.FindMonster(saveMonsterID).avatar[1];
                monsterMainAvatar[i].SetNativeSize();
                monsterMainAvatar[i].color = Color.white;
            }
            else
            {
                monsterMainAvatar[i].color = new Color(0, 0, 0, 0);
                if (!CheckMonsterAvailable(saveMonsterID))
                {
                    PlayerPrefs.SetInt(varSaveMonsterKey[i], 0);
                }
            }
        }
    }

    public bool CheckMonsterAvailable(int IDMonster)
    {
        foreach (var b in UIManager.ins.monsterView.allBallButton)
        {
            if (b.IDMonster == IDMonster) return true;
        }
        return false;
    }

    public void IntroArena()
    {
        GameController gc = GameController.ins;
        PlayerController player = gc.player;
        UIManager.ins.sIngame.touchField.enabled = false;
        UIManager.ins.sIngame.btnBack.gameObject.SetActive(false);
        GameController.ins.isMoving = true;
        gc.UseSubCam();

        player.transform.DOMove(GameController.ins.currentMap.camPos[0].position, 2.5f).OnComplete(() =>
        {
            player.transform.DOMove(GameController.ins.currentMap.camPos[0].position, 1f).OnComplete(() =>
            {
                AudioManager.ins.PlayOther("Boss2In" + UnityEngine.Random.Range(0, 2));
                gc.currentBoss.animator.Play(gc.currentBoss.clipAction.name);
                player.transform.DOMove(GameController.ins.currentMap.camPos[1].position, 1f).OnComplete(() =>
                {
                    player.transform.DOMove(GameController.ins.currentMap.camPos[1].position, 1f).OnComplete(() =>
                    {
                        player.transform.DOMove(gc.currentMap.playerPos[0].position, 3.5f).OnComplete(() =>
                        {
                            gc.UseMainCam();
                            gc.isPlaying = true;
                            gc.isMoving = false;
                            gc.currentBoss.bossRound++;
                            UIManager.ins.sIngame.touchField.enabled = true;
                            UIManager.ins.sIngame.btnBack.gameObject.SetActive(true);

                        });
                    });
                });
            });
        });
    }

    public void BtnBattle()
    {
        if (CheckBattle())
        {
            AudioManager.ins.PlayOther("Click");

            int mapType = GameController.ins.ReadInt(GameController.ins.txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14);
            string levelType = "";
            switch (mapType)
            {
                case 0:
                    levelType = "Normal";
                    break;
                case 1:
                    levelType = "Boss";
                    break;
                case 2:
                    levelType = "SuperBoss";
                    break;
            }

            GameController.ins.timeStartLevel = Time.time;
            GameController.ins.gameMode = "Arena";
            GameController.ins.levelType = levelType;
            GameController.ins.levelNumber = PlayerPrefs.GetInt(StaticString.saveLevel);

            LevelStart.CustomParameters(new Dictionary<string, object>
            {
                {LevelStartParameters.game_mode , GameController.ins.gameMode},
                {LevelStartParameters.level_type , GameController.ins.levelType},
                {LevelStartParameters.level_number , GameController.ins.levelNumber},
            });
            SendEventManager.ins.SendEventBuffer(LevelStart.GetEventAppmetrica());

            UIManager.ins.ShowPopup(Popup.PArenaLoading);
        }
        else
        {
            BtnChangeInfo();
        }
    }

    private bool CheckBattle()
    {
        string[] varSaveMonsterKey = new string[5] { StaticString.saveArenaMonster0, StaticString.saveArenaMonster1, StaticString.saveArenaMonster2, StaticString.saveArenaMonster3, StaticString.saveArenaMonster4 };

        for (int i = 0; i < 5; i++)
        {
            int saveMonsterID = PlayerPrefs.GetInt(varSaveMonsterKey[i]);
            if (saveMonsterID > 0 && CheckMonsterAvailable(saveMonsterID))
            {
                return true;
            }
        }
        return false;
    }

    public void BtnChangeInfo()
    {
        AudioManager.ins.PlayOther("Click");
        UIManager.ins.ShowPopup(Popup.PChangeInfo);
    }

    public void BtnBack()
    {
        AudioManager.ins.PlayOther("Click");

        imgBG.DOAnchorPos(new Vector2(0, 100), 0.15f).OnComplete(() =>
        {
            imgBG.DOAnchorPos(new Vector2(0, -2000), 0.4f).OnComplete(() =>
            {
                UIManager.ins.ShowScreen(ScreenX.SHome);
            });
        });
    }
}
