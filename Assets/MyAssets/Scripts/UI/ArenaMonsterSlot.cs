using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArenaMonsterSlot : MonoBehaviour
{
    public int IDMonster;
    public Image imgAvatarMonster;
    public Button btnUnchoose;
    public BtnChoseMonster btnOfThisSlot;

    public void BtnUnchoose()
    {
        IDMonster = -1;
        btnUnchoose.gameObject.SetActive(false);
        imgAvatarMonster.color = new Color(0, 0, 0, 0);
    }
}
