using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlovesItem : MonoBehaviour
{
    private int ID;
    public Image thisAvatar;
    public Button btnEquip;
    public Sprite[] btnEquipSprite;

    public void OnSpawn(int _ID)
    {
        ID = _ID;
        float thisGlovesStatus = GameController.ins.LoadGloves()[ID];

        thisAvatar.sprite = GameController.ins.allGlovesAvatar[ID];
        thisAvatar.SetNativeSize();

        if (thisGlovesStatus >= 2f)
        {
            thisAvatar.color = Color.white;
            btnEquip.image.sprite = btnEquipSprite[1];
            btnEquip.GetComponentInChildren<Text>().text = "USE";
            btnEquip.interactable = true;
        }
        else if (thisGlovesStatus >= 1f)
        {
            if (PlayerPrefs.GetInt(ManagerAds.Save_Remove_RewardAds) == 1)
            {
                GameController.ins.SaveGloves(ID, thisGlovesStatus + 1.2f);
                thisAvatar.color = Color.white;
                btnEquip.image.sprite = btnEquipSprite[1];
                btnEquip.GetComponentInChildren<Text>().text = "USE";
                btnEquip.interactable = true;
            }
            else
            {
                thisAvatar.color = Color.white;
                btnEquip.image.sprite = btnEquipSprite[2];
                btnEquip.GetComponentInChildren<Text>().text = "";
                btnEquip.interactable = true;
            }
        }
        else
        {
            thisAvatar.color = Color.black;
            btnEquip.image.sprite = btnEquipSprite[0];
            btnEquip.GetComponentInChildren<Text>().text = "USE";
            btnEquip.interactable = false;
        }

        if (ID == PlayerPrefs.GetInt(StaticString.saveUsingGloves))
        {
            thisAvatar.color = Color.white;
            btnEquip.image.sprite = btnEquipSprite[3];
            btnEquip.GetComponentInChildren<Text>().text = "USED";
            btnEquip.interactable = true;
        }
    }

    public void BtnEquip()
    {
        AudioManager.ins.PlayOther("Click");
        float thisGlovesStatus = GameController.ins.LoadGloves()[ID];
        GameController gc = GameController.ins;
        GameObject oldHand = gc.player.handEvents.gameObject;
        GlovesItem oldGlovesItem = UIManager.ins.sGloves.glovesItemsSpawned[PlayerPrefs.GetInt(StaticString.saveUsingGloves)];

        if (thisGlovesStatus >= 1f && thisGlovesStatus < 2f)
        {
            ManagerAds.ins.ShowRewarded((s) =>
             {
                 if (!s) return;

                 gc.SaveGloves(ID, thisGlovesStatus + 1.2f);

                 BtnEquip();
                 return;
             }, "ClaimGlovesInShop");
            return;
        }

        oldGlovesItem.btnEquip.image.sprite = btnEquipSprite[1];
        oldGlovesItem.btnEquip.GetComponentInChildren<Text>().text = "USE";
        oldGlovesItem.btnEquip.interactable = true;

        thisAvatar.color = Color.white;
        btnEquip.image.sprite = btnEquipSprite[3];
        btnEquip.GetComponentInChildren<Text>().text = "USED";
        btnEquip.interactable = true;

        PlayerPrefs.SetInt(StaticString.saveUsingGloves, ID);
        gc.player.Setup();

        Destroy(oldHand);
    }
}
