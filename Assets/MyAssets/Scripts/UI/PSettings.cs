
/* Copyright © @ThanhDV */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PSettings : UIController
{
    public RectTransform imgBGSetting;
    public Sprite spriteBtnOn;
    public Sprite spriteBtnOff;
    public Button btnMusic;
    public Button btnSound;
    public Button btnVibration;
    // public GameObject btnLogin;

    public override void Show(bool value)
    {
        base.Show(value);
        if (value)
        {
            LoadSettings();
            imgBGSetting.anchoredPosition = new Vector2(0, 1250);

            imgBGSetting.DOAnchorPos(new Vector2(0, -100), 0.3f).OnComplete(() =>
            {
                imgBGSetting.DOAnchorPos(new Vector2(0, 0), 0.15f);
            });
        }
    }

    public void BtnPrivacyPolicy()
    {
        ManagerAds.ins.ShowPrivacyPolicy();
    }

    private void LoadSettings()
    {
        if (PlayerPrefs.GetInt(StaticString.saveMusic) == 0)
        {
            btnMusic.image.sprite = spriteBtnOff;
        }
        else
        {
            btnMusic.image.sprite = spriteBtnOn;
        }

        if (PlayerPrefs.GetInt(StaticString.saveSound) == 0)
        {
            btnSound.image.sprite = spriteBtnOff;
        }
        else
        {
            btnSound.image.sprite = spriteBtnOn;
        }

        if (PlayerPrefs.GetInt(StaticString.saveVibration) == 0)
        {
            btnVibration.image.sprite = spriteBtnOff;
        }
        else
        {
            btnVibration.image.sprite = spriteBtnOn;
        }

        // btnLogin.SetActive(!Facebook.Unity.FB.IsLoggedIn);
    }

    public void BtnQuit()
    {
        AudioManager.ins.PlayOther("Click");
        imgBGSetting.DOAnchorPos(new Vector2(0, -100), 0.15f).OnComplete(() =>
        {
            imgBGSetting.DOAnchorPos(new Vector2(0, 1250), 0.3f).OnComplete(() =>
            {
                UIManager.ins.ShowPopup(Popup.None);
            });
        });
    }

    public void BtnMusic()
    {
        AudioManager.ins.PlayOther("Click");
        if (PlayerPrefs.GetInt(StaticString.saveMusic) == 0)
        {
            btnMusic.image.sprite = spriteBtnOn;
            PlayerPrefs.SetInt(StaticString.saveMusic, 1);
            AudioManager.ins.PlayBackGround("BG1");
        }
        else
        {
            btnMusic.image.sprite = spriteBtnOff;
            PlayerPrefs.SetInt(StaticString.saveMusic, 0);
            AudioManager.ins.StopBackGround();
        }
    }

    public void BtnSound()
    {
        AudioManager.ins.PlayOther("Click");
        if (PlayerPrefs.GetInt(StaticString.saveSound) == 0)
        {
            btnSound.image.sprite = spriteBtnOn;
            PlayerPrefs.SetInt(StaticString.saveSound, 1);
        }
        else
        {
            btnSound.image.sprite = spriteBtnOff;
            PlayerPrefs.SetInt(StaticString.saveSound, 0);
        }
    }

    public void BtnVibration()
    {
        AudioManager.ins.PlayOther("Click");
        GameController.ins.Vibrate();
        if (PlayerPrefs.GetInt(StaticString.saveVibration) == 0)
        {
            btnVibration.image.sprite = spriteBtnOn;
            PlayerPrefs.SetInt(StaticString.saveVibration, 1);
        }
        else
        {
            btnVibration.image.sprite = spriteBtnOff;
            PlayerPrefs.SetInt(StaticString.saveVibration, 0);
        }
    }
}
