using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionReward : MonoBehaviour
{
    private ClaimMode claimMode;
    private int rewardPoint;
    private int currentPoint;
    public Button thisBtn;
    public Image thisImg;
    public Text txtClaimStatus;
    public Sprite spriteBox;
    public int thisID;
    private int IDMonsterReward;


    public void OnSpawn(ClaimMode _claimMode, Vector2 anchoredPosition, Vector3 pos, int _rewardPoint, int _currentPoint, bool status, int _ID, int _IDMonsterReward = -1)
    {
        claimMode = _claimMode;
        currentPoint = _currentPoint;
        rewardPoint = _rewardPoint;
        thisID = _ID;
        IDMonsterReward = _IDMonsterReward;

        if (_currentPoint < _rewardPoint || !status)
        {
            thisBtn.interactable = false;
        }
        else
        {
            thisBtn.interactable = true;
        }


        if (claimMode == ClaimMode.claimSlot)
        {
            thisImg.sprite = spriteBox;
            thisImg.SetNativeSize();
            if (!status)
            {
                txtClaimStatus.text = "Claimed";
                txtClaimStatus.color = Color.red;
                txtClaimStatus.fontSize = 25;
            }
            else
            {
                txtClaimStatus.text = "";
                txtClaimStatus.color = Color.white;
                txtClaimStatus.fontSize = 40;
            }
        }
        else if (claimMode == ClaimMode.claimMonster)
        {

            thisImg.sprite = GameController.ins.FindMonster(_IDMonsterReward).avatar[1];
            thisImg.color = Color.black;
            thisImg.SetNativeSize();
            if (!status)
            {
                txtClaimStatus.text = "Claimed";
                txtClaimStatus.color = Color.red;
                txtClaimStatus.fontSize = 25;
            }
            else
            {
                txtClaimStatus.text = "???";
                txtClaimStatus.color = Color.white;
                txtClaimStatus.fontSize = 40;
            }
        }

        RectTransform thisRect = GetComponent<RectTransform>();
        thisRect.anchoredPosition = anchoredPosition;
        thisRect.localPosition = pos;
    }

    public void Claim()
    {
        AudioManager.ins.PlayOther("Win");
        if (claimMode == ClaimMode.claimSlot)
        {
            UIManager.ins.sCollection.ClaimMonsterBall();
            thisBtn.interactable = false;
            GameController.ins.SaveCollectionReward(thisID, false);
            txtClaimStatus.text = "Claimed";
            return;
        }

        if (claimMode == ClaimMode.claimMonster)
        {
            UIManager.ins.sCollection.ClaimMonster(IDMonsterReward);
            thisBtn.interactable = false;
            GameController.ins.SaveCollectionReward(thisID, false);
            txtClaimStatus.text = "Claimed";
            return;
        }
    }
}

public enum ClaimMode
{
    claimSlot,
    claimMonster
}
