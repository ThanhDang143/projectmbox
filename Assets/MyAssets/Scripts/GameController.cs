
/* Copyright © @ThanhDV */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.IO;
using System;
using UnityEditor;
using Pathfinding;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
public class GameController : MonoBehaviour
{
    public static GameController ins;
    public TextAsset txtLevelScript;
    public TextAsset txtMonsterInfo;
    public TextAsset txtArenaTop100;
    public Transform mainCam;
    public PlayerController player;
    public AstarPath astarPath;
    public List<Transform> listBallSpawned;
    public List<PlayerMonster> listPlayerMonster;
    public List<EnemyMonster> listEnemyMonster;
    public MonsterInfo[] allMonster;
    public Transform[] monsterBall;

    [Header("Map")]
    public MapController evolveMaps;
    public MapController exchangeMaps;
    public MapController[] allNormalMaps;
    public MapController[] allBossMaps;
    public MapController[] allArenaMaps;

    [Header("Skybox")]
    public Light mainLight;
    public Light fillLight;
    public Material[] skyDay;
    public Material[] skyNight;
    public GameObject sea;

    [Header("Boss")]
    public BossController exchangeBoss;
    [HideInInspector] public BossController currentBoss;
    public BossController[] allBoss;

    [Header("Gloves")]
    public HandEvents[] allGloves;
    public Sprite[] allGlovesAvatar;
    public Sprite[] allGlovesBg;

    [Header("Debug")]
    public GameObject[] rootMonstersFBX;
    public UIHealthBar[] healthBars;
    [HideInInspector] public bool isPlaying;
    [HideInInspector] public bool isLosing;
    [HideInInspector] public bool isEvolving;
    [HideInInspector] public bool isMoving;
    [HideInInspector] public bool isCatching;
    [HideInInspector] public bool isInSpecialWorld;
    [HideInInspector] public bool isExchangeMap;
    [HideInInspector] public bool isSpin;
    [HideInInspector] public bool isChoosingBuff;
    [HideInInspector] public bool winChecked;
    [HideInInspector] public bool isInArena;
    private MapController preMap;
    [SerializeField] private Sprite[] allAvatar;
    [HideInInspector] public MapController currentMap;
    [HideInInspector] public int chosenMonster;
    [HideInInspector] public BtnChoseMonster currentBtnChoseMonster;
    public Sprite whiteHealthBar;
    private AssetBundle monsterAssetBundle;

    public Transform[] tempFX;

    [Header("ReportData")]
    public float timeStartLevel;
    public string gameMode;
    public string levelType;
    public int levelNumber;
    private bool appOpenAdsShowed;

    private void Awake()
    {
        Application.targetFrameRate = 10000;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        ins = this;

        // allMonster = Loading.ins.tempMonsterInfo;

        isPlaying = false;
        isLosing = false;
        isEvolving = false;
        isMoving = false;
        isInSpecialWorld = false;
        isExchangeMap = false;
        isSpin = false;
        isChoosingBuff = false;
        winChecked = false;
        isInArena = false;
        appOpenAdsShowed = false;
        chosenMonster = -1;

        player.Setup();

        InitializationSave();
    }

    private void Start()
    {
        if (PlayerPrefs.GetInt(StaticString.checkFirstTimePlay) == 1 && LoadMonsterCollection().Count <= 0)
        {
            LoadTutorial();
        }
        else
        {
            LoadLevel();
        }
        LoadMonsterView();
        AudioManager.ins.PlayBackGround("BG1");

        if (!appOpenAdsShowed)
        {
            ManagerAds.ins.ShowAppOpen();
            appOpenAdsShowed = true;
        }

        // for (int i = 0; i < 100; i++)
        // {
        //     SpawnEnemyMonster(FindMonster(i + 1), new Vector3(800 - (i * 5), 1, 100), Quaternion.identity);
        //     SpawnEnemyMonster(FindMonster(i + 101), new Vector3(800 - (i * 5), 1, 105f), Quaternion.identity);
        // }

    }

    private void Update()
    {
        if (listEnemyMonster.Count > 0)
        {
            if (isEvolving) return;

            foreach (EnemyMonster e in listEnemyMonster)
            {
                e.ManualUpdate();
            }
        }

        if (listPlayerMonster.Count > 0)
        {
            if (isEvolving) return;

            foreach (PlayerMonster p in listPlayerMonster)
            {
                p.ManualUpdate();
            }
        }

        // if (Input.GetKeyDown(KeyCode.Home))
        // {
        //     ReadMonsterInfo();
        //     Debug.Log("Read and Write monster info");
        // }
    }

    public void UpdateAStar()
    {
        astarPath.data.recastGraph.forcedBoundsCenter = currentMap.transform.position;
        astarPath.Scan();
    }

    public void ChangeTime(bool isDay = true)
    {
        //LoadTime
        if (isDay) // Day
        {
            sea.SetActive(true);
            RenderSettings.skybox = skyDay[UnityEngine.Random.Range(0, skyDay.Length)];
            RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Flat;
            mainLight.intensity = 0.45f;
            fillLight.intensity = 0.45f;
        }
        else // Night
        {
            sea.SetActive(false);
            RenderSettings.skybox = skyNight[UnityEngine.Random.Range(0, skyNight.Length)];
            RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Flat;
            mainLight.intensity = 0.25f;
            fillLight.intensity = 0f;
        }

    }

    public void ResetAnimTrigger()
    {
        player.animator.ResetTrigger("GetBall");
        player.animator.ResetTrigger("ThrowMons");
        player.animator.ResetTrigger("StartCatching");
        player.animator.ResetTrigger("Catching");
        player.animator.ResetTrigger("CatchSuccessful");
        player.animator.ResetTrigger("CatchCancel");
        player.animator.ResetTrigger("AutoThrow");
    }

    public void ResetMonsterBallBoss(bool resetBoss = true)
    {

        if (listEnemyMonster.Count > 0)
        {
            foreach (EnemyMonster e in listEnemyMonster)
            {
                e.OnDespawn(false);
            }
        }
        listEnemyMonster.Clear();

        if (listPlayerMonster.Count > 0)
        {
            foreach (PlayerMonster p in listPlayerMonster)
            {
                p.OnDespawn(false);
            }
        }
        listPlayerMonster.Clear();

        if (listBallSpawned.Count > 0)
        {
            foreach (Transform p in listBallSpawned)
            {
                if (p != null)
                {
                    p.GetComponent<MonsterBall>().OnDespawn();
                }
            }
        }
        listBallSpawned.Clear();

        PoolVFX.pool.DespawnAll();

        if (currentBoss && resetBoss)
        {
            Destroy(currentBoss.gameObject);
        }
    }

    public void Lose()
    {
        isPlaying = false;
        isLosing = true;

        try
        {
            player.DisableLaser();
        }
        catch (System.Exception)
        {

        }

        ResetMonsterBallBoss();

        player.animator.Play(player.clipLose.name);
        Destroy(player.handEvents.newBall);
        Destroy(player.handEvents.monsterOnHand);

        ManagerAds.ins.ShowInterstitial(null, "Lose");
        UIManager.ins.ShowScreen(ScreenX.SLose);

        if (isInArena) return;
        player.transform.DOLocalRotate(new Vector3(-90, 0, 0), 0.5f);
        player.transMainCam.DOLocalRotate(Vector3.zero, 0.5f);
    }

    public void CheckWin()
    {
        StartCoroutine(IECheckWin());
    }

    public IEnumerator IECheckWin()
    {
        if (isLosing || isEvolving || !isPlaying || winChecked) yield break;

        winChecked = true;
        yield return new WaitForSeconds(0.75f);
        if (isInSpecialWorld)
        {
            ActionWinWorldMap();
            winChecked = false;
            yield break;
        }

        if (isInArena)
        {
            ActionWinArena();
            winChecked = false;
            yield break;
        }

        if (ReadInt(txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14) == 0) // If this map is Normal Map
        {
            ActionWinNormal();
        }
        else
        {
            ActionWinSpecial();
        }
        winChecked = false;
    }

    public void UseMainCam()
    {
        player.vSubCam.enabled = false;
        player.vMainCam.enabled = true;
        if (player.handRenderer == null)
        {
            player.handRenderer = player.handEvents.handRenderer;
        }
        player.handRenderer.enabled = true;
        UIManager.ins.sIngame.crossHair.enabled = true;
    }

    public void UseSubCam()
    {
        player.vSubCam.LookAt = currentBoss.transform;
        player.vSubCam.enabled = true;
        player.vMainCam.enabled = false;
        player.handRenderer.enabled = false;
        UIManager.ins.sIngame.crossHair.enabled = false;
    }

    private void ActionWinArena()
    {
        foreach (Transform p in listBallSpawned)
        {
            if (p != null)
            {
                p.GetComponent<MonsterBall>().OnDespawn();
            }
        }
        listBallSpawned.Clear();

        if (listEnemyMonster.Count > 0) return;

        isPlaying = false;
        isLosing = false;
        isMoving = true;
        UseSubCam();
        UIManager.ins.sIngame.touchField.enabled = false;
        UIManager.ins.sIngame.btnBack.gameObject.SetActive(false);

        foreach (PlayerMonster p in listPlayerMonster)
        {
            p.OnDespawn();
        }
        listPlayerMonster.Clear();

        player.transform.DOMove(currentMap.camPos[1].position, 2f).OnComplete(() =>
        {
            currentBoss.animator.Play(currentBoss.clipLose.name);

            AudioManager.ins.PlayOther("Boss2Out");

            player.transform.DOMove(currentMap.camPos[1].position, 1f).OnComplete(() =>
            {
                player.transform.DOMove(currentMap.camPos[1].position, 1f).OnComplete(() =>
                {
                    ResetAnimTrigger();
                    UseMainCam();
                    isMoving = false;
                    ManagerAds.ins.ShowInterstitial(null, "WinArena");
                    UIManager.ins.ShowScreen(ScreenX.SWin);
                    UIManager.ins.sIngame.touchField.enabled = true;
                    UIManager.ins.sIngame.btnBack.gameObject.SetActive(true);
                });
            });
        });
    }

    private void ActionWinSpecial()
    {
        int readMapType = ReadInt(txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14);
        foreach (Transform p in listBallSpawned)
        {
            if (p != null)
            {
                p.GetComponent<MonsterBall>().OnDespawn();
            }
        }
        listBallSpawned.Clear();

        if (readMapType == 1 || readMapType == 2) // if current map is Special Map
        {
            if (listEnemyMonster.Count > 0) return;

            MonsterView mv = UIManager.ins.monsterView;

            for (int i = 1; i < mv.allBallButton.Count; i++)
            {
                if (mv.allBallButton[i].thisBtn.image.color != Color.red && mv.allBallButton[i].IDMonster != -1)
                {
                    mv.allBallButton[i].ChooseMonster();
                }
            }

            if (currentBoss.bossRound * 100 >= currentBoss.maxRound) // Check turn 2 of boss map
            {
                isPlaying = false;
                isLosing = false;
                isMoving = true;
                UseSubCam();
                UIManager.ins.sIngame.touchField.enabled = false;
                UIManager.ins.sIngame.btnBack.gameObject.SetActive(false);
                player.transform.DOMove(currentMap.camPos[1].position, 2f).OnComplete(() =>
                {
                    for (int i = 1; i < mv.allBallButton.Count; i++)
                    {
                        if (mv.allBallButton[i].thisBtn.image.color != Color.red && mv.allBallButton[i].IDMonster != -1)
                        {
                            mv.allBallButton[i].ChooseMonster();
                        }
                    }
                    currentBoss.animator.Play(currentBoss.clipLose.name);
                    if (readMapType == 2)
                    {
                        AudioManager.ins.PlayOther("Boss2Out");
                    }
                    else
                    {
                        AudioManager.ins.PlayOther("Boss1Out");
                    }
                    player.transform.DOMove(currentMap.camPos[1].position, 1f).OnComplete(() =>
                    {
                        player.transform.DOMove(currentMap.camPos[1].position, 1f).OnComplete(() =>
                        {
                            ResetAnimTrigger();
                            UseMainCam();
                            isMoving = false;
                            ManagerAds.ins.ShowInterstitial(null, "WinBoss");
                            UIManager.ins.ShowScreen(ScreenX.SWin);
                            UIManager.ins.sIngame.touchField.enabled = true;
                            UIManager.ins.sIngame.btnBack.gameObject.SetActive(true);
                        });
                    });
                });
            }
            else
            {
                isPlaying = false;
                isMoving = true;
                UseSubCam();
                UIManager.ins.sIngame.touchField.enabled = false;
                UIManager.ins.sIngame.btnBack.gameObject.SetActive(false);
                player.transform.DOMove(currentMap.camPos[2].position, 2).OnComplete(() =>
                {
                    for (int i = 1; i < mv.allBallButton.Count; i++)
                    {
                        if (mv.allBallButton[i].thisBtn.image.color != Color.red && mv.allBallButton[i].IDMonster != -1)
                        {
                            mv.allBallButton[i].ChooseMonster();
                        }
                    }
                    player.transform.DOMove(currentMap.camPos[2].position, 1).OnComplete(() =>
                    {
                        currentBoss.animator.Play(currentBoss.clipThrow.name);

                        player.transform.DOMove(currentMap.playerPos[0].position, 3f).OnComplete(() =>
                        {
                            UseMainCam();
                            ResetAnimTrigger();
                            isPlaying = true;
                            isMoving = false;
                            player.animator.Play(player.clipIdle1.name);
                            player.DisableLaser();
                            UIManager.ins.sIngame.touchField.enabled = true;
                            UIManager.ins.sIngame.btnBack.gameObject.SetActive(true);
                            currentBoss.bossRound++;
                            foreach (BtnChoseMonster btn in UIManager.ins.monsterView.allBallButton)
                            {
                                btn.thisBtn.interactable = true;
                            }

                            if (ReadInt(txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14) == 2)
                            {
                                player.animator.Play(player.clipIdle1.name);
                                player.animator.SetTrigger("AutoThrow");
                                return;
                            }

                            UIManager.ins.monsterView.allBallButton[1].ChooseMonster();
                        });
                    });
                });
            }

            return;
        }
    }

    public void AutoThrowMonster()
    {
        if (!isInArena)
        {
            MonsterView mv = UIManager.ins.monsterView;
            int maxMons = 0;
            for (int i = 1; i < mv.allBallButton.Count; i++)
            {
                if (mv.allBallButton[i].IDMonster < 1 || mv.allBallButton[i].thisText.text == "X") { }
                else
                {
                    if (FindMonster(mv.allBallButton[i].IDMonster).IDMonsterEvolve < 1)
                    {
                        Transform b = PoolMonsterBall.pool.Spawn(monsterBall[3], player.attackPos.position, Quaternion.identity);
                        b.GetComponent<PlayerBallAuto>().OnSpawn(FindMonster(mv.allBallButton[i].IDMonster), currentMap.playerMonsterPos[maxMons].position, mv.allBallButton[i]);
                    }
                    else
                    {
                        Transform b = PoolMonsterBall.pool.Spawn(monsterBall[2], player.attackPos.position, Quaternion.identity);
                        b.GetComponent<PlayerBallAuto>().OnSpawn(FindMonster(mv.allBallButton[i].IDMonster), currentMap.playerMonsterPos[maxMons].position, mv.allBallButton[i]);
                    }
                    maxMons++;
                }

                if (maxMons >= 5)
                {
                    return;
                }
            }
        }
        else
        {
            string[] varSaveMonsterKey = new string[5] { StaticString.saveArenaMonster0, StaticString.saveArenaMonster1, StaticString.saveArenaMonster2, StaticString.saveArenaMonster3, StaticString.saveArenaMonster4 };

            for (int i = 0; i < 5; i++)
            {
                int saveMonsterID = PlayerPrefs.GetInt(varSaveMonsterKey[i]);
                if (saveMonsterID > 0)
                {
                    if (FindMonster(saveMonsterID).IDMonsterEvolve < 1)
                    {
                        Transform b = PoolMonsterBall.pool.Spawn(monsterBall[3], player.attackPos.position, Quaternion.identity);
                        b.GetComponent<PlayerBallAuto>().OnSpawn(FindMonster(saveMonsterID), currentMap.playerMonsterPos[i].position);
                    }
                    else
                    {
                        Transform b = PoolMonsterBall.pool.Spawn(monsterBall[2], player.attackPos.position, Quaternion.identity);
                        b.GetComponent<PlayerBallAuto>().OnSpawn(FindMonster(saveMonsterID), currentMap.playerMonsterPos[i].position);
                    }
                }
            }
        }
    }

    private void ActionWinWorldMap()
    {
        if (listEnemyMonster.Count > 0) return;

        player.DisableLaser();
        player.animator.Play(player.clipWin.name);
        Destroy(player.handEvents.newBall);
        Destroy(player.handEvents.monsterOnHand);

        transform.DOScale(transform.localScale, player.clipWin.length).OnComplete(() =>
        {
            isPlaying = false;
            isLosing = false;
            // isInSpecialWorld = false;
            ManagerAds.ins.ShowInterstitial(null, "WinWorldMap");
            UIManager.ins.ShowScreen(ScreenX.SWin);
        });
    }

    private void ActionWinNormal()
    {
        if (listEnemyMonster.Count <= 0)
        {
            MonsterView mv = UIManager.ins.monsterView;

            int r = UnityEngine.Random.Range(0, 5);

            if (PlayerPrefs.GetInt(StaticString.checkFirstTimePlay) == 1)
            {
                r = 1;
            }

            if (r != 0)
            {
                player.DisableLaser();
                player.animator.Play(player.clipWin.name);
                Destroy(player.handEvents.newBall);
                Destroy(player.handEvents.monsterOnHand);

                transform.DOScale(transform.localScale, player.clipWin.length).OnComplete(() =>
                {
                    isPlaying = false;
                    isLosing = false;
                    ManagerAds.ins.ShowInterstitial(null, "WinNormal");
                    UIManager.ins.ShowScreen(ScreenX.SWin);
                });
                return;
            }

            int mapIndex = UnityEngine.Random.Range(0, allNormalMaps.Length);
            preMap = currentMap;
            Vector3 currentMapPos = currentMap.transform.position;
            Vector3 newMapPos = currentMapPos + player.transform.forward * UnityEngine.Random.Range(40, 50) + player.transform.up * UnityEngine.Random.Range(20, 30);
            currentMap = PoolMap.pool.Spawn(allNormalMaps[mapIndex].transform, newMapPos, Quaternion.identity).GetComponent<MapController>();

            int totalMonster = UnityEngine.Random.Range(1, 4);
            int readRateLv2 = ReadInt(txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 13);
            int countMonsterLv2 = 0;

            List<int> kindOfMonster = GetKindOfMonster();

            for (int i = 0; i < totalMonster; i++)
            {
                int ranMonster = kindOfMonster[UnityEngine.Random.Range(0, kindOfMonster.Count)];

                if (countMonsterLv2 < readRateLv2) // chose monster lv1 or lv2
                {
                    int idEvolve = FindMonster(ranMonster).IDMonsterEvolve;

                    if (idEvolve > 0)
                    {
                        MonsterInfo monsterLv2 = FindMonster(idEvolve);
                        SpawnEnemyMonster(monsterLv2, currentMap.enemiesPos[i].position, Quaternion.identity);
                        countMonsterLv2++;
                    }
                    else
                    {
                        SpawnEnemyMonster(FindMonster(ranMonster), currentMap.enemiesPos[i].position, Quaternion.identity);
                    }
                }
                else
                {
                    SpawnEnemyMonster(FindMonster(ranMonster), currentMap.enemiesPos[i].position, Quaternion.identity);
                }
            }


            if (listPlayerMonster.Count > 0)
            {
                foreach (PlayerMonster pm in listPlayerMonster)
                {
                    if (pm.btnControllThisMonster != null)
                    {
                        pm.btnControllThisMonster.ChooseMonster();
                    }
                    else
                    {
                        pm.OnDespawn(false);
                    }
                }
            }

            isMoving = true;
            transform.DOScale(Vector3.one, 0.5f).OnComplete(() => // Just Wait 0.5s
            {
                UIManager.ins.sIngame.touchField.enabled = false;
                UIManager.ins.sIngame.btnBack.gameObject.SetActive(false);
                player.animator.Play(player.clipIdle1.name);
                player.DisableLaser();
                AudioManager.ins.PlayOther("Jump");
                player.transform.DOJump(currentMap.playerPos[0].position, 5, 1, 2).OnComplete(() =>
                {
                    ResetAnimTrigger();
                    if (listPlayerMonster.Count > 0)
                    {
                        foreach (PlayerMonster p in listPlayerMonster)
                        {
                            p.OnDespawn(false);
                        }
                    }
                    UIManager.ins.sIngame.touchField.enabled = true;
                    UIManager.ins.sIngame.btnBack.gameObject.SetActive(true);
                    PoolMap.pool.Despawn(preMap.transform);
                    UpdateAStar();
                    isMoving = false;
                    for (int i = 1; i < mv.allBallButton.Count; i++)
                    {
                        if (mv.allBallButton[i].thisBtn.image.color != Color.red && mv.allBallButton[i].IDMonster != -1)
                        {
                            mv.allBallButton[i].ChooseMonster();
                        }
                    }
                    UIManager.ins.monsterView.allBallButton[1].ChooseMonster();
                });
            });
        }
    }

    private void InitializationSave()
    {
        if (!PlayerPrefs.HasKey(StaticString.saveLevel))
        {
            PlayerPrefs.SetInt(StaticString.saveLevel, 1);
        }

        if (!PlayerPrefs.HasKey(StaticString.savePreLevel))
        {
            PlayerPrefs.SetInt(StaticString.savePreLevel, 0);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveLevelLoop))
        {
            PlayerPrefs.SetInt(StaticString.saveLevelLoop, 0);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveMusic))
        {
            PlayerPrefs.SetInt(StaticString.saveMusic, 1);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveSound))
        {
            PlayerPrefs.SetInt(StaticString.saveSound, 1);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveVibration))
        {
            PlayerPrefs.SetInt(StaticString.saveVibration, 0);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveBuff))
        {
            PlayerPrefs.SetInt(StaticString.saveBuff, -1);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveTicket))
        {
            PlayerPrefs.SetInt(StaticString.saveTicket, 3);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveCollectionRewardClaimed))
        {
            PlayerPrefs.SetInt(StaticString.saveCollectionRewardClaimed, 0);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveUsingGloves))
        {
            PlayerPrefs.SetInt(StaticString.saveUsingGloves, 0);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveWatchVideoMonsterBox))
        {
            PlayerPrefs.SetInt(StaticString.saveWatchVideoMonsterBox, 0);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveIAPNormalRemoveAds))
        {
            PlayerPrefs.SetInt(StaticString.saveIAPNormalRemoveAds, 0);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveIAPPremiumRemoveAds))
        {
            PlayerPrefs.SetInt(StaticString.saveIAPPremiumRemoveAds, 0);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveIAPWorldTicket))
        {
            PlayerPrefs.SetInt(StaticString.saveIAPWorldTicket, 0);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveIAPGloves))
        {
            PlayerPrefs.SetInt(StaticString.saveIAPGloves, 0);
        }

        if (!PlayerPrefs.HasKey(StaticString.checkFirstTimePlay))
        {
            PlayerPrefs.SetInt(StaticString.checkFirstTimePlay, 1);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveNewIAPMonsterUnlock))
        {
            PlayerPrefs.SetInt(StaticString.saveNewIAPMonsterUnlock, 0);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveTimeLastDailyClaim))
        {
            PlayerPrefs.SetString(StaticString.saveTimeLastDailyClaim, DateTime.Now.AddDays(-1).ToBinary().ToString());
        }

        if (!PlayerPrefs.HasKey(StaticString.saveDailyClaimed))
        {
            PlayerPrefs.SetInt(StaticString.saveDailyClaimed, 0);
        }

        InitializationArena();
        ChooseRewardGloves();
    }

    private void InitializationArena()
    {
        if (!PlayerPrefs.HasKey(StaticString.saveAvatar))
        {
            PlayerPrefs.SetInt(StaticString.saveAvatar, 1);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveUpRankPoint))
        {
            PlayerPrefs.SetInt(StaticString.saveUpRankPoint, 0);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveArenaRank))
        {
            PlayerPrefs.SetInt(StaticString.saveArenaRank, UnityEngine.Random.Range(1403, 1998));
        }

        if (!PlayerPrefs.HasKey(StaticString.saveArenaPoint))
        {
            PlayerPrefs.SetInt(StaticString.saveArenaPoint, UnityEngine.Random.Range(1250, 1750));
        }

        if (!PlayerPrefs.HasKey(StaticString.saveArenaName))
        {
            PlayerPrefs.SetString(StaticString.saveArenaName, "Player " + PlayerPrefs.GetInt(StaticString.saveArenaRank));
        }

        if (!PlayerPrefs.HasKey(StaticString.saveArenaMonster0))
        {
            PlayerPrefs.SetInt(StaticString.saveArenaMonster0, -1);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveArenaMonster1))
        {
            PlayerPrefs.SetInt(StaticString.saveArenaMonster1, -1);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveArenaMonster2))
        {
            PlayerPrefs.SetInt(StaticString.saveArenaMonster2, -1);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveArenaMonster3))
        {
            PlayerPrefs.SetInt(StaticString.saveArenaMonster3, -1);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveArenaMonster4))
        {
            PlayerPrefs.SetInt(StaticString.saveArenaMonster4, -1);
        }

        if (!PlayerPrefs.HasKey(StaticString.saveLastRefreshRank))
        {
            PlayerPrefs.SetString(StaticString.saveLastRefreshRank, DateTime.Now.ToBinary().ToString());
        }
    }

    public void ChooseRewardGloves()
    {
        if (!PlayerPrefs.HasKey(StaticString.saveRewardGloves))
        {
            PlayerPrefs.SetInt(StaticString.saveRewardGloves, UnityEngine.Random.Range(0, allGloves.Length));
        }

        if (LoadGloves()[PlayerPrefs.GetInt(StaticString.saveRewardGloves)] >= 1f)
        {
            PlayerPrefs.SetInt(StaticString.saveRewardGloves, UnityEngine.Random.Range(0, allGloves.Length));
            ChooseRewardGloves();
        }
    }

    public void Vibrate()
    {
        if (PlayerPrefs.GetInt(StaticString.saveVibration) == 0) return;

        Handheld.Vibrate();
    }

    public List<int> GetKindOfMonster()
    {
        List<int> kindOfMonster = new List<int>();
        for (int i = 1; i < 13; i++)
        {
            int readMonster = ReadInt(txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), i);
            if (readMonster > 0)
            {
                kindOfMonster.Add(readMonster);
            }
        }
        return kindOfMonster;
    }

    public List<int> GetKindOfMonster(World _world)
    {
        List<int> kindOfMonster = new List<int>();
        for (int i = 1; i < 13; i++)
        {
            int readMonster = ReadInt(txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel) + 2, i);
            if (readMonster > 0)
            {
                List<int> tempWorldOfMonster = new List<int>(FindMonster(readMonster).world);
                if (_world == World.Grass)
                {
                    if (tempWorldOfMonster.Contains(0))
                    {
                        kindOfMonster.Add(readMonster);
                    }
                }
                else if (_world == World.Fire)
                {
                    if (tempWorldOfMonster.Contains(1))
                    {
                        kindOfMonster.Add(readMonster);
                    }
                }
                else
                {
                    if (tempWorldOfMonster.Contains(2))
                    {
                        kindOfMonster.Add(readMonster);
                    }
                }
            }
        }

        int count = 0;
        int maxCount = kindOfMonster.Count;

        for (int i = 1; i < allMonster.Length - 10; i++)
        {
            MonsterInfo checkInfo = allMonster[i];

            if (checkInfo.ID <= kindOfMonster[kindOfMonster.Count - 1]) continue;

            List<int> tempWorldOfMonster = new List<int>(allMonster[i].world);
            if (_world == World.Grass)
            {
                if (tempWorldOfMonster.Contains(0))
                {
                    kindOfMonster.Add(allMonster[i].ID);
                    count++;
                }
            }
            else if (_world == World.Fire)
            {
                if (tempWorldOfMonster.Contains(1))
                {
                    kindOfMonster.Add(allMonster[i].ID);
                    count++;
                }
            }
            else
            {
                if (tempWorldOfMonster.Contains(2))
                {
                    kindOfMonster.Add(allMonster[i].ID);
                    count++;
                }
            }

            if (count > maxCount) break;

        }

        return kindOfMonster;
    }

    public void LoadTutorial()
    {
        if (currentMap)
        {
            PoolMap.pool.Despawn(currentMap.transform);
            currentMap = null;
        }

        if (currentBoss)
        {
            Destroy(currentBoss.gameObject);
        }

        int mapIndex = UnityEngine.Random.Range(0, allNormalMaps.Length);
        currentMap = PoolMap.pool.Spawn(allNormalMaps[mapIndex].transform).GetComponent<MapController>();
        currentMap.transform.position += new Vector3(0, 2, 0);
        UseMainCam();

        UpdateAStar();

        player.transform.position = currentMap.playerPos[0].position;

        SpawnEnemyMonster(allMonster[0], currentMap.enemiesPos[0].position, Quaternion.identity);
        SpawnEnemyMonster(allMonster[0], currentMap.enemiesPos[1].position, Quaternion.identity);

        GC.Collect();
    }

    public void LoadArena()
    {
        if (currentMap)
        {
            PoolMap.pool.Despawn(currentMap.transform);
            currentMap = null;
        }

        if (currentBoss)
        {
            Destroy(currentBoss.gameObject);
        }

        ResetMonsterBallBoss();

        int mapIndex = UnityEngine.Random.Range(0, allArenaMaps.Length);
        currentMap = PoolMap.pool.Spawn(allArenaMaps[mapIndex].transform).GetComponent<MapController>();
        currentMap.transform.localScale = Vector3.one * 0.5f;
        currentBoss = Instantiate(allBoss[1].gameObject, currentMap.playerPos[1].position, Quaternion.identity).GetComponent<BossController>();
        currentBoss.OnSpawn();
        UseMainCam();

        UpdateAStar();

        player.transform.position = currentMap.playerPos[0].position;
    }

    public void LoadLevel()
    {
        isPlaying = false;

        if (currentMap)
        {
            PoolMap.pool.Despawn(currentMap.transform);
            currentMap = null;
        }

        if (currentBoss)
        {
            Destroy(currentBoss.gameObject);
        }


        if (PlayerPrefs.GetInt(StaticString.saveLevel) < 466)
        {
            if (ReadInt(txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14) == 0) // if current map is normal map
            {
                int mapIndex = UnityEngine.Random.Range(0, allNormalMaps.Length);
                currentMap = PoolMap.pool.Spawn(allNormalMaps[mapIndex].transform).GetComponent<MapController>();
                currentMap.transform.position += new Vector3(0, 2, 0);
                UseMainCam();
            }
            else if (ReadInt(txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14) == 1) // if current map is boss map
            {
                int mapIndex = UnityEngine.Random.Range(0, allBossMaps.Length);
                currentMap = PoolMap.pool.Spawn(allBossMaps[mapIndex].transform).GetComponent<MapController>();
                currentMap.transform.localScale = Vector3.one * 0.5f;
                currentBoss = Instantiate(allBoss[0].gameObject, currentMap.playerPos[1].position, Quaternion.identity).GetComponent<BossController>();
                currentBoss.OnSpawn();
                UseSubCam();
            }
            else // if current map is arena map
            {
                int mapIndex = UnityEngine.Random.Range(0, allArenaMaps.Length);
                currentMap = PoolMap.pool.Spawn(allArenaMaps[mapIndex].transform).GetComponent<MapController>();
                currentMap.transform.localScale = Vector3.one * 0.5f;
                currentBoss = Instantiate(allBoss[1].gameObject, currentMap.playerPos[1].position, Quaternion.identity).GetComponent<BossController>();
                currentBoss.OnSpawn();
                UseSubCam();
            }
        }
        else
        {
            if (ReadInt(txtLevelScript, 466, 14) == 0) // if current map is normal map
            {
                int mapIndex = UnityEngine.Random.Range(0, allNormalMaps.Length);
                currentMap = PoolMap.pool.Spawn(allNormalMaps[mapIndex].transform).GetComponent<MapController>();
                currentMap.transform.position += new Vector3(0, 2, 0);
                UseMainCam();
            }
        }

        UpdateAStar();

        player.transform.position = currentMap.playerPos[0].position;

        if (PlayerPrefs.GetInt(StaticString.saveLevel) >= 466 || ReadInt(txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 14) == 0) //If current map is normal map
        {
            int totalMonster = UnityEngine.Random.Range(2, 4);
            int readRateLv2;
            if (PlayerPrefs.GetInt(StaticString.saveLevel) >= 466)
            {
                readRateLv2 = ReadInt(txtLevelScript, 466, 13);
            }
            else
            {
                readRateLv2 = ReadInt(txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 13);
            }
            int countMonsterLv2 = 0;

            List<int> kindOfMonster = GetKindOfMonster();

            for (int i = 0; i < totalMonster; i++)
            {
                int ranMonster = kindOfMonster[UnityEngine.Random.Range(0, kindOfMonster.Count)];

                if (countMonsterLv2 < readRateLv2) // chose monster lv1 or lv2
                {
                    int idEvolve = FindMonster(ranMonster).IDMonsterEvolve;

                    if (idEvolve > 0)
                    {
                        MonsterInfo monsterLv2 = FindMonster(idEvolve);
                        SpawnEnemyMonster(monsterLv2, currentMap.enemiesPos[i].position, Quaternion.identity);
                        countMonsterLv2++;
                    }
                    else
                    {
                        SpawnEnemyMonster(FindMonster(ranMonster), currentMap.enemiesPos[i].position, Quaternion.identity);
                    }
                }
                else
                {
                    SpawnEnemyMonster(FindMonster(ranMonster), currentMap.enemiesPos[i].position, Quaternion.identity);
                }
            }
        }
        GC.Collect();
    }

    public void LoadSpecialMap(World _world)
    {
        if (currentMap)
        {
            PoolMap.pool.Despawn(currentMap.transform);
            currentMap = null;
        }

        if (currentBoss)
        {
            Destroy(currentBoss.gameObject);
        }

        ResetMonsterBallBoss();
        ResetAnimTrigger();

        // Get map in chosen World
        List<MapController> mapInWorld = new List<MapController>();
        for (int i = 0; i < allNormalMaps.Length; i++)
        {
            if (allNormalMaps[i].world == _world)
            {
                mapInWorld.Add(allNormalMaps[i]);
            }
        }
        int mapIndex = UnityEngine.Random.Range(0, mapInWorld.Count);
        currentMap = PoolMap.pool.Spawn(mapInWorld[mapIndex].transform).GetComponent<MapController>();
        currentMap.transform.position += new Vector3(0, 2, 0);

        UpdateAStar();

        player.transform.position = currentMap.playerPos[0].position;

        int totalMonster = UnityEngine.Random.Range(2, 5);
        int readRateLv2 = ReadInt(txtLevelScript, PlayerPrefs.GetInt(StaticString.saveLevel), 13);
        int countMonsterLv2 = 0;

        //Get Kind Of Monster In Chosen World
        List<int> kindOfMonster = GetKindOfMonster(_world);

        for (int i = 0; i < totalMonster; i++)
        {
            int ranMonster = kindOfMonster[UnityEngine.Random.Range(0, kindOfMonster.Count)];

            if (countMonsterLv2 < readRateLv2) // chose monster lv1 or lv2
            {
                int idEvolve = FindMonster(ranMonster).IDMonsterEvolve;

                if (idEvolve > 0)
                {
                    MonsterInfo monsterLv2 = FindMonster(idEvolve);
                    SpawnEnemyMonster(monsterLv2, currentMap.enemiesPos[i].position, Quaternion.identity);
                    countMonsterLv2++;
                }
                else
                {
                    SpawnEnemyMonster(FindMonster(ranMonster), currentMap.enemiesPos[i].position, Quaternion.identity);
                }
            }
            else
            {
                SpawnEnemyMonster(FindMonster(ranMonster), currentMap.enemiesPos[i].position, Quaternion.identity);
            }
        }

        isInSpecialWorld = true;
        GC.Collect();
    }

    public void LoadExchangeMap()
    {
        isExchangeMap = true;
        player.vMainCam.transform.localEulerAngles = Vector3.zero;
        player.transform.localEulerAngles = new Vector3(0, -135, 0);

        player.handRenderer.enabled = false;

        if (currentMap)
        {
            PoolMap.pool.Despawn(currentMap.transform);
            currentMap = null;
        }
        if (currentBoss)
        {
            Destroy(currentBoss.gameObject);
        }

        currentMap = PoolMap.pool.Spawn(exchangeMaps.transform).GetComponent<MapController>();
        currentBoss = Instantiate(exchangeBoss.gameObject, currentMap.playerPos[1].position, Quaternion.identity).GetComponent<BossController>();
        currentBoss.OnSpawn();
        currentBoss.transform.localScale = Vector3.one * 0.001f;

        player.transform.position = currentMap.playerPos[0].position;

        GetMonsterToExchange();
        GC.Collect();
    }

    public void LoadSpin()
    {
        isSpin = true;
        sea.SetActive(false);
        UIManager.ins.sSpin.allSpinBall.Clear();
        player.transform.position = Vector3.zero;
        player.vMainCam.transform.localEulerAngles = Vector3.zero;
        player.transform.eulerAngles = Vector3.zero;

        player.handRenderer.enabled = false;

        ResetMonsterBallBoss();
        if (currentMap)
        {
            PoolMap.pool.Despawn(currentMap.transform);
            currentMap = null;
        }
        if (currentBoss)
        {
            Destroy(currentBoss.gameObject);
        }

        UIManager.ins.sSpin.spinBase = Instantiate(UIManager.ins.sSpin.spinBasePrefabs, new Vector3(0, -1.75f, 6), Quaternion.Euler(0, -180, 0));
        Vector3 ballSpawnPos = UIManager.ins.sSpin.spinBase.transform.GetChild(1).GetChild(0).GetComponent<Renderer>().bounds.center;
        for (int i = 0; i < 30; i++)
        {
            GameObject ball = UIManager.ins.sSpin.spinBallsPrefabs[UnityEngine.Random.Range(0, UIManager.ins.sSpin.spinBallsPrefabs.Length)];
            UIManager.ins.sSpin.allSpinBall.Add(Instantiate(ball, ballSpawnPos, Quaternion.Euler(UnityEngine.Random.Range(-180, 180), UnityEngine.Random.Range(-180, 180), UnityEngine.Random.Range(-180, 180))));
        }
        GC.Collect();
    }

    public void GetMonsterToExchange()
    {
        UIManager uIManager = UIManager.ins;

        List<int> getMonsterToExchange = new List<int>();
        List<BtnChoseMonster> getBtnChoseMonsterToExchange = new List<BtnChoseMonster>();

        for (int i = 1; i < uIManager.monsterView.allBallButton.Count; i++)
        {
            int IDMonster = uIManager.monsterView.allBallButton[i].IDMonster;
            if (IDMonster > 0)
            {
                getMonsterToExchange.Add(IDMonster);
                getBtnChoseMonsterToExchange.Add(uIManager.monsterView.allBallButton[i]);
            }
        }

        //Spawn player's Monster
        int rpm = UnityEngine.Random.Range(0, getMonsterToExchange.Count);
        ParticleSystem ps1 = PoolVFX.pool.Spawn(uIManager.sExchange.freeMonsterFX, transform.position, Quaternion.Euler(-90, 0, 0)).GetComponent<ParticleSystem>();
        PoolVFX.pool.Despawn(ps1.transform, ps1.main.duration);
        uIManager.sExchange.playerMonster = SpawnEnemyMonster(FindMonster(getMonsterToExchange[rpm]), currentMap.enemiesPos[0].position, Quaternion.identity).GetComponent<MonsterInfo>();
        uIManager.sExchange.playerMonster.healthBar.transform.parent.gameObject.SetActive(false);
        uIManager.sExchange.btnControllMonsterExchange = getBtnChoseMonsterToExchange[rpm];

        //Spawn Monster to exchange
        ParticleSystem ps2 = PoolVFX.pool.Spawn(uIManager.sExchange.freeMonsterFX, transform.position, Quaternion.Euler(-90, 0, 0)).GetComponent<ParticleSystem>();
        PoolVFX.pool.Despawn(ps2.transform, ps2.main.duration);
        // uIManager.sExchange.monsterToExchange = SpawnEnemyMonster(FindMonster(99), currentMap.enemiesPos[1].position, Quaternion.identity).GetComponent<MonsterInfo>();

        int rm = getMonsterToExchange[rpm] + UnityEngine.Random.Range(-1, 3);
        MonsterInfo chosen = FindMonster(rm);

        if (chosen != null)
        {
            if (RandomByPercent(40))
            {
                if (chosen.IDMonsterEvolve <= 0)
                {
                    chosen = FindMonster(FindMonster(rm).ID - 100);
                }
            }
            else
            {
                if (chosen.IDMonsterEvolve > 0)
                {
                    chosen = FindMonster(FindMonster(rm).ID + 100);
                }
            }

            uIManager.sExchange.monsterToExchange = SpawnEnemyMonster(chosen, currentMap.enemiesPos[1].position, Quaternion.identity).GetComponent<MonsterInfo>();
            uIManager.sExchange.monsterToExchange.healthBar.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            chosen = FindMonster(getMonsterToExchange[rpm]);
            uIManager.sExchange.monsterToExchange = SpawnEnemyMonster(chosen, currentMap.enemiesPos[1].position, Quaternion.identity).GetComponent<MonsterInfo>();
            uIManager.sExchange.monsterToExchange.healthBar.transform.parent.gameObject.SetActive(false);
        }

        uIManager.sExchange.playerMonster.transform.localScale /= 2f;
        uIManager.sExchange.monsterToExchange.transform.localScale /= 2f;
    }

    public int ReadInt(TextAsset txtFile, int r, int c)
    {
        string[] data = txtFile.text.Split(new string[] { "\n" }, StringSplitOptions.None);
        string[] dataInRow = data[r].Split(new string[] { "," }, StringSplitOptions.None);

        if (dataInRow[c] == "" || dataInRow[c] == null)
        {
            return -1;
        }

        if (int.TryParse(dataInRow[c], out int a))
        {
            return a;
        }
        else
        {
            var last = dataInRow[c][dataInRow[c].Length - 1];
            try
            {
                return int.Parse("1403" + last);
            }
            catch (System.Exception)
            {
                return 14030;
            }
        }
    }

    public float ReadFloat(TextAsset txtFile, int r, int c)
    {
        string[] data = txtFile.text.Split(new string[] { "\n" }, StringSplitOptions.None);
        string[] dataInRow = data[r].Split(new string[] { "," }, StringSplitOptions.None);

        if (dataInRow[c] == "" || dataInRow[c] == null)
        {
            return -1;
        }

        if (float.TryParse(dataInRow[c], out float a))
        {
            return a;
        }
        else
        {
            var last = dataInRow[c][dataInRow[c].Length - 1];
            try
            {
                return int.Parse("1403" + last);
            }
            catch (System.Exception)
            {
                return 14030;
            }
        }
    }

    public string ReadString(TextAsset txtFile, int r, int c)
    {
        string[] data = txtFile.text.Split(new string[] { "\n" }, StringSplitOptions.None);
        string[] dataInRow = data[r].Split(new string[] { "," }, StringSplitOptions.None);

        if (dataInRow[c] == "" || dataInRow[c] == null)
        {
            return "Waiting For Name";
        }

        return dataInRow[c];

    }

    public void ArenaRefreshRank()
    {
        for (int i = 0; i < UnityEngine.Random.Range(3, 11); i++)
        {
            int random = UnityEngine.Random.Range(0, 100);
            int randomRank;

            // Choose rank to change
            if (random > 0 && random <= 10)         // 10% change rank 1-3
            {
                randomRank = UnityEngine.Random.Range(1, 4);
            }
            else if (random > 10 && random <= 40)   // 30% change rank 4-29
            {
                randomRank = UnityEngine.Random.Range(4, 30);
            }
            else                                    // 60% change rank 30-100
            {
                randomRank = UnityEngine.Random.Range(30, 101);
            }

            if (randomRank == PlayerPrefs.GetInt(StaticString.saveArenaRank)) continue;

            // Create new data
            int newRank = LoadDataRank(randomRank).rankArena;
            int newAvatar = UnityEngine.Random.Range(1, 211);
            string newName = StaticString.arenaName[UnityEngine.Random.Range(0, StaticString.arenaName.Length)];
            int[] oldMonster = new int[5] { LoadDataRank(randomRank).monster1Arena, LoadDataRank(randomRank).monster2Arena, LoadDataRank(randomRank).monster3Arena, LoadDataRank(randomRank).monster4Arena, LoadDataRank(randomRank).monster5Arena };
            int[] newMonster = new int[5];
            for (int j = 0; j < 5; j++)
            {
                if (oldMonster[j] > 0)
                {
                    if (oldMonster[j] + 2 <= 210 && oldMonster[j] - 2 >= 1)
                    {
                        newMonster[j] = oldMonster[j] + UnityEngine.Random.Range(-2, 3);
                    }
                    else
                    {
                        newMonster[j] = oldMonster[j];
                    }
                }
                else
                {
                    if (newRank <= 100 && newRank > 30)
                    {
                        int r = Random(new int[3] { 30, 60, 10 });
                        if (r == 0)           // 30% monster 80-90 E2
                        {
                            newMonster[j] = UnityEngine.Random.Range(180, 190);
                        }
                        else if (r == 1)     // 60% monster 90-100 E2
                        {
                            newMonster[j] = UnityEngine.Random.Range(190, 201);
                        }
                        else                            // 10% monster IAP 
                        {
                            newMonster[j] = UnityEngine.Random.Range(201, 210);
                        }
                    }
                    else if (newRank <= 30 && newRank > 3)
                    {
                        int r = Random(new int[3] { 15, 65, 20 });
                        if (r == 0)           // 15% monster 80-90 E2
                        {
                            newMonster[j] = UnityEngine.Random.Range(180, 190);
                        }
                        else if (r == 1)     // 65% monster 90-100 E2
                        {
                            newMonster[j] = UnityEngine.Random.Range(190, 201);
                        }
                        else                            // 20% monster IAP 
                        {
                            newMonster[j] = UnityEngine.Random.Range(201, 210);
                        }
                    }
                    else
                    {
                        int r = Random(new int[2] { 40, 60 });
                        if (r == 0)           // 40% monster 80-90 E2
                        {
                            newMonster[j] = UnityEngine.Random.Range(190, 201);
                        }
                        else                            // 60% monster IAP 
                        {
                            newMonster[j] = UnityEngine.Random.Range(201, 210);
                        }
                    }
                }
            }
            int newPoint;
            int currentPoint = LoadDataRank(randomRank).pointArena;
            if (randomRank == 1)
            {
                int lowerPoint = LoadDataRank(randomRank + 1).pointArena;
                newPoint = currentPoint + UnityEngine.Random.Range(lowerPoint - currentPoint + 1, 100);
            }
            else if (randomRank == 100)
            {
                int higherPoint = LoadDataRank(randomRank - 1).pointArena;
                newPoint = currentPoint + UnityEngine.Random.Range(-100, higherPoint - currentPoint - 1);
            }
            else
            {
                int higherPoint = LoadDataRank(randomRank - 1).pointArena;
                int lowerPoint = LoadDataRank(randomRank + 1).pointArena;
                newPoint = UnityEngine.Random.Range(lowerPoint + 1, higherPoint);
            }

            // Debug.Log("Updated rank " + randomRank);
            // Debug.Log(LoadDataRank(randomRank).rankArena + " " + LoadDataRank(randomRank).avatarArena + " " + LoadDataRank(randomRank).nameArena + " " + LoadDataRank(randomRank).monster1Arena + " " + LoadDataRank(randomRank).monster2Arena + " " + LoadDataRank(randomRank).monster3Arena + " " + LoadDataRank(randomRank).monster4Arena + " " + LoadDataRank(randomRank).monster5Arena + " " + LoadDataRank(randomRank).pointArena);

            SaveDataRankArena(newRank, newAvatar, newName, newMonster[0], newMonster[1], newMonster[2], newMonster[3], newMonster[4], newPoint);

            // Debug.Log(newRank + " " + newAvatar + " " + newName + " " + newMonster[0] + " " + newMonster[1] + " " + newMonster[2] + " " + newMonster[3] + " " + newMonster[4] + " " + newPoint);
        }
    }

    public void UpdateArenaPlayerRank(int oldRank, int newRank)
    {
        if (oldRank == newRank) return;

        if (newRank <= 100)
        {
            int newAvatar = PlayerPrefs.GetInt(StaticString.saveAvatar);
            string newArenaName = PlayerPrefs.GetString(StaticString.saveArenaName);
            int newArenaMonster0 = PlayerPrefs.GetInt(StaticString.saveArenaMonster0);
            int newArenaMonster1 = PlayerPrefs.GetInt(StaticString.saveArenaMonster1);
            int newArenaMonster2 = PlayerPrefs.GetInt(StaticString.saveArenaMonster2);
            int newArenaMonster3 = PlayerPrefs.GetInt(StaticString.saveArenaMonster3);
            int newArenaMonster4 = PlayerPrefs.GetInt(StaticString.saveArenaMonster4);
            int newArenaPoint = PlayerPrefs.GetInt(StaticString.saveArenaPoint);

            SaveDataRankArena(newRank, newAvatar, newArenaName, newArenaMonster0, newArenaMonster1, newArenaMonster2, newArenaMonster3, newArenaMonster4, newArenaPoint);
        }

        if (oldRank <= 100)
        {
            int random = UnityEngine.Random.Range(0, 100);

            // Create new data
            int oldAvatar = UnityEngine.Random.Range(1, 211);
            string oldName = StaticString.arenaName[UnityEngine.Random.Range(0, StaticString.arenaName.Length)];
            int[] oldMonster = new int[5] { LoadDataRank(oldRank).monster1Arena, LoadDataRank(oldRank).monster2Arena, LoadDataRank(oldRank).monster3Arena, LoadDataRank(oldRank).monster4Arena, LoadDataRank(oldRank).monster5Arena };
            int[] oldOMonster = new int[5];
            for (int j = 0; j < 5; j++)
            {
                if (oldMonster[j] > 0)
                {
                    if (oldMonster[j] + 2 <= 210 && oldMonster[j] - 2 >= 1)
                    {
                        oldOMonster[j] = oldMonster[j] + UnityEngine.Random.Range(-2, 3);
                    }
                    else
                    {
                        oldOMonster[j] = oldMonster[j];
                    }
                }
                else
                {
                    if (oldRank <= 100 && oldRank > 30)
                    {
                        int r = Random(new int[3] { 30, 60, 10 });
                        if (r == 0)           // 30% monster 80-90 E2
                        {
                            oldOMonster[j] = UnityEngine.Random.Range(180, 190);
                        }
                        else if (r == 1)     // 60% monster 90-100 E2
                        {
                            oldOMonster[j] = UnityEngine.Random.Range(190, 201);
                        }
                        else                            // 10% monster IAP 
                        {
                            oldOMonster[j] = UnityEngine.Random.Range(201, 210);
                        }
                    }
                    else if (oldRank <= 30 && oldRank > 3)
                    {
                        int r = Random(new int[3] { 15, 65, 20 });
                        if (r == 0)           // 15% monster 80-90 E2
                        {
                            oldOMonster[j] = UnityEngine.Random.Range(180, 190);
                        }
                        else if (r == 1)     // 65% monster 90-100 E2
                        {
                            oldOMonster[j] = UnityEngine.Random.Range(190, 201);
                        }
                        else                            // 20% monster IAP 
                        {
                            oldOMonster[j] = UnityEngine.Random.Range(201, 210);
                        }
                    }
                    else
                    {
                        int r = Random(new int[2] { 40, 60 });
                        if (r == 0)           // 40% monster 80-90 E2
                        {
                            oldOMonster[j] = UnityEngine.Random.Range(190, 201);
                        }
                        else                            // 60% monster IAP 
                        {
                            oldOMonster[j] = UnityEngine.Random.Range(201, 210);
                        }
                    }
                }
            }
            int oldPoint;
            int currentPoint = LoadDataRank(oldRank).pointArena;
            if (oldRank == 1)
            {
                int lowerPoint = LoadDataRank(oldRank + 1).pointArena;
                oldPoint = currentPoint + UnityEngine.Random.Range(lowerPoint - currentPoint + 1, 100);
            }
            else if (oldRank == 100)
            {
                int higherPoint = LoadDataRank(oldRank - 1).pointArena;
                oldPoint = currentPoint + UnityEngine.Random.Range(-100, higherPoint - currentPoint - 1);
            }
            else
            {
                int higherPoint = LoadDataRank(oldRank - 1).pointArena;
                int lowerPoint = LoadDataRank(oldRank + 1).pointArena;
                oldPoint = UnityEngine.Random.Range(lowerPoint + 1, higherPoint);
            }

            SaveDataRankArena(oldRank, oldAvatar, oldName, oldOMonster[0], oldOMonster[1], oldOMonster[2], oldOMonster[3], oldOMonster[4], oldPoint);
        }
    }

    // public void CreateArenaRankData()
    // {
    //     string top100Path = AssetDatabase.GetAssetPath(txtArenaTop100);
    //     string tempTxtArenaTop100 = txtArenaTop100.text;

    //     string[] data = tempTxtArenaTop100.Split(new string[] { "\n" }, StringSplitOptions.None);

    //     for (int i = 1; i < data.Length; i++)
    //     {
    //         int newRank = i;
    //         int newAvatar = UnityEngine.Random.Range(1, 211);
    //         string newName = StaticString.arenaName[UnityEngine.Random.Range(0, StaticString.arenaName.Length)];
    //         int[] newMonster = new int[5];
    //         int newPoint;

    //         if (i <= 100 && i > 30)
    //         {
    //             for (int j = 0; j < 5; j++)
    //             {
    //                 int r = Random(new int[3] { 30, 60, 10 });
    //                 if (r == 0)           // 30% monster 80-90 E2
    //                 {
    //                     newMonster[j] = UnityEngine.Random.Range(180, 190);
    //                 }
    //                 else if (r == 1)     // 60% monster 90-100 E2
    //                 {
    //                     newMonster[j] = UnityEngine.Random.Range(190, 201);
    //                 }
    //                 else                            // 10% monster IAP 
    //                 {
    //                     newMonster[j] = UnityEngine.Random.Range(201, 210);
    //                 }
    //             }
    //             newPoint = (14398 * 2) - (i * 143);
    //         }
    //         else if (i <= 30 && i > 3)
    //         {
    //             for (int j = 0; j < 5; j++)
    //             {
    //                 int r = Random(new int[3] { 15, 65, 20 });
    //                 if (r == 0)           // 15% monster 80-90 E2
    //                 {
    //                     newMonster[j] = UnityEngine.Random.Range(180, 190);
    //                 }
    //                 else if (r == 1)     // 65% monster 90-100 E2
    //                 {
    //                     newMonster[j] = UnityEngine.Random.Range(190, 201);
    //                 }
    //                 else                            // 20% monster IAP 
    //                 {
    //                     newMonster[j] = UnityEngine.Random.Range(201, 210);
    //                 }
    //             }
    //             newPoint = Mathf.RoundToInt((14398 * 2.5f) - (i * 143));
    //         }
    //         else
    //         {
    //             for (int j = 0; j < 5; j++)
    //             {
    //                 int r = Random(new int[2] { 40, 60 });
    //                 if (r == 0)           // 40% monster 80-90 E2
    //                 {
    //                     newMonster[j] = UnityEngine.Random.Range(190, 201);
    //                 }
    //                 else                            // 60% monster IAP 
    //                 {
    //                     newMonster[j] = UnityEngine.Random.Range(201, 210);
    //                 }
    //             }
    //             newPoint = (14398 * 4) - (i * 143);
    //         }


    //         string newData = newRank + "," + newAvatar + "," + newName + "," + newMonster[0] + "," + newMonster[1] + "," + newMonster[2] + "," + newMonster[3] + "," + newMonster[4] + "," + newPoint;
    //         Debug.Log(newData);
    //         tempTxtArenaTop100 = tempTxtArenaTop100.Replace(data[i], newData);
    //     }

    //     File.WriteAllText(top100Path, tempTxtArenaTop100);
    //     AssetDatabase.ImportAsset(top100Path);
    // }

    public int Random(int[] allRate)
    {
        int total = 0;
        foreach (int i in allRate)
        {
            total += i;
        }

        int r = UnityEngine.Random.Range(0, total);
        for (int i = 0; i < allRate.Length; i++)
        {
            if (r <= allRate[i])
            {
                return i;
            }
            else
            {
                r -= allRate[i];
            }
        }
        return -1;
    }

    public void SaveDataRankArena(int rank, int avatar, string name, int monster1, int monster2, int monster3, int monster4, int monster5, int point)
    {
        SaveGame saveRankData = new SaveGame(rank, avatar, name, monster1, monster2, monster3, monster4, monster5, point);
        SaveSystem.SaveData(saveRankData, StaticString.pathSaveArenaRank);
    }

    public ArenaData100[] LoadAllDataRank()
    {
        if (File.Exists(StaticString.pathSaveArenaRank))
        {
            SaveGame data = SaveSystem.LoadData<SaveGame>(StaticString.pathSaveArenaRank);
            return data.arenaData100s;
        }
        else
        {
            string[] data = txtArenaTop100.text.Split(new string[] { "\n" }, StringSplitOptions.None);
            int dataLength = data.Length;
            ArenaData100[] arenaData100s = new ArenaData100[dataLength - 1];

            for (int i = 1; i < dataLength; i++)
            {
                arenaData100s[i - 1] = new ArenaData100();
                arenaData100s[i - 1].rankArena = ReadInt(txtArenaTop100, i, 0);
                arenaData100s[i - 1].avatarArena = ReadInt(txtArenaTop100, i, 1);
                arenaData100s[i - 1].nameArena = ReadString(txtArenaTop100, i, 2);
                arenaData100s[i - 1].monster1Arena = ReadInt(txtArenaTop100, i, 3);
                arenaData100s[i - 1].monster2Arena = ReadInt(txtArenaTop100, i, 4);
                arenaData100s[i - 1].monster3Arena = ReadInt(txtArenaTop100, i, 5);
                arenaData100s[i - 1].monster4Arena = ReadInt(txtArenaTop100, i, 6);
                arenaData100s[i - 1].monster5Arena = ReadInt(txtArenaTop100, i, 7);
                arenaData100s[i - 1].pointArena = ReadInt(txtArenaTop100, i, 8);
            }

            return arenaData100s;
        }
    }

    public ArenaData100 LoadDataRank(int rank)
    {
        if (File.Exists(StaticString.pathSaveArenaRank))
        {
            SaveGame data = SaveSystem.LoadData<SaveGame>(StaticString.pathSaveArenaRank);
            return data.arenaData100s[rank - 1];
        }
        else
        {
            string[] data = txtArenaTop100.text.Split(new string[] { "\n" }, StringSplitOptions.None);
            int dataLength = data.Length;
            ArenaData100[] arenaData100s = new ArenaData100[dataLength - 1];

            for (int i = 1; i < dataLength; i++)
            {
                arenaData100s[i - 1] = new ArenaData100();
                arenaData100s[i - 1].rankArena = ReadInt(txtArenaTop100, i, 0);
                arenaData100s[i - 1].avatarArena = ReadInt(txtArenaTop100, i, 1);
                arenaData100s[i - 1].nameArena = ReadString(txtArenaTop100, i, 2);
                arenaData100s[i - 1].monster1Arena = ReadInt(txtArenaTop100, i, 3);
                arenaData100s[i - 1].monster2Arena = ReadInt(txtArenaTop100, i, 4);
                arenaData100s[i - 1].monster3Arena = ReadInt(txtArenaTop100, i, 5);
                arenaData100s[i - 1].monster4Arena = ReadInt(txtArenaTop100, i, 6);
                arenaData100s[i - 1].monster5Arena = ReadInt(txtArenaTop100, i, 7);
                arenaData100s[i - 1].pointArena = ReadInt(txtArenaTop100, i, 8);
            }

            return arenaData100s[rank - 1];
        }
    }

    public void SaveCollectionReward(int IDCReward, bool status)
    {
        SaveGame smr = new SaveGame(IDCReward, status);
        SaveSystem.SaveData(smr, StaticString.pathSaveCollectionReward);
    }

    public bool[] LoadCollectionReward()
    {
        if (File.Exists(StaticString.pathSaveCollectionReward))
        {
            SaveGame data = SaveSystem.LoadData<SaveGame>(StaticString.pathSaveCollectionReward);
            return data.collectionRewardStatus;
        }
        else
        {
            bool[] collectionRewardStatus = new bool[9];

            for (int i = 0; i < collectionRewardStatus.Length; i++)
            {
                collectionRewardStatus[i] = true;
            }

            return collectionRewardStatus;
        }
    }

    public void SaveGloves(int IDGloves, float status)
    {
        SaveGame smc = new SaveGame(IDGloves, status);
        SaveSystem.SaveData(smc, StaticString.pathSaveGloves);
    }

    public float[] LoadGloves()
    {
        if (File.Exists(StaticString.pathSaveGloves))
        {
            SaveGame data = SaveSystem.LoadData<SaveGame>(StaticString.pathSaveGloves);
            return data.glovesStatus;
        }
        else
        {
            float[] glovesStatus = new float[allGloves.Length];
            glovesStatus[0] = 2;
            for (int i = 1; i < glovesStatus.Length; i++)
            {
                glovesStatus[i] = 0;
            }

            return glovesStatus;
        }
    }

    public void SaveMonsterCollection(int IDMonster)
    {
        SaveGame smc = new SaveGame(IDMonster);
        SaveSystem.SaveData(smc, StaticString.pathSaveMonsterCollection);
    }

    public List<int> LoadMonsterCollection()
    {
        if (File.Exists(StaticString.pathSaveMonsterCollection))
        {
            SaveGame data = SaveSystem.LoadData<SaveGame>(StaticString.pathSaveMonsterCollection);
            return data.IDOfCaughtMonster;
        }
        return new List<int>();
    }

    public void SaveMonsterView()
    {
        SaveGame smv = new SaveGame();
        SaveSystem.SaveData(smv, StaticString.pathSaveMonsterView);
    }

    public void LoadMonsterView()
    {
        if (File.Exists(StaticString.pathSaveMonsterView))
        {
            SaveGame data = SaveSystem.LoadData<SaveGame>(StaticString.pathSaveMonsterView);
            List<BtnChoseMonster> allBallBtnList = UIManager.ins.monsterView.allBallButton;
            for (int i = 1; i < data.arrayIDMonster.Length; i++)
            {
                if (i < allBallBtnList.Count)
                {
                    allBallBtnList[i].IDMonster = data.arrayIDMonster[i];
                    allBallBtnList[i].unlockAtLevel = data.arrayLvUnlock[i];
                }
                else
                {
                    UIManager.ins.monsterView.AddEmptyButtonNotScroll();
                    allBallBtnList[i].IDMonster = data.arrayIDMonster[i];
                    allBallBtnList[i].unlockAtLevel = data.arrayLvUnlock[i];
                }
            }

        }
    }

    public bool RandomByPercent(int percent)
    {
        if (UnityEngine.Random.Range(0, 100) < percent)
        {
            return true;
        }
        return false;
    }

    public MonsterInfo FindMonster(int ID)
    {
        foreach (MonsterInfo mi in allMonster)
        {
            if (mi.ID == ID)
            {
                return mi;
            }
        }

        Debug.LogError("Can not find Monster " + ID);
        return null;
    }


    public Transform SpawnEnemyMonster(MonsterInfo monster, Vector3 pos, Quaternion rot)
    {
        Transform m = PoolEnemyMonster.pool.Spawn(monster.transform, pos, rot);
        try
        {
            Destroy(m.GetComponent<EnemyMonster>());
        }
        catch (System.Exception)
        {

        }
        m.gameObject.AddComponent<EnemyMonster>().OnSpawn();
        // m.GetChild(2).gameObject.SetActive(false);
        return m;
    }

    public Transform InstantiateEnemyMonster(MonsterInfo monster, Vector3 pos, Quaternion rot)
    {
        Transform m = Instantiate(monster.transform, pos, rot);
        m.gameObject.AddComponent<EnemyMonster>().OnSpawn();
        return m;
    }

    public Transform SpawnPlayerMonster(MonsterInfo monster, Vector3 pos, Quaternion rot)
    {
        Transform m = PoolPlayerMonster.pool.Spawn(monster.transform, pos, rot);
        try
        {
            Destroy(m.GetComponent<PlayerMonster>());
        }
        catch (System.Exception)
        {

        }
        m.gameObject.AddComponent<PlayerMonster>().OnSpawn();
        return m;
    }

    public Transform SpawnMonsterBuff(MonsterInfo monster, Vector3 pos, Quaternion rot)
    {
        Transform m = PoolPlayerMonster.pool.Spawn(monster.transform, pos, rot);
        try
        {
            Destroy(m.GetComponent<PlayerMonster>());
        }
        catch (System.Exception)
        {

        }
        m.gameObject.AddComponent<PlayerMonster>().OnSpawnBuff();
        return m;
    }

    public Transform SpawnPlayerMonster(MonsterInfo monster, Vector3 pos, Quaternion rot, BtnChoseMonster _btnControllThisMonster)
    {
        Transform m = PoolPlayerMonster.pool.Spawn(monster.transform, pos, rot);
        try
        {
            Destroy(m.GetComponent<PlayerMonster>());
        }
        catch (System.Exception)
        {

        }
        m.gameObject.AddComponent<PlayerMonster>().AutoOnSpawn(_btnControllThisMonster);
        return m;
    }

    public Transform InstantiatePlayerMonster(MonsterInfo monster, Vector3 pos, Quaternion rot)
    {
        Transform m = Instantiate(monster.transform, pos, rot);
        m.gameObject.AddComponent<PlayerMonster>().OnSpawn();
        return m;
    }

    public Sprite FindAvatar(string name)
    {
        foreach (Sprite s in allAvatar)
        {
            if (s.name == name)
            {
                return s;
            }
        }

        Debug.Log("Cant find sprite: " + name);
        return null;
    }

    // private void ReadMonsterInfo()
    // {
    //     int monsterInfoScriptLength = txtMonsterInfo.text.Split(new string[] { "\n" }, StringSplitOptions.None).Length;

    //     foreach (MonsterInfo monsterInfoScript in allMonster)
    //     {
    //         GameObject monsterPrefab = monsterInfoScript.gameObject;
    //         string monsterPrefabPath = AssetDatabase.GetAssetPath(monsterPrefab); // AssetDatabase.GetAssetPath(monsterPrefab) return Assets/MyAssets/Prefabs/Monster/Monster001.prefab

    //         // Load the contents of the Prefab Asset.
    //         GameObject monsterContentsRoot = PrefabUtility.LoadPrefabContents(monsterPrefabPath);


    //         // // Add Components
    //         // monsterContentsRoot.AddComponent<AIDestinationSetter>();
    //         // AIPath monsterAIPath = monsterContentsRoot.AddComponent<AIPath>();

    //         // monsterAIPath.radius = 0.35f;
    //         // monsterAIPath.height = 1;
    //         // monsterAIPath.enableRotation = true;

    //         // Get ID
    //         string getID = monsterContentsRoot.name.Substring(monsterContentsRoot.name.Length - 3, 3);

    //         // Modify Prefab contents.
    //         MonsterInfo monsterContentsInfo = monsterContentsRoot.GetComponent<MonsterInfo>();

    //         // monsterContentsInfo.ID = int.Parse(getID);
    //         for (int j = 0; j < monsterInfoScriptLength; j++)
    //         {
    //             if (ReadFloat(txtMonsterInfo, j, 0) == int.Parse(getID))
    //             {
    //                 // monsterContentsInfo.IDMonsterEvolve = ReadInt(txtMonsterInfo, j, 1);
    //                 // monsterContentsInfo.monsterName = ReadString(txtMonsterInfo, j, 2);
    //                 // monsterContentsInfo.moveSpeed = ReadFloat(txtMonsterInfo, j, 7);

    //                 // monsterContentsInfo.attackZone = ReadFloat(txtMonsterInfo, j, 9);
    //                 // monsterContentsInfo.dmg = ReadFloat(txtMonsterInfo, j, 6);

    //                 // monsterContentsInfo.maxHealth = ReadFloat(txtMonsterInfo, j, 5);

    //                 // monsterContentsInfo.world[0] = ReadInt(txtMonsterInfo, j, 11);
    //                 // monsterContentsInfo.world[1] = ReadInt(txtMonsterInfo, j, 12);
    //                 // monsterContentsInfo.world[2] = ReadInt(txtMonsterInfo, j, 13);

    //                 // monsterContentsInfo.attackGrassFX = tempFX[0];
    //                 // monsterContentsInfo.attackFireFX = tempFX[1];
    //                 // monsterContentsInfo.attackWaterFX = tempFX[2];

    //                 monsterContentsInfo.scaleIndex = ReadFloat(txtMonsterInfo, j, 14);

    //                 // if (ReadInt(txtMonsterInfo, j, 1) != -1)
    //                 // {
    //                 //     monsterContentsInfo.avatar[0] = FindAvatar("IconMInbox_M" + getID);
    //                 //     monsterContentsInfo.avatar[1] = FindAvatar("IconMActive_M" + getID);
    //                 // }
    //                 // else
    //                 // {
    //                 //     monsterContentsInfo.avatar[0] = FindAvatar("IconMInbox_ME" + getID);
    //                 //     monsterContentsInfo.avatar[1] = FindAvatar("IconMActive_ME" + getID);
    //                 // }
    //             }
    //         }

    //         // Save contents back to Prefab Asset and unload contents.
    //         PrefabUtility.SaveAsPrefabAsset(monsterContentsRoot, monsterPrefabPath);
    //         PrefabUtility.UnloadPrefabContents(monsterContentsRoot);
    //     }
    // }
}
