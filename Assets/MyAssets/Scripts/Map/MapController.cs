
/* Copyright © @ThanhDV */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{
    public World world;
    public Transform monsterBuffPos;
    public Transform[] enemiesPos;
    public Transform[] playerMonsterPos;
    public Transform[] playerPos;
    public Transform[] camPos;
}

public enum World
{
    Special,
    Grass,
    Fire,
    Water
}