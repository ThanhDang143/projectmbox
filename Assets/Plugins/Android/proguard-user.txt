-keep class com.facebook.** {
   *;
}

-keep class com.adjust.sdk.** { *; }
-keep class com.google.android.gms.common.ConnectionResult {
    int SUCCESS;
}
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient {
    com.google.android.gms.ads.identifier.AdvertisingIdClient$Info getAdvertisingIdInfo(android.content.Context);
}
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient$Info {
    java.lang.String getId();
    boolean isLimitAdTrackingEnabled();
}
-keep public class com.android.installreferrer.** { *; }

-dontwarn com.google.android.**
-dontwarn com.bytedance.**
-dontwarn com.facebook.infer.annotation.**
-dontwarn com.facebook.infer.annotation.**
-dontwarn com.unity3d.services.**