/* @ThanhD143 */

using System;
using System.Collections;
using SendEventAppMetrica;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using static SendEventAppMetrica.SendEventManager;

public class ManagerAds : MonoBehaviour
{
    public static ManagerAds ins;

    [Header("Advertisement Key")]
    [SerializeField] private string MaxSdkKey = "7PVEKQP99BDlhoV1Hc14DThXC_ER1yiVdlVcEdF-Wg1ysQxZd8IotWvSz-RwbNTsW-b-_r78TuNvgFAFJkQ_K5";
    [SerializeField] public string BannerKey;
    [SerializeField] public string InterstitialKey;
    [SerializeField] public string RewardedKey;

    [Header("Options")]
    // [SerializeField] private bool hasUserConsent = true;
    // [SerializeField] private bool isAgeRestrictedUser = true;
    [SerializeField] private bool autoStartBanner = true;
    [SerializeField] private MaxSdkBase.BannerPosition bannerPosition = MaxSdkBase.BannerPosition.BottomCenter;
    [SerializeField] private string privacyPolicyUrl = "https://omegame.asia/policy/";
    private string show_interstitial_ads_interval = "show_interstitial_ads_interval";

    [Header("Debug Screen")]
    public GameObject debugger;
    public Button showBannerButton;
    public Button showInterstitialButton;
    public Button showRewardedButton;
    public Button mediationDebuggerButton;
    public Text interstitialStatusText;
    public Text rewardedStatusText;
    private bool isBannerShowing;
    private int interstitialRetryAttempt;
    private int rewardedRetryAttempt;

    [Header("Other")]
    private string placementAdsInt;
    private string placementAdsReward;
    private bool receivedRewardComplete = false;
    private UnityAction<bool> OnCompleteRewardMethod;
    private UnityAction OnInterstitialClosed;
    private bool interstitialIsCountingDown;

    [Header("RemoveAds")]
    public static string Save_Remove_BannerAds;
    public static string Save_Remove_InterstitialAds;
    public static string Save_Remove_RewardAds;

    private void Awake()
    {
        ins = this;
    }

    private void Start()
    {
        debugger.SetActive(false);
        interstitialIsCountingDown = false;
        SetupSaveRemoveAds();

        showInterstitialButton.onClick.AddListener(() => ShowInterstitial());
        showRewardedButton.onClick.AddListener(() => ShowRewarded());
        showBannerButton.onClick.AddListener(ToggleBannerVisibility);
        mediationDebuggerButton.onClick.AddListener(MaxSdk.ShowMediationDebugger);

        MaxSdkCallbacks.OnSdkInitializedEvent += sdkConfiguration =>
        {
            // AppLovin SDK is initialized, configure and start loading ads.
            Debug.Log("MAX SDK Initialized");

            InitializeBannerAds();
            InitializeInterstitialAds();
            InitializeRewardedAds();
        };

        MaxSdk.SetSdkKey(MaxSdkKey);
        // MaxSdk.SetHasUserConsent(hasUserConsent);
        // MaxSdk.SetIsAgeRestrictedUser(isAgeRestrictedUser);
        MaxSdk.InitializeSdk();
    }

    public void BtnBack()
    {
        debugger.SetActive(false);
    }

    public void ShowPrivacyPolicy()
    {
        if (String.IsNullOrEmpty(privacyPolicyUrl) || String.IsNullOrWhiteSpace(privacyPolicyUrl)) return;

        Application.OpenURL(privacyPolicyUrl);
    }

    #region Check Ads

    public bool CanShowBannerAds()
    {
        return PlayerPrefs.GetInt(Save_Remove_BannerAds) == 0;
    }

    public bool CanShowInterstitialAds()
    {
        return PlayerPrefs.GetInt(Save_Remove_InterstitialAds) == 0;
    }

    public bool CanShowRewardedAds()
    {
        return PlayerPrefs.GetInt(Save_Remove_RewardAds) == 0;
    }

    #endregion

    #region  Remove Ads

    private void SetupSaveRemoveAds()
    {
        if (!PlayerPrefs.HasKey(Save_Remove_BannerAds))
        {
            PlayerPrefs.SetInt(Save_Remove_BannerAds, 0);
        }
        if (!PlayerPrefs.HasKey(Save_Remove_InterstitialAds))
        {
            PlayerPrefs.SetInt(Save_Remove_InterstitialAds, 0);
        }
        if (!PlayerPrefs.HasKey(Save_Remove_RewardAds))
        {
            PlayerPrefs.SetInt(Save_Remove_RewardAds, 0);
        }
    }

    public void RemoveBannerAds()
    {
        PlayerPrefs.SetInt(Save_Remove_BannerAds, 1);
        HideBanner();
    }

    public void RemoveInterstitialAds()
    {
        PlayerPrefs.SetInt(Save_Remove_InterstitialAds, 1);
    }

    public void RemoveRewardAds()
    {
        PlayerPrefs.SetInt(Save_Remove_RewardAds, 1);
    }

    #endregion

    public void ShowAppOpen()
    {
        AdmobManager.ins.ShowAppOpenAds();
    }

    #region Banner Ad Methods

    private void InitializeBannerAds()
    {
        // Attach Callbacks
        MaxSdkCallbacks.Banner.OnAdLoadedEvent += OnBannerAdLoadedEvent;
        MaxSdkCallbacks.Banner.OnAdLoadFailedEvent += OnBannerAdFailedEvent;
        MaxSdkCallbacks.Banner.OnAdClickedEvent += OnBannerAdClickedEvent;
        MaxSdkCallbacks.Banner.OnAdRevenuePaidEvent += OnBannerAdRevenuePaidEvent;

        // Banners are automatically sized to 320x50 on phones and 728x90 on tablets.
        // You may use the utility method `MaxSdkUtils.isTablet()` to help with view sizing adjustments.
        MaxSdk.CreateBanner(BannerKey, bannerPosition);

        // Set background or background color for banners to be fully functional.
        MaxSdk.SetBannerBackgroundColor(BannerKey, Color.black);
    }

    private void ToggleBannerVisibility()
    {
        if (!isBannerShowing)
        {
            ShowBanner();
            showBannerButton.GetComponentInChildren<Text>().text = "Hide Banner";
        }
        else
        {
            HideBanner();
            showBannerButton.GetComponentInChildren<Text>().text = "Show Banner";
        }

        isBannerShowing = !isBannerShowing;
    }

    public void ShowBanner()
    {
        if (PlayerPrefs.GetInt(Save_Remove_BannerAds) == 1) return;
        MaxSdk.ShowBanner(BannerKey);
    }

    public void HideBanner()
    {
        MaxSdk.HideBanner(BannerKey);
    }

    private void OnBannerAdLoadedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        // Banner ad is ready to be shown.
        // If you have already called MaxSdk.ShowBanner(BannerKey) it will automatically be shown on the next ad refresh.
        Debug.Log("Banner ad loaded");
        if (autoStartBanner)
        {
            ShowBanner();
        }
    }

    private void OnBannerAdFailedEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo)
    {
        // Banner ad failed to load. MAX will automatically try loading a new ad internally.
        Debug.Log("Banner ad failed to load with error code: " + errorInfo.Code);
    }

    private void OnBannerAdClickedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        Debug.Log("Banner ad clicked");
    }

    private void OnBannerAdRevenuePaidEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        // Banner ad revenue paid. Use this callback to track user revenue.
        Debug.Log("Banner ad revenue paid");

        // Ad revenue
        double revenue = adInfo.Revenue;

        // Miscellaneous data
        string countryCode = MaxSdk.GetSdkConfiguration().CountryCode; // "US" for the United States, etc - Note: Do not confuse this with currency code which is "USD" in most cases!
        string networkName = adInfo.NetworkName; // Display name of the network that showed the ad (e.g. "AdColony")
        string adUnitIdentifier = adInfo.AdUnitIdentifier; // The MAX Ad Unit ID
        string placement = adInfo.Placement; // The placement this ad's postbacks are tied to
    }

    #endregion

    #region Interstitial Ad Methods

    private void InitializeInterstitialAds()
    {
        // Attach callbacks
        MaxSdkCallbacks.Interstitial.OnAdLoadedEvent += OnInterstitialLoadedEvent;
        MaxSdkCallbacks.Interstitial.OnAdLoadFailedEvent += OnInterstitialFailedEvent;
        MaxSdkCallbacks.Interstitial.OnAdDisplayFailedEvent += InterstitialFailedToDisplayEvent;
        MaxSdkCallbacks.Interstitial.OnAdHiddenEvent += OnInterstitialDismissedEvent;
        MaxSdkCallbacks.Interstitial.OnAdRevenuePaidEvent += OnInterstitialRevenuePaidEvent;
        MaxSdkCallbacks.Interstitial.OnAdClickedEvent += InterstitialOnOnAdClickedEvent;

        // Load the first interstitial
        LoadInterstitial();
    }

    void LoadInterstitial()
    {
        interstitialStatusText.text = "Loading...";
        MaxSdk.LoadInterstitial(InterstitialKey);
    }

    private IEnumerator IEWaitingToInterstitialAds()
    {
        interstitialIsCountingDown = true;
        yield return new WaitForSeconds(((float)FirebaseManager.ins.GetNumberFromRemoteConfig(show_interstitial_ads_interval)));
        interstitialIsCountingDown = false;
    }

    private void WaitingToInterstitialAds()
    {
        StartCoroutine(IEWaitingToInterstitialAds());
    }

    public void ShowInterstitial(UnityAction callback = null, string placement = "Unknown")
    {
        if (PlayerPrefs.GetInt(Save_Remove_InterstitialAds) == 1 || interstitialIsCountingDown)
        {
            OnInterstitialClosed = callback;
            callback?.Invoke();
            return;
        }

        placementAdsInt = placement;
        OnInterstitialClosed = callback;
        SendEventManager.ins.SendVideoAvailable(placement, VideoAdsType.Interstitial);
        if (MaxSdk.IsInterstitialReady(InterstitialKey))
        {
            interstitialStatusText.text = "Showing";
            MaxSdk.ShowInterstitial(InterstitialKey);
            SendEventManager.ins.SendAdsStarted("Interstitial", placementAdsInt);
        }
        else
        {
            interstitialStatusText.text = "Ad not ready";
            callback?.Invoke();
        }
    }

    public bool IsInterstitialReady()
    {
        return MaxSdk.IsInterstitialReady(InterstitialKey);
    }

    private void OnInterstitialLoadedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        // Interstitial ad is ready to be shown. MaxSdk.IsInterstitialReady(InterstitialKey) will now return 'true'
        interstitialStatusText.text = "Loaded";
        Debug.Log("Interstitial loaded");

        // Reset retry attempt
        interstitialRetryAttempt = 0;
    }

    private void OnInterstitialFailedEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo)
    {
        // Interstitial ad failed to load. We recommend retrying with exponentially higher delays up to a maximum delay (in this case 64 seconds).
        interstitialRetryAttempt++;
        double retryDelay = Math.Pow(2, Math.Min(6, interstitialRetryAttempt));

        interstitialStatusText.text = "Load failed: " + errorInfo.Code + "\nRetrying in " + retryDelay + "s...";
        Debug.Log("Interstitial failed to load with error code: " + errorInfo.Code);

        Invoke("LoadInterstitial", (float)retryDelay);
    }

    private void InterstitialFailedToDisplayEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo, MaxSdkBase.AdInfo adInfo)
    {
        // Interstitial ad failed to display. We recommend loading the next ad
        Debug.Log("Interstitial failed to display with error code: " + errorInfo.Code);
        LoadInterstitial();
    }

    private void OnInterstitialDismissedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        // Interstitial ad is hidden. Pre-load the next ad
        Debug.Log("Interstitial dismissed");
        LoadInterstitial();
    }

    private void OnInterstitialRevenuePaidEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        // Interstitial ad revenue paid. Use this callback to track user revenue.
        Debug.Log("Interstitial revenue paid");

        // Ad revenue
        double revenue = adInfo.Revenue;

        // Miscellaneous data
        string countryCode = MaxSdk.GetSdkConfiguration().CountryCode; // "US" for the United States, etc - Note: Do not confuse this with currency code which is "USD" in most cases!
        string networkName = adInfo.NetworkName; // Display name of the network that showed the ad (e.g. "AdColony")
        string adUnitIdentifier = adInfo.AdUnitIdentifier; // The MAX Ad Unit ID
        string placement = adInfo.Placement; // The placement this ad's postbacks are tied to
    }

    private void InterstitialOnOnAdClickedEvent(string arg1, MaxSdkBase.AdInfo arg2)
    {
        if (placementAdsInt != String.Empty)
        {
            SendEventManager.ins.SendVideoAdsWatch("Interstitial", placementAdsInt, ResultVideoAdsWatch.clicked);
        }
    }

    private void CompleteMethodInterstitial()
    {
        if (OnInterstitialClosed != null)
        {
            OnInterstitialClosed();
            WaitingToInterstitialAds();
            OnInterstitialClosed = null;

            if (placementAdsInt != String.Empty)
            {
                SendEventManager.ins.SendVideoAdsWatch("Interstitial", placementAdsInt, ResultVideoAdsWatch.watched);
                placementAdsInt = String.Empty;
            }
        }
    }

    #endregion

    #region Rewarded Ad Methods

    private void InitializeRewardedAds()
    {
        // Attach callbacks
        MaxSdkCallbacks.Rewarded.OnAdLoadedEvent += OnRewardedAdLoadedEvent;
        MaxSdkCallbacks.Rewarded.OnAdLoadFailedEvent += OnRewardedAdFailedEvent;
        MaxSdkCallbacks.Rewarded.OnAdDisplayFailedEvent += OnRewardedAdFailedToDisplayEvent;
        MaxSdkCallbacks.Rewarded.OnAdDisplayedEvent += OnRewardedAdDisplayedEvent;
        MaxSdkCallbacks.Rewarded.OnAdClickedEvent += OnRewardedAdClickedEvent;
        MaxSdkCallbacks.Rewarded.OnAdHiddenEvent += OnRewardedAdDismissedEvent;
        MaxSdkCallbacks.Rewarded.OnAdReceivedRewardEvent += OnRewardedAdReceivedRewardEvent;
        MaxSdkCallbacks.Rewarded.OnAdRevenuePaidEvent += OnRewardedAdRevenuePaidEvent;

        // Load the first RewardedAd
        LoadRewardedAd();
    }

    private void LoadRewardedAd()
    {
        rewardedStatusText.text = "Loading...";
        MaxSdk.LoadRewardedAd(RewardedKey);
    }

    public void ShowRewarded(UnityAction<bool> callback = null, string placement = "Unknown")
    {
        if (PlayerPrefs.GetInt(Save_Remove_RewardAds) == 1)
        {
            OnCompleteRewardMethod = callback;
            callback?.Invoke(true);
            return;
        }

        placementAdsReward = placement;
        receivedRewardComplete = false;
        OnCompleteRewardMethod = callback;
        SendEventManager.ins.SendVideoAvailable(placement, VideoAdsType.Rewarded);
        if (MaxSdk.IsRewardedAdReady(RewardedKey))
        {
            rewardedStatusText.text = "Showing";
            SendEventManager.ins.SendAdsStarted("Rewarded", placementAdsReward);
            MaxSdk.ShowRewardedAd(RewardedKey);
        }
        else
        {
            rewardedStatusText.text = "Ad not ready";
            callback?.Invoke(false);
        }
    }


    public bool IsRewardedReady()
    {
        return MaxSdk.IsRewardedAdReady(RewardedKey);
    }

    private void OnRewardedAdLoadedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        // Rewarded ad is ready to be shown. MaxSdk.IsRewardedAdReady(RewardedKey) will now return 'true'
        rewardedStatusText.text = "Loaded";
        Debug.Log("Rewarded ad loaded");

        // Reset retry attempt
        rewardedRetryAttempt = 0;
    }

    private void OnRewardedAdFailedEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo)
    {
        // Rewarded ad failed to load. We recommend retrying with exponentially higher delays up to a maximum delay (in this case 64 seconds).
        rewardedRetryAttempt++;
        double retryDelay = Math.Pow(2, Math.Min(6, rewardedRetryAttempt));

        rewardedStatusText.text = "Load failed: " + errorInfo.Code + "\nRetrying in " + retryDelay + "s...";
        Debug.Log("Rewarded ad failed to load with error code: " + errorInfo.Code);

        Invoke("LoadRewardedAd", (float)retryDelay);
    }

    private void OnRewardedAdFailedToDisplayEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo, MaxSdkBase.AdInfo adInfo)
    {
        // Rewarded ad failed to display. We recommend loading the next ad
        Debug.Log("Rewarded ad failed to display with error code: " + errorInfo.Code);
        LoadRewardedAd();
    }

    private void OnRewardedAdDisplayedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        Debug.Log("Rewarded ad displayed");
    }

    private void OnRewardedAdClickedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        Debug.Log("Rewarded ad clicked");
        if (placementAdsReward != String.Empty)
        {
            SendEventManager.ins.SendVideoAdsWatch("Rewarded", placementAdsReward, ResultVideoAdsWatch.clicked);
        }
    }

    private void OnRewardedAdDismissedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        // Rewarded ad is hidden. Pre-load the next ad
        Debug.Log("Rewarded ad dismissed");
        LoadRewardedAd();
        if (!receivedRewardComplete)
        {
            CompleteMethodRewardedVideo(false);
        }
    }

    private void OnRewardedAdReceivedRewardEvent(string adUnitId, MaxSdk.Reward reward, MaxSdkBase.AdInfo adInfo)
    {
        // Rewarded ad was displayed and user should receive the reward
        Debug.Log("Rewarded ad received reward");
        receivedRewardComplete = true;
        CompleteMethodRewardedVideo(true);
    }

    private void OnRewardedAdRevenuePaidEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        // Rewarded ad revenue paid. Use this callback to track user revenue.
        Debug.Log("Rewarded ad revenue paid");

        // Ad revenue
        double revenue = adInfo.Revenue;

        // Miscellaneous data
        string countryCode = MaxSdk.GetSdkConfiguration().CountryCode; // "US" for the United States, etc - Note: Do not confuse this with currency code which is "USD" in most cases!
        string networkName = adInfo.NetworkName; // Display name of the network that showed the ad (e.g. "AdColony")
        string adUnitIdentifier = adInfo.AdUnitIdentifier; // The MAX Ad Unit ID
        string placement = adInfo.Placement; // The placement this ad's postbacks are tied to
    }

    void CompleteMethodRewardedVideo(bool val)
    {
        if (OnCompleteRewardMethod != null)
        {
            OnCompleteRewardMethod(val);
            if (placementAdsReward != String.Empty)
            {
                Debug.Log("nhan thuong");
                SendEventManager.ins.SendVideoAdsWatch("Rewarded", placementAdsReward,
                    val ? ResultVideoAdsWatch.watched : ResultVideoAdsWatch.canceled);
                placementAdsReward = String.Empty;
            }
            OnCompleteRewardMethod = null;
        }
    }

    #endregion
}