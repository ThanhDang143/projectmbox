/* @ThanhD143 */
/* Sample https://github.com/firebase/quickstart-unity/blob/master/remote_config/testapp/Assets/Firebase/Sample/RemoteConfig/UIHandler.cs */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Analytics;
using System.Threading.Tasks;
using Firebase.Extensions;
using System;

public class FirebaseManager : MonoBehaviour
{
    public static FirebaseManager ins;

    Firebase.DependencyStatus dependencyStatus = Firebase.DependencyStatus.UnavailableOther;
    private bool isFirebaseInitialized = false;

    [Header("Remote Config Variable & Default value")]
    [SerializeField] private RemoteConfigBoolVariable[] remoteConfigBoolVariables;
    [SerializeField] private RemoteConfigStringVariable[] remoteConfigStringVariables;
    [SerializeField] private RemoteConfigNumberVariable[] remoteConfigNumberVariables;

    private void Awake()
    {
        ins = this;
    }

    private void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
        {
            dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                System.Collections.Generic.Dictionary<string, object> defaults = new System.Collections.Generic.Dictionary<string, object>();

                // Init Default Value
                // These are the values that are used if we haven't fetched data from the server yet, or if we ask for values that the server doesn't have:
                if (remoteConfigBoolVariables.Length > 0)
                {
                    foreach (RemoteConfigBoolVariable b in remoteConfigBoolVariables)
                    {
                        defaults.Add(b.variableName, b.defaultBoolValue);
                    }
                }

                if (remoteConfigNumberVariables.Length > 0)
                {
                    foreach (RemoteConfigNumberVariable b in remoteConfigNumberVariables)
                    {
                        defaults.Add(b.variableName, b.defaultNumberValue);
                    }
                }

                if (remoteConfigStringVariables.Length > 0)
                {
                    foreach (RemoteConfigStringVariable b in remoteConfigStringVariables)
                    {
                        defaults.Add(b.variableName, b.defaultStringValue);
                    }
                }

                Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.SetDefaultsAsync(defaults).ContinueWithOnMainThread(task =>
                {
                    Debug.Log("RemoteConfig configured and ready!");
                    isFirebaseInitialized = true;
                    FetchDataFromRemoteConfig();
                });

                //Firebase Messaging
                Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
                Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
            }
            else
            {
                Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

    public void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
    {
        UnityEngine.Debug.Log("Received Registration Token: " + token.Token);
    }

    public void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
    {
        UnityEngine.Debug.Log("Received a new message from: " + e.Message.From);
    }

    public void FetchDataFromRemoteConfig()
    {
        if (!isFirebaseInitialized)
        {
            Initialize();
            return;
        }

        FetchDataAsync();
    }

    public bool GetBoolFromRemoteConfig(string key)
    {
        return Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.GetValue(key).BooleanValue;
    }

    public string GetStringFromRemoteConfig(string key)
    {
        return Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.GetValue(key).StringValue;
    }

    public double GetNumberFromRemoteConfig(string key)
    {
        return Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.GetValue(key).DoubleValue;
    }

    public Task FetchDataAsync()
    {
        Debug.Log("Fetching data...");
        System.Threading.Tasks.Task fetchTask = Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.FetchAsync(TimeSpan.Zero);
        return fetchTask.ContinueWith(FetchComplete);
    }

    private void FetchComplete(Task fetchTask)
    {
        if (fetchTask.IsCanceled)
        {
            Debug.Log("Fetch canceled.");
        }
        else if (fetchTask.IsFaulted)
        {
            Debug.Log("Fetch encountered an error.");
        }
        else if (fetchTask.IsCompleted)
        {
            Debug.Log("Fetch completed successfully!");
        }

        var info = Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.Info;
        switch (info.LastFetchStatus)
        {
            case Firebase.RemoteConfig.LastFetchStatus.Success:
                Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.ActivateAsync()
                .ContinueWith(task =>
                {
                    Debug.Log(String.Format("Remote data loaded and ready (last fetch time {0}).", info.FetchTime));
                });

                break;
            case Firebase.RemoteConfig.LastFetchStatus.Failure:
                switch (info.LastFetchFailureReason)
                {
                    case Firebase.RemoteConfig.FetchFailureReason.Error:
                        Debug.Log("Fetch failed for unknown reason");
                        break;
                    case Firebase.RemoteConfig.FetchFailureReason.Throttled:
                        Debug.Log("Fetch throttled until " + info.ThrottledEndTime);
                        break;
                }
                break;
            case Firebase.RemoteConfig.LastFetchStatus.Pending:
                Debug.Log("Latest Fetch call still pending.");
                break;
        }
    }
}

[System.Serializable]
public class RemoteConfigBoolVariable
{
    public string variableName;
    public bool defaultBoolValue;
}

[System.Serializable]
public class RemoteConfigNumberVariable
{
    public string variableName;
    public double defaultNumberValue;
}

[System.Serializable]
public class RemoteConfigStringVariable
{
    public string variableName;
    public string defaultStringValue;
}