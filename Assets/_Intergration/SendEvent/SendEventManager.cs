using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

namespace SendEventAppMetrica
{
    public class SendEventManager : MonoBehaviour
    {
        public static SendEventManager ins;
        private void Awake()
        {
            if (ins != null)
            {
                Destroy(gameObject);
            }
            else
            {
                ins = this;
                DontDestroyOnLoad(gameObject);
            }

            //            LevelStart = new LevelStart();
            //            LevelStart.InitDefault();
            //            LevelFinish = new LevelFinish();
            //            LevelFinish.InitDefault();
            //            VideoAdsAvailable = new VideoAdsAvailable();
            //            VideoAdsAvailable.InitDefault();
            //            VideoAdsStarted = new VideoAdsStarted();
            //            VideoAdsStarted.InitDefault();
            //            VideoAdsWatch = new VideoAdsWatch();
            //            VideoAdsWatch.InitDefault();
            //            PaymentSucceed = new PaymentSucceed();
            //            PaymentSucceed.InitDefault();
            //            Debug.Log(LevelStart.eventAppmetrica.nameEvent);
            //            Debug.Log(LevelFinish.eventAppmetrica.nameEvent);

        }

        public void SendEvent(EventAppmetrica eventAppmetrica)
        {
            AppMetrica.Instance.ReportEvent(eventAppmetrica.nameEvent, eventAppmetrica.parameters);
        }

        public void SendEventBuffer(EventAppmetrica eventAppmetrica)
        {
            AppMetrica.Instance.ReportEvent(eventAppmetrica.nameEvent, eventAppmetrica.parameters);
            AppMetrica.Instance.SendEventsBuffer();
        }

        public void SendVideoAvailable(string placement, VideoAdsType adsType)
        {
            bool isSuccess = false;
            if (adsType == VideoAdsType.Interstitial)
            {
                isSuccess = MaxSdk.IsInterstitialReady(ManagerAds.ins.InterstitialKey);
            }
            else
            {
                isSuccess = MaxSdk.IsRewardedAdReady(ManagerAds.ins.RewardedKey);
            }

            VideoAdsAvailable.CustomParameters(new Dictionary<string, object>()
            {
                {VideoAdsAvailableParameters.ad_type,"Rewarded"},
                {VideoAdsAvailableParameters.placement,placement},
                {VideoAdsAvailableParameters.result, isSuccess ? "Success" : "Not_available"},
                {VideoAdsAvailableParameters.connection, Application.internetReachability != NetworkReachability.NotReachable},
            });
            SendEvent(VideoAdsAvailable.GetEventAppmetrica());
        }

        public void SendAdsStarted(string ad_type, string placement)
        {
            VideoAdsStarted.CustomParameters(new Dictionary<string, object>()
            {
                {VideoAdsStartedParameters.ad_type,ad_type},
                {VideoAdsStartedParameters.placement,placement},
                {VideoAdsStartedParameters.result,"Start"},
                {VideoAdsStartedParameters.connection,Application.internetReachability != NetworkReachability.NotReachable},
            });
            SendEvent(VideoAdsStarted.GetEventAppmetrica());
        }

        public void SendVideoAdsWatch(string ad_type, string placement, ResultVideoAdsWatch result)// video ads watch
        {
            VideoAdsWatch.CustomParameters(new Dictionary<string, object>()
            {
                {VideoAdsWatchParameters.ad_type,ad_type},
                {VideoAdsWatchParameters.placement,placement},
                {VideoAdsWatchParameters.result,result.ToString()},
                {VideoAdsWatchParameters.connection,Application.internetReachability != NetworkReachability.NotReachable},
            });
            SendEvent(VideoAdsWatch.GetEventAppmetrica());
        }

        public void SendPaymentSucceed(Product product, string iap_type)
        {
            Debug.Log(product.metadata.localizedPrice);
            PaymentSucceed.CustomParameters(new Dictionary<string, object>()
               {
                   {PaymentSucceedParameters.inapp_id, product.definition.id},
                   {PaymentSucceedParameters.currency, "USD"},
                   {PaymentSucceedParameters.price, product.metadata.localizedPrice},
                   {PaymentSucceedParameters.inapp_type, iap_type},
               });
        }
        // }

        public enum ResultVideoAdsWatch
        {
            watched,
            clicked,
            canceled
        }
    }

    [System.Serializable]
    public class EventAppmetrica
    {
        public string nameEvent;
        public Dictionary<string, object> parameters;

        public EventAppmetrica(string nEvent, Dictionary<string, object> param)
        {
            nameEvent = nEvent;
            parameters = param;
        }
    }

    public enum VideoAdsType
    {
        Interstitial,
        Rewarded
    }
}
