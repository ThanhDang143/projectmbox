using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SendEventAppMetrica
{
    public class LevelStart
    {
        private const string nameEvent = "level_start";
        public static EventAppmetrica eventAppmetrica;

        /// <summary>
        /// Thay đổi tất
        /// </summary>
        public static void SetEvent(int level_number, string level_name,   int    level_count, string level_diff, 
            int level_loop,   bool   level_random, string level_type,  string game_mode)
        {
            eventAppmetrica.parameters[LevelStartParameters.level_number] = level_number;
            eventAppmetrica.parameters[LevelStartParameters.level_name]   = level_name;
            eventAppmetrica.parameters[LevelStartParameters.level_count]  = level_count;
            eventAppmetrica.parameters[LevelStartParameters.level_diff]   = level_diff;
            eventAppmetrica.parameters[LevelStartParameters.level_loop]   = level_loop;
            eventAppmetrica.parameters[LevelStartParameters.level_random] = level_random;
            eventAppmetrica.parameters[LevelStartParameters.level_type]   = level_type;
            eventAppmetrica.parameters[LevelStartParameters.game_mode]    = game_mode;
        }

        /// <summary>
        /// Thay đổi đơn lẻ giá trị levelstart
        /// </summary>
        /// <param name="nameParameter">Get from LevelStartParameters. Eg: LevelStartParameters.level_number</param>
        /// <param name="value">eg: bool, int, string</param>
        public static void SetEvent(string nameParameter, object value)
        {
            eventAppmetrica.parameters[nameParameter] = value;
        }

        public static void InitDefault()
        {
            eventAppmetrica = new EventAppmetrica(nameEvent, new Dictionary<string, object>()
            {
                {LevelStartParameters.level_number, -1},
                {LevelStartParameters.level_name, "init"},
                {LevelStartParameters.level_count, -1},
                {LevelStartParameters.level_diff, "init"},
                {LevelStartParameters.level_loop, -1},
                {LevelStartParameters.level_random, false},
                {LevelStartParameters.level_type, "init"},
                {LevelStartParameters.game_mode, "init"},
            });
        }
        
        public static void CustomParameters(Dictionary<string, object> parameter)
        {
            eventAppmetrica = new EventAppmetrica(nameEvent, parameter);
        }
        
        public static EventAppmetrica GetEventAppmetrica()
        {
            return eventAppmetrica;
        }
    }
    
    
    [System.Serializable]
    public static class LevelStartParameters
    {
        public const string level_number = "level_number";
        public const string level_name   = "level_name";
        public const string level_count  = "level_count";
        public const string level_diff   = "level_diff";
        public const string level_loop   = "level_loop";
        public const string level_random = "level_random";
        public const string level_type   = "level_type";
        public const string game_mode    = "game_mode";
    }
    
}
