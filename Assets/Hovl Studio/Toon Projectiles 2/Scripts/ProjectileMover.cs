﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ProjectileMover : MonoBehaviour
{
    public float speed = 15f;
    public float hitOffset = 0f;
    public bool UseFirePointRotation;
    public Vector3 rotationOffset = new Vector3(0, 0, 0);
    public GameObject hit;
    public GameObject flash;
    private Rigidbody rb;
    public GameObject[] Detached;
    private Transform target;
    private Transform spawner;

    public void OnSpawn(Transform _target, Transform _spawner)
    {
        if (_spawner.GetComponent<MonsterInfo>().IDMonsterEvolve <= 0)
        {
            transform.localScale = transform.localScale * 1.5f;
        }

        rb = GetComponent<Rigidbody>();
        if (flash != null)
        {
            var flashInstance = Instantiate(flash, transform.position, Quaternion.identity);
            if (_spawner.GetComponent<MonsterInfo>().IDMonsterEvolve <= 0)
            {
                flashInstance.transform.localScale = flashInstance.transform.localScale * 1.5f;
            }

            flashInstance.transform.forward = gameObject.transform.forward;
            var flashPs = flashInstance.GetComponent<ParticleSystem>();
            if (flashPs != null)
            {
                Destroy(flashInstance, flashPs.main.duration);
            }
            else
            {
                var flashPsParts = flashInstance.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(flashInstance, flashPsParts.main.duration);
            }
        }

        target = _target;
        spawner = _spawner;

        Vector3 centerPos = _target.GetChild(1).GetComponent<Renderer>().bounds.center;

        transform.DOMove(centerPos, 0.3f).OnComplete(() =>
        {
            if (gameObject.activeSelf)
            {
                Destroy(gameObject);
            }
        });
    }

    // void FixedUpdate ()
    // {
    // 	if (speed != 0)
    //     {
    //         rb.velocity = transform.forward * speed;
    //         //transform.position += transform.forward * (speed * Time.deltaTime);         
    //     }
    // }

    private void OnCollisionEnter(Collision collision)
    {
        if (!target || !spawner) return;
        if (collision.transform.CompareTag(spawner.tag) || collision.transform.CompareTag(StaticString.tagPlane) || collision.transform.CompareTag(StaticString.tagAttackFX)) return;

        //Lock all axes movement and rotation
        rb.constraints = RigidbodyConstraints.FreezeAll;
        speed = 0;

        ContactPoint contact = collision.contacts[0];
        Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
        Vector3 pos = contact.point + contact.normal * hitOffset;

        if (hit != null)
        {
            var hitInstance = Instantiate(hit, pos, rot);
            if (spawner.GetComponent<MonsterInfo>().IDMonsterEvolve <= 0)
            {
                hitInstance.transform.localScale = hitInstance.transform.localScale * 1.5f;
            }

            if (UseFirePointRotation) { hitInstance.transform.rotation = gameObject.transform.rotation * Quaternion.Euler(0, 180f, 0); }
            else if (rotationOffset != Vector3.zero) { hitInstance.transform.rotation = Quaternion.Euler(rotationOffset); }
            else { hitInstance.transform.LookAt(contact.point + contact.normal); }

            var hitPs = hitInstance.GetComponent<ParticleSystem>();
            if (hitPs != null)
            {
                Destroy(hitInstance, hitPs.main.duration);
            }
            else
            {
                var hitPsParts = hitInstance.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(hitInstance, hitPsParts.main.duration);
            }
        }
        foreach (var detachedPrefab in Detached)
        {
            if (detachedPrefab != null)
            {
                detachedPrefab.transform.parent = null;
            }
        }
        Destroy(gameObject);
    }
}
